/*
 * author ganesh.krishnan
 * 
 * This class communicates with paypal servers using NVP (name value pairs). There are no soap calls and all the values are passed
 * as http parameters. The paypal SDK is used for communcation and is installed manually in the maven repository
 * using mvn -install command
 */

package com.luckydog.payment.paypal;

import com.paypal.sdk.core.nvp.NVPDecoder;
import com.paypal.sdk.core.nvp.NVPEncoder;
import com.paypal.sdk.profiles.APIProfile;
import com.paypal.sdk.profiles.ProfileFactory;
import com.paypal.sdk.services.NVPCallerServices;

public class PaypalComm {

    private String paypalUserName = "finance-facilitator_api1.alphasigma.com.au";

    private String paypalPassword = "1365566420";

    private String paypalSignature = "AFcWxV21C7fd0v3bYYYRCpSSRl31AuRH11GyX67V4xnN08dC9A87mqLv";

    private String paypalEnvironment = "sandbox";

    private String paypalAPIVersion = "98";

    //https://www.sandbox.paypal.com/webscr?cmd=_express-checkout&token=tokenValue
    public PaypalResponseToken getToken(String returnURL, String cancelURL,
                                        String amount, String paymentType, String currencyCode) {

        NVPCallerServices caller = null;
        NVPEncoder encoder = new NVPEncoder();
        NVPDecoder decoder = new NVPDecoder();
        PaypalResponseToken token = new PaypalResponseToken();
        try {
            caller = new NVPCallerServices();
            APIProfile profile = ProfileFactory.createSignatureAPIProfile();
            // Set up the API credentials, PayPal end point, API operation and
            // version.
            profile.setAPIUsername(paypalUserName);
            profile.setAPIPassword(paypalPassword);
            profile.setSignature(paypalSignature);
            profile.setEnvironment(paypalEnvironment);
            profile.setSubject("");
            caller.setAPIProfile(profile);
            encoder.add("VERSION", paypalAPIVersion);
            encoder.add("METHOD", "SetExpressCheckout");

            // Add request-specific fields to the request string.
            encoder.add("RETURNURL", returnURL);
            encoder.add("CANCELURL", cancelURL);

            encoder.add("AMT", amount);
            encoder.add("PAYMENTACTION", paymentType);
            encoder.add("CURRENCYCODE", currencyCode);

            // Execute the API operation and obtain the response.
            String NVPRequest = encoder.encode();
            String NVPResponse = caller.call(NVPRequest);
            decoder.decode(NVPResponse);

        } catch (Exception ex) {
            ex.printStackTrace();
            // exception handling needs to be paypal specific
            token.setResponseMessage(ex.getMessage());
        }

        if (decoder.get("ACK").equalsIgnoreCase("Failure")) {
            //TODO remove the decoder.
            token.setResponseMessage("FAIL " + decoder.toString());

        } else {
            token.setResponseToken(decoder.get("TOKEN"));
        }
        return token;

    }

    public String doCheckout(String token, String payerID, String amount,
                             String paymentType, String currencyCode) {
        NVPEncoder encoder = new NVPEncoder();
        NVPDecoder decoder = new NVPDecoder();
        NVPCallerServices caller = null;
        try {

            caller = new NVPCallerServices();
            APIProfile profile = ProfileFactory.createSignatureAPIProfile();
            // Set up the API credentials, PayPal end point, API operation and
            // version.
            profile.setAPIUsername(paypalUserName);
            profile.setAPIPassword(paypalPassword);
            profile.setSignature(paypalSignature);
            profile.setEnvironment(paypalEnvironment);
            profile.setSubject("");
            caller.setAPIProfile(profile);
            encoder.add("VERSION", paypalAPIVersion);
            encoder.add("METHOD", "DoExpressCheckoutPayment");

            // Add request-specific fields to the request string.
            // Pass the token value by actual value returned in the
            // SetExpressCheckout.
            encoder.add("TOKEN", token);
            encoder.add("PAYERID", payerID);
            encoder.add("PAYMENTREQUEST_0_AMT", amount);
            encoder.add("PAYMENTACTION", paymentType);
            encoder.add("CURRENCYCODE", currencyCode);
            // Execute the API operation and obtain the response.
            String NVPRequest = encoder.encode();
            String NVPResponse = caller.call(NVPRequest);
            decoder.decode(NVPResponse);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return decoder.get("ACK");
    }

    public String doRefund() throws Exception {
        throw new Exception("Not Supported!");
    }

    public String getPaypalUserName() {
        return paypalUserName;
    }

    public void setPaypalUserName(String paypalUserName) {
        this.paypalUserName = paypalUserName;
    }

    public String getPaypalPassword() {
        return paypalPassword;
    }

    public void setPaypalPassword(String paypalPassword) {
        this.paypalPassword = paypalPassword;
    }

    public String getPaypalSignature() {
        return paypalSignature;
    }

    public void setPaypalSignature(String paypalSignature) {
        this.paypalSignature = paypalSignature;
    }

    public String getPaypalEnvironment() {
        return paypalEnvironment;
    }

    public void setPaypalEnvironment(String paypalEnvironment) {
        this.paypalEnvironment = paypalEnvironment;
    }

    public String getPaypalAPIVersion() {
        return paypalAPIVersion;
    }

    public void setPaypalAPIVersion(String paypalAPIVersion) {
        this.paypalAPIVersion = paypalAPIVersion;
    }

}
