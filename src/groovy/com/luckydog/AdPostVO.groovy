package com.luckydog

import com.luckydog.dealpost.AdPost
import org.codehaus.groovy.grails.plugins.web.taglib.ApplicationTagLib

class AdPostVO {
    def grailsApplication


    String title
    String description
    String tac
    List location

    Float priorPrice = 0
    Float curPrice = 0
    Float percent = 0
    String startDate;
    String endDate
    Integer quantity = 1
    Long karma = 0
    Float avgRating = 0.0
    Boolean isActive = true
    List<String> categories = []
    List<String> address = []
    Long id
    Long merchantId
    String imageURL
    String backURL




    AdPostVO(AdPost adPost) {
        ApplicationTagLib applicationTagLib = new ApplicationTagLib()

        this.id = adPost.id;
        this.merchantId = adPost.merchantId;
//        this.address = adPost.address.toString()]
        this.avgRating = adPost.avgRating
        this.curPrice = adPost.curPrice;
        this.startDate = adPost.formattedStartDate
        this.endDate = adPost.formattedEndDate
        this.description = adPost.description;
        this.isActive = adPost.isActive;
        this.karma = adPost.karma;
        this.location = adPost.location;
        this.percent = adPost.percent;
        this.quantity = adPost.quantity;
        this.priorPrice = adPost.priorPrice;
        this.tac = adPost.tac;
        this.title = adPost.title;
        this.imageURL = applicationTagLib.createLink(controller:"adImage", action: "getAllImages",params: [id: adPost.id] )
//        this.backURL="createLink(controller:'PublicApi',action: 'getPurchaseDealList')";
    }

    AdPostVO() {

    }

}
