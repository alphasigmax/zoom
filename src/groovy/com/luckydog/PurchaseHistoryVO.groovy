package com.luckydog

import com.luckydog.shopper.PurchaseHistory
import org.codehaus.groovy.grails.plugins.web.taglib.ApplicationTagLib

class PurchaseHistoryVO {
    def grailsApplication
    Long id
    Long buyerId
    Long itemId
    Integer status
    Date dateCreated
    Date dateUpdated
    String buyerListURL

    PurchaseHistoryVO() {}

    PurchaseHistoryVO(PurchaseHistory purchaseHistory) {
        ApplicationTagLib applicationTagLib=new ApplicationTagLib()
        this.id=purchaseHistory.id
        this.buyerId = purchaseHistory.buyerId
        this.itemId=purchaseHistory.itemId
        this.status=purchaseHistory.status
        this.buyerListURL=applicationTagLib.createLink(controller:"deal" ,action:"getMerchantsDealJSON" )
    }

}
