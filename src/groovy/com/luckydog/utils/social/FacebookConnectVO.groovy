package com.luckydog.utils.social

class FacebookConnectVO extends SocialConnectVO {

    String about
    String location
    String gender
    String address
    String dateOfBirth
    String hometown
    String phoneNumber

    List<String> languages = []

    Long followers
    Long followings


    FacebookConnectVO(def facebookData) {
        println("----------- Facebook Data --------------------- " + facebookData)
        this.firstName = facebookData?.first_name
        this.lastName = facebookData?.last_name
        this.username = facebookData?.username
        this.link = facebookData?.link
        this.location = facebookData?.location?.name
        this.about = facebookData?.description
        this.profileImgUrl = facebookData?.profile_image_url
        this.profileId = facebookData?.id
        this.email = facebookData?.email
        this.hometown = facebookData?.hometown?.name
        this.gender = facebookData?.gender
        this.address = facebookData?.address
        this.phoneNumber = facebookData?.phoneNumber
        this.followers = facebookData?.friends?.data?.size()
        this.location = facebookData?.location?.name

        facebookData?.languages?.each {
            String language = it.name
            this.languages.add(language)
        }
    }
}


