package com.luckydog.utils.social

class LinkedInConnectVO extends SocialConnectVO {

    String location
    String about
    String address
    String dateOfBirth
    String phoneNumber
    String gender
    String accessSecret
    String industry
    String summary
    String publicProfileUrl
    Long followers
    Long followings
    List<String> languages = []

    LinkedInConnectVO(def linkedInData) {
        this.firstName = linkedInData?.firstName
        this.lastName = linkedInData?.lastName
        this.about = linkedInData?.summary
        this.profileImgUrl = linkedInData?.pictureUrl
        this.profileId = linkedInData?.id
        this.email = linkedInData?.emailAddress
        this.address = linkedInData?.mainAddress
        this.phoneNumber = linkedInData?.dateOfBirth
        this.phoneNumber = linkedInData?.phoneNumbers?.values?.phoneNumber?.get(0)
        this.location = linkedInData?.location?.country?.code?.toUpperCase()
        this.gender = linkedInData?.gender
        this.industry = linkedInData?.industry
        this.summary = linkedInData?.summary
        this.publicProfileUrl = linkedInData?.publicProfileUrl
        this.followers = linkedInData?.connections?._total

//        if (linkedInData?.dateOfBirth?.day && linkedInData?.dateOfBirth?.month && linkedInData?.dateOfBirth?.year) {
//            String dateStr = linkedInData.dateOfBirth.month + "/" + linkedInData.dateOfBirth.day + "/" + linkedInData.dateOfBirth.year
//            this.dateOfBirth = new LocalDate(new Date(dateStr).time)
//        }
        linkedInData?.languages?.values?.each {
            String language = it.language.name
            this.languages.add(language)
        }

    }
}