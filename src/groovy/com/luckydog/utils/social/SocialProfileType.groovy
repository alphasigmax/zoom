package com.luckydog.utils.social

public enum SocialProfileType {
    FACEBOOK,
    LINKED_IN,
    TWITTER,
    GOOGLE
}
