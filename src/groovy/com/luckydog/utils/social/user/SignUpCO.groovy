package com.luckydog.utils.social.user

import com.luckydog.security.User
import org.codehaus.groovy.grails.validation.Validateable

@Validateable
class SignUpCO {

    String name
    String email
    String password
    String confirmPassword

    static constraints = {
        email(nullable: false, blank: false, email: true, validator: { val, obj ->
            if (User.findByUsername(val)) {
                return "email.must.be.unique"
            }
        })
        name(nullable: false, blank: false)
        password(nullable: false, blank: false)
        confirmPassword(nullable: false, blank: false, validator: { val, obj ->
            val = obj.confirmPassword
            if (!obj.password.equals(val)) {
                return "your.confirm.pass.is.not.match"
            }
        })

    }


}
