package com.luckydog.utils.social.user

import org.codehaus.groovy.grails.validation.Validateable


@Validateable
class ShopperCO {

    String name;
    String email;
    String password;

    static constraints = {
        name(nullable: true, blank: true)
        email(nullable: false, blank: false)
        password(nullable: false, blank: false)

    }
}
