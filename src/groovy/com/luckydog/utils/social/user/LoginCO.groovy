package com.luckydog.utils.social.user

import org.codehaus.groovy.grails.validation.Validateable

@Validateable
class LoginCo {

    String username
    String password
    static constraints = {

        username(nullable: false, blank: false)
        password(nullable: false, blank: false)

    }
}
