


var haggell = angular.module('haggell',['satellizer','ui.bootstrap']);

haggell.config(['$authProvider',
    function($authProvider) {
  
      // For any unmatched url, redirect to /state1
     
      $authProvider.loginSignup = true;
      $authProvider.loginRedirect = '/login.html';

      $authProvider.facebook({
        url: 'signIn/connectFacebook',
        clientId: '263863147021728',
        redirectUri: 'http://localhost:8080/Zoom',
        scope: 'email',
        scopeDelimiter: ',',
        requiredUrlParams: ['display', 'scope'],  
      });

      $authProvider.google({
        url:"signIn/connectGoogle",
        clientId: '631036554609-v5hm2amv4pvico3asfi97f54sc51ji4o.apps.googleusercontent.com',
        redirectUri: 'http://localhost:8080/Zoom',
      });

      $authProvider.github({
        clientId: '0ba2600b1dbdb756688b'
      });

      $authProvider.linkedin({
        clientId: '77cw786yignpzj'
      });

      $authProvider.twitter({
        url: '/auth/twitter',
        redirectUri: 'http://localhost:8080/login.html',
      });

      $authProvider.oauth2({
        name: 'foursquare',
        url: '/auth/foursquare',
        redirectUri: window.location.origin,
        clientId: 'MTCEJ3NGW2PNNB31WOSBFDSAD4MTHYVAZ1UKIULXZ2CVFC2K',
        authorizationEndpoint: 'https://foursquare.com/oauth2/authenticate',
      });
    }               
]);

// common login control.
haggell.controller('hgLoginCtrl', ['$scope', '$http' ,'$modal', function ($scope, $http,$modal) {
    
    $scope.loginRequest = {};
    $scope.loginUser = function(isValid){
      if(isValid){
        $http({
          method: "POST",
          url: "j_spring_security_check",
          data: $.param($scope.loginRequest),
          headers: {'X-Requested-With':'XMLHttpRequest','Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'},

        }).success(function(response){ 
          if(response.error){
           loginFailOpen();
          }
          else{
            
            $('#loginpop').modal('hide');
            loginSuccessOpen();
      	  
      	  // TODO in future we might just need to change the top navigation
			// bars. Too complicated for now
          }
        });
      }
      else{
        $scope.submit = false;
      }
    };

    function loginSuccessOpen(){
        var modalInstance = $modal.open({
          templateUrl: 'loginSuccessModal.html',
          controller: 'loginSuccessModalCtrl',
          size: 'lg'
        });
    };

    function loginFailOpen(){
        var modalInstance = $modal.open({
          templateUrl: 'loginFailureModal.html',
          controller: 'loginFailureModalCtrl',
          size: 'lg'
        });
    };
  }
]);

haggell.controller('loginSuccessModalCtrl', ['$scope', '$window', '$modalInstance',
  function ($scope,$window, $modalInstance) {
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
        $window.location.reload();
    };
  }
]);


haggell.controller('loginFailureModalCtrl', function ($scope, $modalInstance) {
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});


haggell.controller('hgMerchRegCtrl', ['$scope', '$http', '$modal' , function ($scope, $http, $modal) {
    
    $scope.regRequest = {address:{}};
    $scope.submitted = false;
    $scope.merRegister = function(){
      $scope.submitted = true;
      console.log($scope.regRequest)
      $scope.regRequest.address.email = $scope.regRequest.username;
      $http.post('merchant/doregister',$scope.registerRequest).success(function(response){
        if(response.errorDescription){
          regFailOpen(response.errorDescription);
        }
        else{
          regSuccessOpen();
        }
      });
    };

    $scope.submitCheck = function(form,$event){
      $event.preventDefault();
      form.$submitted = true;
    }

    $scope.getCountry = function(val) {
      return $http.get('location/findCountry', {
        params: {
          countryName: val
        }
      }).then(function(response){
        return response.data.map(function(item){
          return item.countryName;
        });
      });
    };

    $scope.getCity = function(val) {
      return $http.get('location/findCity', {
        params: {
          cityName: val,
          countryName: $scope.regRequest.country
        }
      }).then(function(response){
        return response.data.map(function(item){
          return item;
        });
      });
    };

    $scope.formatLabel = function(model){
      return model.cityName;
    };

    function regSuccessOpen(){
        var modalInstance = $modal.open({
          templateUrl: 'registerSuccessModal.html',
          controller: 'registerSuccessModalCtrl',
          size: 'lg'
        });
    };

    function regFailOpen(mes){
        var modalInstance = $modal.open({
          templateUrl: 'registerFailureModal.html',
          controller: 'registerFailureModalCtrl',
          size: 'lg',
          resolve: {
              message:function(){
                  return mes;
              }
          }
        });
    };
  }
]);

haggell.controller('registerSuccessModalCtrl', ['$scope', '$window', '$modalInstance',
  function ($scope,$window, $modalInstance) {
    $scope.cancel = function (redirect) {
        $modalInstance.dismiss('cancel');
        if(redirect) $window.location.reload();
    };
  }
]);
haggell.controller('registerFailureModalCtrl', function ($scope, $modalInstance,message) {
    $scope.message = message;
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});

// shopper register
haggell.controller('hgRegisterCtrl', ['$scope', '$http', '$modal', function ($scope, $http, $modal) {
    
    $scope.registerRequest = {};
    $scope.registerUser = function(isValid){
     
      if(isValid){
        $scope.submit = true;
        $http.post('shopper/register',$scope.registerRequest).success(function(response){
          console.log(response);
          if(response.errorDescription){
            // failure to register. Show a bootstrap GUI modal window
           regFailOpen(response.errorDescription);
          }
          else{
            $('#loginpop').modal('hide');
            regSuccessOpen(true);            
            // TODO In the future we have to automatically login the
      // person
          }
        });
      }
      else{
        $scope.submit = false;
      }
    };

    function regSuccessOpen(){
        var modalInstance = $modal.open({
          templateUrl: 'registerSuccessModal.html',
          controller: 'registerSuccessModalCtrl',
          size: 'lg'
        });
    };

    function regFailOpen(mes){
        var modalInstance = $modal.open({
          templateUrl: 'registerFailureModal.html',
          controller: 'registerFailureModalCtrl',
          size: 'lg',
          resolve: {
              message:function(){
                  return mes;
              }
          }
        });
    };
    
  }
]);

//Whats this method for?
haggell.controller('hgPassLoginCtrl', ['$scope','$http','$auth', '$location',
    function($scope,$http,$auth, $location) {  

    $scope.login = function() {
      $auth.login({ email: $scope.email, password: $scope.password })
        .then(function() {
          
        })
        .catch(function(response) {
          console.log(response.data.message);
        });
    };

        $scope.authenticate = function(provider) {
            
            $auth.authenticate(provider)
            .then(function(response) {
              console.log(response);
            });
        };

    }
]);



// create deal

haggell.controller('hgCreateDealCtrl', ['$scope', '$http', function ($scope, $http) {
    
    $scope.createDealRequest = {};

    $scope.discounts =[];
    $scope.dis_visible = false;
    $scope.createDeal = function(isValid){
     
      if(isValid){
        $scope.createDealRequest.discounts = $scope.discounts;
        $scope.submit = true;
        $http.post('deal/create',$scope.createDealRequest).success(function(response){
        	console.log(response);
          if(response.errorDescription){
        	  // failure to register
          }
          else{
            // we have a success. Close the login box and refresh the page.
          }
        });
      }
      else{
        $scope.submit = false;
      }
    };

    $scope.saveDiscount = function(data) {
      //angular.extend(data);
    };

    $scope.removeDiscount = function(index) {
      $scope.discounts.splice(index, 1);
    };

  // add discount
    $scope.addDiscount = function() {
      $scope.inserted = {
        price: $scope.discountprice,
        quantity: $scope.discountquanity
      };
      $scope.discounts.push($scope.inserted);
      $scope.discountprice = '';
      $scope.discountquanity = '';
      $scope.dis_visible = false;
      console.log($scope.discounts);
    };
  }
]);
