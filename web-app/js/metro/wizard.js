var map;

jQuery(document).ready(function($){
	var wiz_div  = $('#myWizard');
	wiz_div.on('changed.fu.wizard', function () {
	    google.maps.event.trigger(map, 'resize');
	});
	
	wiz_div.on('finished.fu.wizard', function (event) {
	   var scope = angular.element(wiz_div).scope();
	   if(formCheck(event)) scope.merRegister();
	});

	wiz_div.on('actionclicked.fu.wizard', function (event) {
		formCheck(event)
	});

	function formCheck(event){
		var step = wiz_div.wizard('selectedItem').step;
		var form = wiz_div.find('.step-pane[data-step="'+step+'"] form');
	  	 if(form.hasClass('ng-invalid')) {
	  	 	form.submit();
	  	 	event.preventDefault();
	  	 	return false;
	  	 }
	  	 else{
	  	 	return true;
	  	 }
	}
});
  
