modules = {



	stylesheets {

		resource url:'/css/flatize/css/bootstrap.min.css'
		resource url:'/css/flatize/fonts/font-awesome/css/font-awesome.css'
		resource url:'/css/flatize/owl-carousel/owl.carousel.css'
		resource url:'/css/flatize/owl-carousel/owl.theme.css'
		resource url:'/css/flatize/owl-carousel/owl.transitions.css'
		resource url:'/css/flatize/theme-animate.css'
		resource url:'/css/flatize/theme-elements.css'
		resource url:'/css/flatize/theme-blog.css'
		resource url:'/css/flatize/theme-shop.css'
		resource url:'/css/flatize/theme.css'
		resource url:'/css/flatize/theme-responsive.css'
		resource url:'/css/flatize/style-switcher.css'
		resource url:'/css/flatize/colors/blue/style.css'
		resource url:'/css/flatize/bootstrap-social.css'
		resource url:'/css/fuelfx/fuelux.min.css'
	}

	homepage {
		resource url:'/js/flatize/jquery.min.js'
		resource url:'/js/angular.min.js'
		resource url:'/js/flatize/bootstrap.min.js'
		resource url:'/js/flatize/bootstrap-hover-dropdown.min.js'
		resource url:'/js/flatize/owl-carousel/owl.carousel.js'
		resource url:'/js/flatize/modernizr.custom.js'
		resource url:'/js/flatize/masonry.pkgd.min.js'
		resource url:'/js/flatize/jquery.stellar.js'
		resource url:'/js/flatize/jquery.pricefilter.js'
		resource url:'/js/flatize/jquery.bxslider.min.js'
		resource url:'/js/flatize/mediaelement-and-player.js'
		resource url:'/js/flatize/waypoints.min.js'
		resource url:'/js/flatize/theme.plugins.js'
		resource url:'/js/flatize/theme.js'
		
		resource url:'/js/satellizer.min.js'
		resource url:'/js/ui-bootstrap-tpls-0.11.2.min.js'
		resource url:'/js/custom.js',exclude:'minify'
		
		resource url:'/js/fuelfx/fuelux.min.js',exclude:'minify'		
	}




	loggedin {
		resource url:'/js/flatize/jquery.min.js'
		resource url:'/js/flatize/bootstrap.min.js'
	}

	anonymous {
		resource url:'/js/flatize/jquery.min.js'
		resource url:'/js/flatize/bootstrap.min.js'
		resource url:'/js/flatize/bootstrap-hover-dropdown.min.js'
		resource url:'/js/flatize/owl-carousel/owl.carousel.js'
		resource url:'/js/flatize/modernizr.custom.js'
		resource url:'/js/flatize/masonry.pkgd.min.js'
		resource url:'/js/flatize/jquery.stellar.js'
		resource url:'/js/flatize/jquery.pricefilter.js'
		resource url:'/js/flatize/jquery.bxslider.min.js'
		resource url:'/js/flatize/mediaelement-and-player.js'
		resource url:'/js/flatize/waypoints.min.js'
		resource url:'/js/flatize/theme.plugins.js'
		resource url:'/js/flatize/theme.js'
	}

	merchantregister {

		resource url:'/js/flatize/merchantRegister.js'
		resource url:'/js/flatize/bootstrap.file-input.js'
		
	}




	metrouistyle {
		//dependsOn "stylesheets"
		resource url:'/css/metro/metro-bootstrap.css'
		resource url:'/css/metro/metro-bootstrap-responsive.css'
	}

	metrouijs {

		resource url:'/js/jquery.min.js',exclude:'minify'
		resource url:'/js/jquery-ui.min.js',exclude:'minify'
		resource url:'js/metro/metro.min.js',exclude:'minify'
		resource url:'js/metro/wizard.js',exclude:'minify'
	}

	fuelstyle {
		dependsOn "stylesheets"
		resource url:'/css/fuelfx/fuelux.min.css'
	}

	fueljs {
		dependsOn "homepage"
		resource url:'js/metro/wizard.js',exclude:'minify'
		resource url:'js/richeditor/advanced.js',exclude:'minify'
		resource url:'js/richeditor/wysihtml5-0.3.0.min.js',exclude:'minify'			
	}

	timeline {
		dependsOn "metro"
		resource url:'/js/flatize/timeline.js'
		resource url:'/js/flatize/profile.js'
	}

	metro {
		resource url:'/css/flatize/metro/style.css'
		//resource url:'/css/flatize/metro/metro-bootstrap.css'
		//resource url:'/css/flatize/metro/metro-bootstrap-responsive.css'
	}
}