class UrlMappings {

	static mappings = {
		"/gimages/${imageId}-${size}.${type}" {
			controller = 'dbContainerImage'
			action = 'index'
		}
		
		"/$controller/$action?/$id?"(){
			constraints {
				// apply constraints here
			}
		}

		"/viewImage/$filename" {
			controller = "domainFiles"
			action = "downloadFile"
		}
		
		"/index.gsp"(view:"/index")
		"/"(view:"/index")
      
		"/contactus"(view:"contactus")
		"/register" (view:"register")
		"/cart" (view:"cart")
		"/detail" (view:"/detail")
		"/merregister" (view:"/merchant/register")
		"/busregister" (view:"/merchant/register")
	
	
        "/category/create"(controller: 'category',action: 'createCategory')
        "/category/delete"(controller: 'category',action: 'deleteCategory')
	
		
			
			"500"(view:'/error')
			
	}
	
}
