import grails.converters.JSON


class JsonFilters {
	def filters={

		JsonFilters(controller:'location|shopper|merchant|profile', action:'*') {
			after = { model ->
				//	def accept = request.getHeader('Accept')

				//if (!accept.contains('application/json')) { return true }


				//				def artefact = grailsApplication .getArtefactByLogicalPropertyName("Controller", controllerName)
				//	println "Filter is accepting requests $model  .. $accept for arteface $artefact"
				//				if (!artefact) {
				//					return true
				//				}

				//	def isJsonified = artefact.clazz.declaredFields.find { it.name == 'jsonify' } != null


				//def jsonified = isJsonified ? artefact.clazz?.jsonify : []

				//if (actionName in jsonified || '*' in jsonified) {
				// check if we can unwrap the model (such in the case of a show)

				String resp=""
				if (model.size() == 1) {
					def nested = model.find { true }.value
					resp= nested as JSON
				}
				else { resp= model as JSON }

				if(params.callback) {
					resp = params.callback + "(" + resp + ")"
				}

				render (contentType: "application/json", text:resp)



				return false

				//}
				//return true

			}


		}
	}
}



