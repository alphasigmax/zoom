package com.luckydog.service

import com.mongodb.DBObject
import com.mongodb.gridfs.GridFSDBFile
import org.springframework.beans.factory.InitializingBean;

import com.mongodb.BasicDBObject;
import com.mongodb.Mongo
import com.mongodb.gridfs.GridFS
import com.mongodb.gridfs.GridFSInputFile
import org.springframework.data.mongodb.core.query.Query;

class GridfsService {
    def mongo
    def gridfs
    def grailsApplication

    void afterPropertiesSet() {
    }

    boolean saveFile(arrayBytes, contentType, uid, _fileName, int width = -1, int height = -1) {


        String fileName = uid + _fileName

        try {
            if (getGridFile().findOne(fileName) == null) {
                println "saving file directly $fileName"
            } else {
                println "Removing old file $fileName and uploading new file"
                getGridFile().remove(fileName)
            }
            GridFSInputFile inputFile = getGridFile().createFile(arrayBytes)
            inputFile.setContentType(contentType)
            inputFile.setFilename(fileName)
            BasicDBObject opts = new BasicDBObject("owner", "merchant")
            opts.put("id", uid)
            if (width > 0)
                opts.put("width", width)
            if (height > 0)
                opts.put("height", height)
            opts.put("filename", _fileName)
            inputFile.setMetaData(opts)
            inputFile.save()
        } catch (Exception ex) {
            println ex
            return false
        }
        return true
    }

    def save(inputStream, contentType, filename, gridfs) {
        //gridfs = new GridFS(mongo.getDB(grailsApplication.config.mongodb.databaseName))

    }

    def retrieveFile(String id) {
        println('++++++++++++under retrieve File')
        BasicDBObject query = new BasicDBObject();
        query.put("metadata.id", id);
        GridFSDBFile imageForOutput = getGridFile().findOne(query);
        return imageForOutput;
    }

    def deleteFile(String filename) {
        getGridFile().remove(filename)
    }


    def getImages(def adid, int width, int height) {
        BasicDBObject query = new BasicDBObject("metadata.id", adid)
        if (width > 0) query.put("metadata.width", width)
        if (height > 0) query.put("metadata.height", height)
        GridFSDBFile files = getGridFile().findOne(query);
        return files
    }

//	def getFilesList() {
//
//		def cursor = getGridFile().getFileList()
//		cursor.toArray()
//	}

    private def getGridFile() {

        if (!gridfs)
            gridfs = new GridFS(mongo.getDB(grailsApplication.config.mongodb.databaseName))

        return gridfs
    }

}
