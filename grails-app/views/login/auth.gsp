<!DOCTYPE html>
<html lang="en" ng-app="haggel">
<head>
	<meta name="layout" content="homepage" />
	<title>Login to Haggell</title>

	


</head>
<body>
	
	
				
<div style="width:80%;aligh:center">
	
	     <div  id="loginpop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"  ng-controller="hgLoginCtrl">
       <div class="modal-dialog">
               <form class="form-horizontal" role="form" novalidate="novalidate" name="loginform" ng-submit="loginUser(loginform.$valid)">
                 <div class="modal-content">
                   <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                     <h4 class="modal-title">Login here</h4>
                   </div>
                   <div class="modal-body">
                   
                <div class="form-group" 
                  ng-class="{ 'has-error' : loginform.email.$invalid && !loginform.email.$pristine, 'has-success': loginform.email.$valid && !loginform.email.$pristine}">
                <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                <div class="col-sm-10">
                  <input type="email" ng-model="loginRequest.email" name="email" class="form-control" id="inputEmail3" placeholder="Email" required />
                </div>
                </div>
                <div class="form-group" 
                  ng-class="{ 'has-error' : loginform.password.$invalid && !loginform.password.$pristine, 'has-success': loginform.password.$valid && !loginform.password.$pristine}">
                <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
                <div class="col-sm-10">
                  <input type="password" ng-model="loginRequest.password" class="form-control" id="password" name="password" placeholder="Password" required />
                </div>
                </div>
                <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                  <div class="checkbox">
                  <label>
                    <input type="checkbox"> Remember me
                  </label>
                  </div>
                </div>
                </div>
              
                    
                   </div>
                   <div class="modal-footer">
                     <button type="button" class="btn btn-default" data-dismiss="modal">Reset</button>
                     <button type="submit" class="btn btn-info">Login</button>
                   </div>
                 </div><!-- /.modal-content -->
              </form>

       </div><!-- /.modal-dialog -->
     </div><!-- /.modal -->


</div>


	

	
</body>
</html>