<html>
<head>

<meta name="layout" content="homepage" />
<title>Register your business</title>

  <style type="text/css">
     #map-canvas { height: 400px; width:100%; margin: 10px; padding: 0;}
    </style>
</head>
<body class="fuelux">
<script type="text/javascript"
      src="https://maps.googleapis.com/maps/api/js?xkey=AIzaSyCh0ZRu308fMQCk8qGafPs7jyYWNenl-rI">
    </script>
    
   
	<div id="register" class="container" ng-controller="hgMerchRegCtrl">

			<script type="text/ng-template" id="registerSuccessModal.html">

 			        <div class="modal-header model-success">
 			        	<button type="button" class="close" data-dismiss="modal" aria-hidden="true" ng-click="cancel()">
							<span class="fa-stack fa-lg"> <i class="fa fa-circle fa-inverse fa-stack-2x"></i> <i class="fa fa-stack-1x fa-times"></i></span>
						</button>
 			            <h3 class="modal-title">Registration Success</h3>
 			        </div>
 			        <div class="modal-body">
 			            <p>You are logged in successfully </p>
 			            <p>You can navigate to home page</p>
 			        </div>
 			</script>

 			<script type="text/ng-template" id="registerFailureModal.html">

 			        <div class="modal-header model-error">
 			        	<button type="button" class="close" data-dismiss="modal" aria-hidden="true" ng-click="cancel()">
							<span class="fa-stack fa-lg"> <i class="fa fa-circle fa-inverse fa-stack-2x"></i> <i class="fa fa-stack-1x fa-times"></i></span>
						</button>
 			            <h3 class="modal-title">Registration Failed</h3>
 			        </div>
 			        <div class="modal-body">
 			            <p>{{message}}</p>
 			        </div>
 			</script>

		

			<div class="wizard" data-initialize="wizard" id="myWizard">

				<ul class="steps">
					<li data-step="1" class="active"><span class="badge">1</span>Profile<span
						class="chevron"></span></li>
					<li data-step="2"><span class="badge">2</span>Contact<span
						class="chevron"></span></li>
					<li data-step="3"><span class="badge">3</span>Location<span
						class="chevron"></span></li>
				</ul>
				<div class="actions">
					<button type="button" class="btn btn-default btn-prev">
						<span class="fa fa-angle-left glyphicon glyphicon-arrow-left"></span>Prev
					</button>
					<button type="button" class="btn btn-default btn-next" data-last="Complete">
						Next<span
							class="fa fa-angle-right glyphicon glyphicon-arrow-right"></span>
					</button>
				</div>
				<div class="step-content">
					<div class="step-pane active sample-pane alert" data-step="1">
					<form role="form" name="merchprofform" novalidate="" ng-submit="submitCheck(merchprofform,$event)">
						<h4>Setup Profile</h4>
					
						<div class="form-group"
							ng-class="{ 'has-error' : merchprofform.email.$invalid && merchprofform.$submitted, 'has-success': merchprofform.email.$valid && !merchprofform.email.$pristine}">
							<label for="inputusername">Email</label> 
							<input type="email"	class="form-control input-lg" id="inputusername" name='email'
								placeholder="Enter your email" ng-model="regRequest.username" ng-required="true">
							<span ng-show="merchprofform.email.$invalid && merchprofform.$submitted" class="help-block">required</span>

						</div>


						<div class="form-group"
							ng-class="{ 'has-error' : merchprofform.password.$invalid && merchprofform.$submitted, 'has-success': merchprofform.password.$valid && !merchprofform.password.$pristine}">
							<label for="inputpassword">Password</label> <input
								type="password" class="form-control input-lg" id="inputpassword"
								name='password' placeholder="Enter your password"
								ng-model="regRequest.password" ng-required="true">
							<span ng-show="merchprofform.password.$invalid && merchprofform.$submitted" class="help-block">required</span>
						</div>


						<div class="form-group"
							ng-class="{ 'has-error' : merchprofform.description.$invalid && merchprofform.$submitted, 'has-success': merchprofform.description.$valid && !merchprofform.description.$pristine}">
							<label for="description">Describe your business</label>
							<textarea class="form-control" rows="7" name="description"></textarea>
							<span ng-show="merchprofform.description.$invalid && merchprofform.$submitted" class="help-block">required</span>
						</div>


						<div class="form-group"
							ng-class="{ 'has-error' : merchprofform.logo.$invalid && merchprofform.$submitted, 'has-success': merchprofform.logo.$valid && !merchprofform.logo.$pristine}">
							<label for="logo">Upload your logo</label> <input type="file"
								title="Search for a file to add" name="logo" class="btn-primary">
						</div>


						</form>
						
					</div>
					<div class="step-pane sample-pane bg-info alert" data-step="2">
						<form role="form" name="merchcontform" novalidate="" ng-submit="submitCheck(merchcontform,$event)">
						<h4>Your contact details</h4>
						
						<div class="form-group"
							ng-class="{ 'has-error' : merchcontform.phone.$invalid && merchcontform.$submitted, 'has-success': merchcontform.phone.$valid && !merchcontform.phone.$pristine}">
							<label for="phone">Phone Number</label> <input type="text"
								ui-mask="{{'(999) 999-9999'}}" class="form-control input-lg"
								id="phone" name='phone' placeholder="Enter your phone number"
								ng-model="regRequest.address.phoneNumber" ng-required="true">
							<span ng-show="merchcontform.phone.$invalid && merchcontform.$submitted" class="help-block">required</span>
						</div>


						<div class="form-group"
							ng-class="{ 'has-error' : merchcontform.url.$invalid && merchcontform.$submitted, 'has-success': merchcontform.url.$valid && !merchcontform.url.$pristine}">
							<label for="url">Website</label> <input type="url"
								class="form-control input-lg" id="url" name='url'
								placeholder="Enter your Website address"
								ng-model="regRequest.address.website" ng-required="true">
							<span ng-show="merchcontform.url.$invalid && merchcontform.$submitted" class="help-block">required</span>
						</div>

						</form>

					
					</div>
					<div class="step-pane sample-pane bg-danger alert" data-step="3">
						<form role="form" name="merchlocform" novalidate="" ng-submit="submitCheck(merchlocform,$event)">
						<p class="text-danger" ng-show="!merchlocform.$valid && merchlocform.$submitted">Complete all required fields</p>
						<h4>Your Location</h4>
						
						<div class="form-group"
							ng-class="{ 'has-error' : merchlocform.country.$invalid && merchlocform.$submitted, 'has-success': merchlocform.country.$valid && !merchlocform.country.$pristine}">
							<label for="country">Country</label> 
							<input type="text" ng-model="regRequest.country" placeholder="Country" typeahead="country for country in getCountry($viewValue)" typeahead-loading="loadingCountry" class="form-control" ng-required="true">
    						<i ng-show="loadingCountry" class="glyphicon glyphicon-refresh"></i>
    						<span ng-show="merchcontform.country.$invalid && merchcontform.$submitted" class="help-block">required</span>

						</div>

						<div class="form-group"
							ng-class="{ 'has-error' : merchlocform.city.$invalid && merchlocform.$submitted, 'has-success': merchlocform.city.$valid && !merchlocform.city.$pristine}">
							<label for="city">City</label> 
							<input type="text" ng-model="regRequest.address.city" placeholder="City" typeahead="city.cityName for city in getCity($viewValue)" typeahead-loading="loadingCity" class="form-control" ng-required="true" >
    						<i ng-show="loadingCity" class="glyphicon glyphicon-refresh"></i>
    						<span ng-show="merchcontform.city.$invalid && merchcontform.$submitted" class="help-block">required</span>
    						
						</div>
						
						<div class="form-group"
							ng-class="{ 'has-error' : merchlocform.address.$invalid && merchlocform.$submitted, 'has-success': merchlocform.address.$valid && !merchlocform.address.$pristine}">
							<label for="address">Address</label> <input type="text"
								class="form-control input-lg" id="address" name='address'
								placeholder="Enter your address" ng-model="regRequest.address.addressOne"
								ng-required="true">
							<span ng-show="merchlocform.address.$invalid && merchlocform.$submitted" class="help-block">required</span>
						</div>


						<div id="map-canvas"></div>
						</form>
					
					</div>
				</div>


			</div>

<!--  -->
   <script type="text/javascript">
   var map;
      function initialize() {
        var mapOptions = {
          center: { lat: -34.397, lng: 150.644},
          zoom: 8
        };
        map = new google.maps.Map(document.getElementById('map-canvas'),
            mapOptions);
        console.log("maps init ..")
      }
    google.maps.event.addDomListener(window, 'load', initialize);

  
    </script>
    
	</div>
	
	<r:require module="fueljs" />
	<r:require module="merchantregister" />
</body>
</html>