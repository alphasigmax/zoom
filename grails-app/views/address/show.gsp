
<%@ page import="com.luckydog.common.Address" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="homepage" />
		<g:set var="entityName" value="${message(code: 'address.label', default: 'Address')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
	
		<div class="container">
		
		<div class="nav" role="navigation">
			<ul>
				
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-address" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list address">
			
				<g:if test="${addressInstance?.addressOne}">
				<blockquote>
				<li class="fieldcontain">
					<span id="addressOne-label" class="property-label"><g:message code="address.addressOne.label" default="Address One" /></span>
					
						<span class="property-value" aria-labelledby="addressOne-label"><g:fieldValue bean="${addressInstance}" field="addressOne"/></span>
					
				</li>
				</blockquote>
				</g:if>
			
				<g:if test="${addressInstance?.addressTwo}">
				<blockquote>
				<li class="fieldcontain">
					<span id="addressTwo-label" class="property-label"><g:message code="address.addressTwo.label" default="Address Two" /></span>
					
						<span class="property-value" aria-labelledby="addressTwo-label"><g:fieldValue bean="${addressInstance}" field="addressTwo"/></span>
					
				</li>
				</blockquote>
				</g:if>
			
				<g:if test="${addressInstance?.city}">
				<blockquote>
				<li class="fieldcontain">
					<span id="city-label" class="property-label"><g:message code="address.city.label" default="City" /></span>
					
						<span class="property-value" aria-labelledby="city-label"><g:link controller="cityLocation" action="show" id="${addressInstance?.city?.id}">${addressInstance?.city?.encodeAsHTML()}</g:link></span>
					
				</li>
				</blockquote>
				</g:if>
			
				<g:if test="${addressInstance?.description}">
				<blockquote>
				<li class="fieldcontain">
					<span id="description-label" class="property-label"><g:message code="address.description.label" default="Description" /></span>
					
						<span class="property-value" aria-labelledby="description-label"><g:fieldValue bean="${addressInstance}" field="description"/></span>
					
				</li>
				</blockquote>
				</g:if>
			
				<g:if test="${addressInstance?.email}">
				<blockquote>
				<li class="fieldcontain">
					<span id="email-label" class="property-label"><g:message code="address.email.label" default="Email" /></span>
					
						<span class="property-value" aria-labelledby="email-label"><g:fieldValue bean="${addressInstance}" field="email"/></span>
					
				</li>
				</blockquote>
				</g:if>
			
				<g:if test="${addressInstance?.fax}">
				<blockquote>
				<li class="fieldcontain">
					<span id="fax-label" class="property-label"><g:message code="address.fax.label" default="Fax" /></span>
					
						<span class="property-value" aria-labelledby="fax-label"><g:fieldValue bean="${addressInstance}" field="fax"/></span>
					
				</li>
				</blockquote>
				</g:if>
			
				<g:if test="${addressInstance?.isPrimary}">
				<blockquote>
				<li class="fieldcontain">
					<span id="isPrimary-label" class="property-label"><g:message code="address.isPrimary.label" default="Is Primary" /></span>
					
						<span class="property-value" aria-labelledby="isPrimary-label"><g:formatBoolean boolean="${addressInstance?.isPrimary}" /></span>
					
				</li>
				</blockquote>
				</g:if>
			
				<g:if test="${addressInstance?.phone}">
				<blockquote>
				<li class="fieldcontain">
					<span id="phone-label" class="property-label"><g:message code="address.phone.label" default="Phone" /></span>
					
						<g:each in="${addressInstance.phone}" var="p">
						<span class="property-value" aria-labelledby="phone-label"><g:link controller="phone" action="show" id="${p.id}">${p?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</blockquote>
				</g:if>
			
				<g:if test="${addressInstance?.postCode}">
				<blockquote>
				<li class="fieldcontain">
					<span id="postCode-label" class="property-label"><g:message code="address.postCode.label" default="Post Code" /></span>
					
						<span class="property-value" aria-labelledby="postCode-label"><g:fieldValue bean="${addressInstance}" field="postCode"/></span>
					
				</li>
				</blockquote>
				</g:if>
			
				<g:if test="${addressInstance?.website}">
				<blockquote>
				<li class="fieldcontain">
					<span id="website-label" class="property-label"><g:message code="address.website.label" default="Website" /></span>
					
						<span class="property-value" aria-labelledby="website-label"><g:fieldValue bean="${addressInstance}" field="website"/></span>
					
				</li>
				</blockquote>
				</g:if>
			
			</ol>
			<g:form url="[resource:addressInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${addressInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
		</div>
	</body>
</html>
