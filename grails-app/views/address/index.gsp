
<%@ page import="com.luckydog.common.Address" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="homepage" />
		<g:set var="entityName" value="${message(code: 'address.label', default: 'Address')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
	<div class="container">
		
		<div class="nav" role="navigation">
			<ul>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		
		<div id="list-address" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table width="100%">
			<thead>
					<tr>
					
						<g:sortableColumn property="addressOne" title="${message(code: 'address.addressOne.label', default: 'Address One')}" />
					
						<g:sortableColumn property="addressTwo" title="${message(code: 'address.addressTwo.label', default: 'Address Two')}" />
					
						<th><g:message code="address.city.label" default="City" /></th>
					
						<g:sortableColumn property="description" title="${message(code: 'address.description.label', default: 'Description')}" />
					
						<g:sortableColumn property="email" title="${message(code: 'address.email.label', default: 'Email')}" />
					
						<g:sortableColumn property="fax" title="${message(code: 'address.fax.label', default: 'Fax')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${addressInstanceList}" status="i" var="addressInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${addressInstance.id}">${fieldValue(bean: addressInstance, field: "addressOne")}</g:link></td>
					
						<td>${fieldValue(bean: addressInstance, field: "addressTwo")}</td>
					
						<td>${fieldValue(bean: addressInstance, field: "city")}</td>
					
						<td>${fieldValue(bean: addressInstance, field: "description")}</td>
					
						<td>${fieldValue(bean: addressInstance, field: "email")}</td>
					
						<td>${fieldValue(bean: addressInstance, field: "fax")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${addressInstanceCount ?: 0}" />
			</div>
		</div>
		
		</div>
	</body>
</html>
