<%@ page import="com.luckydog.common.Address" %>



<div class="fieldcontain ${hasErrors(bean: addressInstance, field: 'addressOne', 'error')} ">
	<label for="addressOne">
		<g:message code="address.addressOne.label" default="Address One" />
		
	</label>
	<g:textField name="addressOne" value="${addressInstance?.addressOne}" />

</div>

<div class="fieldcontain ${hasErrors(bean: addressInstance, field: 'addressTwo', 'error')} ">
	<label for="addressTwo">
		<g:message code="address.addressTwo.label" default="Address Two" />
		
	</label>
	<g:textField name="addressTwo" value="${addressInstance?.addressTwo}" />

</div>

<div class="fieldcontain ${hasErrors(bean: addressInstance, field: 'city', 'error')} ">
	<label for="city">
		<g:message code="address.city.label" default="City" />
		
	</label>
	<g:select id="city" name="city.id" from="${com.luckydog.location.CityLocation.list(max:100)}" optionKey="id" required="" value="${addressInstance?.city?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: addressInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="address.description.label" default="Description" />
		
	</label>
	<g:textField name="description" value="${addressInstance?.description}" />

</div>

<div class="fieldcontain ${hasErrors(bean: addressInstance, field: 'email', 'error')} ">
	<label for="email">
		<g:message code="address.email.label" default="Email" />
		
	</label>
	<g:textField name="email" value="${addressInstance?.email}" />

</div>

<div class="fieldcontain ${hasErrors(bean: addressInstance, field: 'fax', 'error')} ">
	<label for="fax">
		<g:message code="address.fax.label" default="Fax" />
		
	</label>
	<g:textField name="fax" value="${addressInstance?.fax}" />

</div>

<div class="fieldcontain ${hasErrors(bean: addressInstance, field: 'isPrimary', 'error')} ">
	<label for="isPrimary">
		<g:message code="address.isPrimary.label" default="Is Primary" />
		
	</label>
	<g:checkBox name="isPrimary" value="${addressInstance?.isPrimary}" />

</div>

<div class="fieldcontain ${hasErrors(bean: addressInstance, field: 'phone', 'error')} ">
	<label for="phone">
		<g:message code="address.phone.label" default="Phone" />
		
	</label>
	<g:select name="phone" from="${com.luckydog.common.Phone.list()}" multiple="multiple" optionKey="id" size="5" required="" value="${addressInstance?.phone*.id}" class="many-to-many"/>

</div>

<div class="fieldcontain ${hasErrors(bean: addressInstance, field: 'postCode', 'error')} ">
	<label for="postCode">
		<g:message code="address.postCode.label" default="Post Code" />
		
	</label>
	<g:textField name="postCode" value="${addressInstance?.postCode}" />

</div>

<div class="fieldcontain ${hasErrors(bean: addressInstance, field: 'website', 'error')} ">
	<label for="website">
		<g:message code="address.website.label" default="Website" />
		
	</label>
	<g:textField name="website" value="${addressInstance?.website}" />

</div>

