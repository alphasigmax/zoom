				
<div class="login-wrapper">
	
	<ul role="tablist" class="nav nav-tabs second-tabs">
		<li class="active"><a data-toggle="tab" role="tab" href="#login">Login</a></li>
		<li><a data-toggle="tab" role="tab" href="#register">Register</a></li>

	</ul>
	<div class="tab-content">
		<div id="login" class="tab-pane active" ng-controller="hgLoginCtrl">
			<form id="form-login" role="form" name="loginform" novalidate="novalidate"   ng-submit="loginUser(loginform.$valid)">
			
	             
					<div class="form-group" 
					ng-class="{ 'has-error' : loginform.j_username.$invalid && !loginform.j_username.$pristine, 'has-success': loginform.j_username.$valid && !loginform.j_username.$pristine}">
					<label for="inputusername">Username</label> <input type="email"
						class="form-control input-lg" id="inputusername" name='j_username'
						placeholder="Username" ng-model="loginRequest.j_username" required>
				</div>
				<div class="form-group"
				 ng-class="{ 'has-error' : loginform.j_password.$invalid && !loginform.j_password.$pristine, 'has-success': loginform.j_password.$valid && !loginform.j_password.$pristine}">
					<label for="inputpassword">Password</label> <input type="password"
						class="form-control input-lg" id="inputpassword"  name='j_password'
						placeholder="Password" ng-model="loginRequest.j_password" required>
				</div>
				<ul class="list-inline">
					<li><button type="submit" class="btn btn-white">Log in</button></li>
					<li><a href="#">Request new password</a></li>
				</ul>
				
				<
			</form>

			<div ng-controller="hgPassLoginCtrl">
				<a class="btn btn-block btn-social btn-twitter" ng-click="authenticate('twitter');">
				   <i class="fa fa-twitter"></i> Sign in with Twitter
				 </a>

				<a class="btn btn-block btn-social btn-facebook" ng-click="authenticate('facebook')">
				   <i class="fa fa-facebook"></i> Sign in with Facebook
				 </a>
				 
				  <a class="btn btn-block btn-social btn-google-plus" ng-click="authenticate('google');">
				   <i class="fa fa-google-plus"></i> Sign in with Google
				 </a>
			</div>
 			
 			<script type="text/ng-template" id="loginSuccessModal.html">

 			        <div class="modal-header model-success">
 			        	<button type="button" class="close" data-dismiss="modal" aria-hidden="true" ng-click="cancel()">
							<span class="fa-stack fa-lg"> <i class="fa fa-circle fa-inverse fa-stack-2x"></i> <i class="fa fa-stack-1x fa-times"></i></span>
						</button>
 			            <h3 class="modal-title">Login Success</h3>
 			        </div>
 			        <div class="modal-body">
 			            <p>You are logged in successfully </p>
 			            <p>You can navigate to home page</p>
 			        </div>
 			</script>

 			<script type="text/ng-template" id="loginFailureModal.html">

 			        <div class="modal-header model-error">
 			        	<button type="button" class="close" data-dismiss="modal" aria-hidden="true" ng-click="cancel()">
							<span class="fa-stack fa-lg"> <i class="fa fa-circle fa-inverse fa-stack-2x"></i> <i class="fa fa-stack-1x fa-times"></i></span>
						</button>
 			            <h3 class="modal-title">Login Failed</h3>
 			        </div>
 			        <div class="modal-body">
 			            <p>Password or username is wrong ,please recheck your details </p>
 			        </div>
 			</script>	
  
		</div>
		<div id="register" class="tab-pane" ng-controller="hgRegisterCtrl">

			<script type="text/ng-template" id="registerSuccessModal.html">

 			        <div class="modal-header model-success">
 			        	<button type="button" class="close" data-dismiss="modal" aria-hidden="true" ng-click="cancel(true)">
							<span class="fa-stack fa-lg"> <i class="fa fa-circle fa-inverse fa-stack-2x"></i> <i class="fa fa-stack-1x fa-times"></i></span>
						</button>
 			            <h3 class="modal-title">Registration Success</h3>
 			        </div>
 			        <div class="modal-body">
 			            <p>You are Regsitered successfully </p>
 			            <p>You can navigate to home page</p>
 			        </div>
 			</script>

 			<script type="text/ng-template" id="registerFailureModal.html">

 			        <div class="modal-header model-error">
 			        	<button type="button" class="close" data-dismiss="modal" aria-hidden="true" ng-click="cancel()">
							<span class="fa-stack fa-lg"> <i class="fa fa-circle fa-inverse fa-stack-2x"></i> <i class="fa fa-stack-1x fa-times"></i></span>
						</button>
 			            <h3 class="modal-title">Registration Failed</h3>
 			        </div>
 			        <div class="modal-body">
 			            <p>{{message}}</p>
 			        </div>
 			</script>
			
			<form id="form-register" role="form" name="registerForm" ng-submit="registerUser(registerForm.$valid)">
				<h4>Register</h4>
						<div class="form-group" ng-class="{ 'has-error' : registerForm.username.$invalid && !registerForm.username.$pristine, 'has-success': registerForm.username.$valid && !registerForm.username.$pristine}">
					<label for="inputname">Name</label> <input type="text"
						class="form-control input-lg" id="inputname" ng-model="registerRequest.username" name="username"
						placeholder="Your Name" required>
				</div>
				
					<div class="form-group" ng-class="{ 'has-error' : registerForm.email.$invalid && !registerForm.email.$pristine, 'has-success': registerForm.email.$valid && !registerForm.email.$pristine}">
					<label for="inputusername">Email</label> <input type="email"
						class="form-control input-lg" id="inputusername" ng-model="registerRequest.email" name="email"
						placeholder="Your email" required>
				</div>
				<div class="form-group"  ng-class="{ 'has-error' : registerForm.password.$invalid && !registerForm.password.$pristine, 'has-success': registerForm.password.$valid && !registerForm.password.$pristine}">
					<label for="inputpassword">Password</label> <input type="password"
						class="form-control input-lg" id="inputpassword" ng-model="registerRequest.password"  name="password"
						placeholder="Password" required>
				</div>
				    <div class="form-group">
                    <div class="col-lg-offset-2 col-lg-10">
                      <div class="checkbox">
                        <label>
                          <input type="checkbox" name="terms" ng-model="registerRequest.terms" required> I Agree to Terms & Conditions
                        </label>
                      </div>
                    </div>
                </div>
				<button type="submit" class="btn btn-white">Register</button>
			</form>
		</div>

	</div>


</div>

