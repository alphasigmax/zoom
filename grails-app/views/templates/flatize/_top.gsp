		<div id="top">
				<div class="container">
					<p class="pull-left text-note" style="text:white">	
			
			<!--  Change the foreground to white -->
				<ul class="nav navbar-nav nav-white">
					<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Select City</a>
								<ul class="dropdown-menu">
									<li class="dropdown-submenu">
										<a href="#">Victoria</a>
										<ul class="dropdown-menu">
											<li><a href="#">Melbourne</a></li>
											<li><a href="#">Geelong</a></li>
											<li><a href="#">Ballarat</a></li>
											<li><a href="#">Mornington Peninsula</a></li>
										</ul>
									</li>
									
									<li class="dropdown-submenu">
										<a href="#">New South Wales</a>
										<ul class="dropdown-menu">
											<li><a href="#">Sydney</a></li>
											<li><a href="#">Aubury</a></li>
											<li><a href="#">Wagga Wagga</a></li>
											<li><a href="#">Brooklyn</a></li>
										</ul>
									</li>
									
										<li class="dropdown-submenu">
										<a href="#">Queensland</a>
										<ul class="dropdown-menu">
											<li><a href="#">Cairns</a></li>
											<li><a href="#">Airlie Beach</a></li>
											<li><a href="#">Byron Bay</a></li>
											<li><a href="#">Sunshine Coast</a></li>
										</ul>
									</li>
									
																	
								</ul>
							</li>
							</ul>
							
							</p>
					<ul class="nav nav-pills nav-top navbar-right">
						<li class="dropdown langs">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="<g:createLinkTo dir="/"  />images/flatize/flags/en.gif" alt="English"> <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="#"><img src="<g:createLinkTo dir="/"  />images/flatize/flags/ar.gif" alt="AR"></a></li>
								<li><a href="#"><img src="<g:createLinkTo dir="/"  />images/flatize/flags/ca.gif" alt="CA"></a></li>
								<li><a href="#"><img src="<g:createLinkTo dir="/"  />images/flatize/flags/de.gif" alt="DE"></a></li>
								<li><a href="#"><img src="<g:createLinkTo dir="/"  />images/flatize/flags/es.gif" alt="ES"></a></li>
								<li><a href="#"><img src="<g:createLinkTo dir="/"  />images/flatize/flags/et.gif" alt="ET"></a></li>
								<li><a href="#"><img src="<g:createLinkTo dir="/"  />images/flatize/flags/fa.gif" alt="FA"></a></li>
								<li><a href="#"><img src="<g:createLinkTo dir="/"  />images/flatize/flags/fr.gif" alt="FR"></a></li>
								<li><a href="#"><img src="<g:createLinkTo dir="/"  />images/flatize/flags/ja.gif" alt="JA"></a></li>
							</ul>
						</li>
					</ul>
				</div>
			</div>