<html>
<head>

<meta name="layout" content="homepage" />
<title>Create A Deal</title>

<style type="text/css">
#map-canvas {
	height: 400px;
	width: 100%;
	margin: 10px;
	padding: 0;
}
</style>
</head>
<body class="fuelux">
	<script type="text/javascript"
		src="https://maps.googleapis.com/maps/api/js?xkey=AIzaSyCh0ZRu308fMQCk8qGafPs7jyYWNenl-rI">
		
	</script>

	<div id="createdeal" class="container" ng-controller="hgCreateDealCtrl">

		<form id="form-dealCreate" role="form" name="dealCreateForm"
			ng-submit="createDeal(dealCreateForm.$valid)">

			<div class="wizard" data-initialize="wizard" id="myWizard">

				<ul class="steps">
					<li data-step="1" class="active"><span class="badge">1</span>Information<span
						class="chevron"></span></li>
					<li data-step="2"><span class="badge">2</span>Details<span
						class="chevron"></span></li>
					<li data-step="3"><span class="badge">3</span>Contact<span
						class="chevron"></span></li>
				</ul>
				<div class="actions">
					<button class="btn btn-default btn-prev">
						<span class="fa fa-angle-left glyphicon glyphicon-arrow-left"></span>Prev
					</button>
					<button class="btn btn-default btn-next" data-last="Complete">
						Next<span
							class="fa fa-angle-right glyphicon glyphicon-arrow-right"></span>
					</button>
				</div>
				<div class="step-content">
					<div class="step-pane active sample-pane alert" data-step="1">
						<h4>Setup Profile</h4>

						<div class="form-group"
							ng-class="{ 'has-error' : dealCreateForm.inputtitle.$invalid && !dealCreateForm.inputtitle.$pristine, 'has-success': dealCreateForm.inputtitle.$valid && !dealCreateForm.inputtitle.$pristine}">
							<label for="inputtitle">Deal Title</label> <input type="text"
								class="form-control input-lg" id="inputtitle" name='inputtitle'
								placeholder="Enter the title for your deal"
								ng-model="regRequest.inputtitle" required>
						</div>

					
    
    <g:render template="/templates/flatize/richtoolbar" />



						<div class="form-group"
							ng-class="{ 'has-error' : dealCreateForm.description.$invalid && !dealCreateForm.description.$pristine, 'has-success': dealCreateForm.description.$valid && !dealCreateForm.description.$pristine}">
							<label for="description">Describe your deal</label>
							<textarea class="form-control" rows="7" name="description"></textarea>
						</div>


						<div class="form-group"
							ng-class="{ 'has-error' : dealCreateForm.logo.$invalid && !dealCreateForm.logo.$pristine, 'has-success': dealCreateForm.logo.$valid && !dealCreateForm.logo.$pristine}">
							<label for="logo">Upload your logo</label> <input type="file"
								title="Search for a file to add" name="logo" class="btn-primary">
						</div>




					</div>
					<div class="step-pane sample-pane bg-info alert" data-step="2">

						<div >
						<button class="btn btn-default" ng-click="dis_visible = true">Add Discount</button>
						
						  <table class="table table-bordered table-hover table-condensed">
						    <tr style="font-weight: bold">
						      <td style="width:25%">Price</td>
						      <td style="width:25%">Quanity</td>
						      <td style="width:50%">Edit</td>
						    </tr>
						    <tr ng-repeat="discount in discounts">
						    	<form ng-submit="editing = false">
						    	<td>
						    		<span ng-hide="editing" ng-click="editing = true">{{discount.price}}</span>
						    		<input ng-show="editing" class="form-control" type="number" ng-model="discount.price" placeholder="price" ng-required/>
						    	</td>
						    	<td>
						    		<span ng-hide="editing" ng-click="editing = true">{{discount.quantity}}</span>
						    		<input ng-show="editing" class="form-control" type="number" ng-model="discount.quantity" placeholder="quantity" ng-required/>
						    	</td>
						    	<td>
						    		<div class="btn-group">
							    		<button class="btn btn-boot btn-primary" type="button" ng-click="editing = !editing">
							    			<span ng-show="editing">Save</span>
							    			<span ng-hide="editing">Edit</span>
							    		</button>
							    		<button type="butoon" class="btn btn-boot btn-adn" ng-click="removeDiscount($index)">Delete</button>
						    		</div>
						    	</td>
						    	</form>						      
						    </tr>
						  </table>

						  <form class="form-inline" ng-show="dis_visible" ng-submit="addDiscount()">
						  	<div class="form-group">
				            	<label>Price:</label>
				            	<input type="number" class="form-control" ng-model="discountprice" placeholder="Price" ng-required/>
				            </div>
				            <div class="form-group">
				            	<label>Quantity:</label>
				            	<input type="number" class="form-control" ng-model="discountquanity" placeholder="Quantity" ng-required/>
				            </div>
				            <div class="form-group"><button class="btn btn-boot btn-vk" type="submit">Save</button></div>
				          </form>
				          <br/>
						  
						</div>

						<h4>Your contact details</h4>

						<div class="form-group"
							ng-class="{ 'has-error' : dealCreateForm.phone.$invalid && !dealCreateForm.phone.$pristine, 'has-success': dealCreateForm.phone.$valid && !dealCreateForm.phone.$pristine}">
							<label for="phone">Phone Number</label> <input type="text"
								ui-mask="{{'(999) 999-9999'}}" class="form-control input-lg"
								id="phone" name='phone' placeholder="Enter your phone number"
								ng-model="regRequest.phone" required>
						</div>


						<div class="form-group"
							ng-class="{ 'has-error' : dealCreateForm.url.$invalid && !dealCreateForm.url.$pristine, 'has-success': dealCreateForm.url.$valid && !dealCreateForm.url.$pristine}">
							<label for="url">Website</label> <input type="url"
								class="form-control input-lg" id="url" name='emailaddress'
								placeholder="Enter your Website address"
								ng-model="regRequest.url" required>
						</div>




					</div>
					<div class="step-pane sample-pane bg-danger alert" data-step="3">
						<h4>Your Location</h4>

						
						<div class="form-group"
							ng-class="{ 'has-error' : dealCreateForm.country.$invalid && !dealCreateForm.country.$pristine, 'has-success': dealCreateForm.country.$valid && !dealCreateForm.country.$pristine}">
							<label for="country">Country</label> 
							<input type="text"
								class="form-control input-lg" id="country" name='country'
								placeholder="Enter your country" ng-model="regRequest.country"
								required placeholder="Country" typeahead="country for country in getCountry($viewValue)" typeahead-loading="loadingCountry" >
						<i ng-show="loadingCountry" class="glyphicon glyphicon-refresh"></i>
						</div>
						
							
							<div class="form-group"
							ng-class="{ 'has-error' : dealCreateForm.city.$invalid && !dealCreateForm.city.$pristine, 'has-success': dealCreateForm.city.$valid && !dealCreateForm.city.$pristine}">
							<label for="city">City</label> 
							<input type="text"
								class="form-control input-lg" id="city" name='city'
								placeholder="Enter your city" ng-model="regRequest.city"
								required 
								placeholder="City" typeahead="city.cityName for city in getCity($viewValue)" typeahead-loading="loadingCity" 
								>
						<i ng-show="loadingCity" class="glyphicon glyphicon-refresh"></i>
						</div>
						
								
						

						<div class="form-group"
							ng-class="{ 'has-error' : dealCreateForm.address.$invalid && !dealCreateForm.address.$pristine, 'has-success': dealCreateForm.address.$valid && !dealCreateForm.address.$pristine}">
							<label for="address">Address</label> <input type="text"
								class="form-control input-lg" id="address" name='address'
								placeholder="Enter your address" ng-model="regRequest.address"
								required>
						</div>


						<div id="map-canvas"></div>

					</div>
				</div>


			</div>

		</form>
		<!--  -->
		<script type="text/javascript">
			var map;
			function initialize() {
				var mapOptions = {
					center : {
						lat : -34.397,
						lng : 150.644
					},
					zoom : 8
				};
				map = new google.maps.Map(
						document.getElementById('map-canvas'), mapOptions);
				console.log("maps init ..")
			}
			google.maps.event.addDomListener(window, 'load', initialize);
		</script>

	</div>

	<r:require module="fueljs" />
</body>
</html>
