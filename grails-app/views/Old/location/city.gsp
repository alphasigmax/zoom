<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <title>Elementary</title>
    <meta charset="iso-8859-1">

    <!--[if lt IE 9]><script src="scripts/html5shiv.js"></script><![endif]-->
    <!-- homepage scripts -->
    <r:require module="common"/>

    <style>
    #feedback {
        font-size: 1.4em;
    }

    #selectable .ui-selecting {
        background: #FECA40;
    }

    #selectable .ui-selected {
        background: #F39814;
        color: white;
    }

    #selectable {
        list-style-type: none;
        margin: 0;
        padding: 0;
        width: 450px;
    }

    #selectable li {
        margin: 3px;
        padding: 1px;
        float: left;
        width: 100px;
        height: 80px;
        font-size: 4em;
        text-align: center;
    }

    #Oceania {
        border: 0px
    }
    </style>
    <!-- / homepage scripts -->
</head>

<body class="home blog chrome et_includes_sidebar daily-deal full-width">
<div id="main-wrap" class="clearfix">

    <g:render template="/templates/header"/>

    <div id="main">
        <div id="wrapper">

            <!-- content -->
            <div class="wrapper row2">
                <div id="container" class="clear">
                    <!-- content body -->

                    <!-- Services area -->

                    <section class="services clear">

                        <div id="tabs">
                            <ul>
                                <li><a href="#Oceania">Oceania</a></li>
                                <li><a href="#loc">Location</a></li>
                                <li><a href="#terms">Terms</a></li>
                            </ul>

                            <div id="Oceania">
                                <ol id="selectable">
                                    <li class="ui-state-default">1</li>
                                    <li class="ui-state-default">2</li>
                                    <li class="ui-state-default">3</li>
                                    <li class="ui-state-default">4</li>
                                    <li class="ui-state-default">5</li>
                                    <li class="ui-state-default">6</li>
                                    <li class="ui-state-default">7</li>
                                    <li class="ui-state-default">8</li>
                                    <li class="ui-state-default">9</li>
                                    <li class="ui-state-default">10</li>
                                    <li class="ui-state-default">11</li>
                                    <li class="ui-state-default">12</li>
                                </ol> <br>

                            </div>

                            <div id="loc">

                                <div id="gmaps">

                                </div>
                            </div>

                            <div id="terms">
                                T&C
                            </div>
                        </div>

                    </section>


                    <!-- / content body -->
                </div>
            </div>

        </div>
    </div>
</div>
<!-- footer -->
<script>
    $(function () {


        $("#selectable").selectable();
        $("#tabs").tabs();
    });



</script>
</body>
</html>
