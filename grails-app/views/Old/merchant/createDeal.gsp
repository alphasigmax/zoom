﻿<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta name="layout" content="application">
    <link rel="stylesheet" type="text/css"
          href="${resource(dir: 'css', file: 'smartwizard/smart_wizard.css', absolute: true)}"/>
    <link rel="stylesheet" type="text/css"
          href="${resource(dir: 'css', file: 'jquery/jquery-ui-1.10.0.custom.css', absolute: true)}"/>


    <style>
    .centerer {
        margin: 0 auto !important;
        float: none !important;
    }

    #step-1, #step-2, #step-3, #step-4 {
        height: 450px
    }

    /* css for timepicker */
    .ui-timepicker-div .ui-widget-header {
        margin-bottom: 8px;
    }

    .ui-timepicker-div dl {
        text-align: left;
    }

    .ui-timepicker-div dl dt {
        height: 25px;
        margin-bottom: -25px;
    }

    .ui-timepicker-div dl dd {
        margin: 0 10px 10px 65px;
    }

    .ui-timepicker-div td {
        font-size: 90%;
    }

    .ui-tpicker-grid-label {
        background: none;
        border: none;
        margin: 0;
        padding: 0;
    }

    .ui-timepicker-rtl {
        direction: rtl;
    }

    .ui-timepicker-rtl dl {
        text-align: right;
    }

    .ui-timepicker-rtl dl dd {
        margin: 0 65px 10px 10px;
    }

    .metrouicss table tbody tr td {
        padding: 0px;
    }

    </style>

    <title>Zoom Deals</title>

</head>

<body class="metrouicss">

<script src="../js/jquery/jquery-ui-1.10.0.custom.js"></script>

<script src="<g:resource dir="js" file="smartwizard/jquery.smartWizard-2.0.js" absolute="true"/>">

</script>
<script src="<g:resource dir="js" file="/metroui/dialog.js" absolute="true"/>">

</script>
<script src="<g:resource dir="js" file="/jquery/jquery.form.js" absolute="true"/>">

</script>



<script src="../js/jquery/jquery-ui-timepicker-addon.js"></script>
<script src="../js/tinymce/tiny_mce.js"></script>

%{-- --}%%{--<script src="https://maps.googleapis.com/maps/api/js?sensor=false&libraries=places"></script>

<script src="<g:resource dir="js" file="/google/gmaps.js" absolute="true" />">--}%%{--
</script>--}%

<div class="page secondary">

    <g:render template="/templates/header"/>


    <div class="page-region">

        <div class="page-region-content">
            <!-- <div class="grid">
					<div class="row centerer"> -->

            <h1>Create A Deal</h1>

            <form action="<g:createLink controller="adPost" action="saveDeal"/>" autocomplete="on" name="dealForm"
                  id="dealForm">
                <div id="wizard" class="swMain">
                    <ul>
                        <li><a href="#step-1"><label class="stepNumber">1</label> <span
                                class="stepDesc">Description<br/> <small>Describe
                            your deal</small>
                        </span>
                        </a></li>
                        <li><a href="#step-2"><label class="stepNumber">2</label> <span
                                class="stepDesc">Price<br/> <small>Prices &
                            Discounts!</small>
                        </span>
                        </a></li>
                        <li><a href="#step-3"><label class="stepNumber">3</label> <span
                                class="stepDesc">Location<br/> <small>Location of
                            the deal</small>
                        </span>
                        </a></li>
                        <li><a href="#step-4"><label class="stepNumber">4</label> <span
                                class="stepDesc">Fine Print<br/> <small>Terms &
                            Conditions</small>
                        </span>
                        </a></li>
                    </ul>

                    <div id="step-1" class="tallbig">
                        <h3>Describe your deal</h3>

                        %{--<p>--}%
                        %{--<label for="adCategories" class="uname" data-icon="f">Category</label> <br>--}%
                        %{--<!-- g:select name="adCategories" from="${com.luckydog.dealpost.Category.list()}" optionKey="id" size="5" required="required" /-->--}%
                        %{--</p>--}%


                        <div class="input-control text span7">
                            <label for="title" class="youmail" data-icon="m">Title</label> <br> <input id="title"
                                                                                                       name="title"
                                                                                                       required="required"
                                                                                                       type="text"
                                                                                                       class="with-helper"/>

                        </div>


                        <div class="input-control text span7">
                            <label for="description" class="youpasswd" data-icon="n">Description</label> <br>
                            <textarea name="description" style="width: 100%"></textarea>

                        </div>


                        <div class="input-control text span7">
                            <label for="logo" class="youpasswd" data-icon="n">Logo</label> <br> <input id="logo"
                                                                                                       name="logo"
                                                                                                       required="required"
                                                                                                       type="file"/>
                        </div>

                    </div>

                    <div id="step-2">
                        <h3>Price your deal</h3>


                        <div class="input-control text span7">
                            <label for="quantity" class="youmail" data-icon="m">Quantity</label>
                            <input id="quantity" name="quantity"
                                   required="required" type="text"/> <input type="checkbox" alt="unlimited"
                                                                            name="unlimited"> Unlimited
                        </div>


                        <div class="input-control text span7">
                            <label for="priorPrice" class="youmail" data-icon="m">Prior Price</label>
                            <input id="priorPrice" name="priorPrice"
                                   required="required" type="text"/>
                        </div>


                        <div class="input-control text span7">
                            <label for="curPrice" class="youmail" data-icon="m">Current Price</label>
                            <input id="curPrice" name="curPrice"
                                   required="required" type="text"/>
                        </div>

                        <p style="float: right">10%</p>

                        <div class="input-control text span7">
                            <label for="startDate" class="youmail" data-icon="m">Start Date</label>
                            <input id="startDate" name="startDate" required="required"
                                   type="text"/>
                        </div>


                        <div class="input-control text span7">
                            <label for="endDate" class="youmail" data-icon="m">End Date</label>
                            <input id="endDate" name="endDate" required="required"
                                   type="text"/>

                        </div>

                    </div>


                    <div id="step-3">
                        <h3>Where is your deal located</h3>

                        <div class="input-control text span7">
                            <label for="description" class="youpasswd" data-icon="n">Description</label> <br>
                            <input type="text" id="address" name="address" size=75>

                        </div>

                        <div id="map_canvas" style="height:350px;width:100%;display:block"></div>
                        <input type="hidden" name="lonid" id="lonid" value="0">
                        <input type="hidden" name="latid" id="latid" value="0">

                    </div>

                    <div id="step-4">
                        <h3>Terms and conditions</h3>
                    </div>

                </div>

            </form>

            <!--</div>
				</div>  -->

        </div>

    </div>

</div>


<div class="notices" id="status" style="display: none">
    <div class="bg-color-green">

        <div class="notice-header fg-color-white">Registered!</div>

        <div class="notice-text">
            Deal has been created!
        </div>
    </div>
</div>


<script>
    $(document).ready(function () {

        initializeMaps();

        tinyMCE.init({
            theme: "advanced",
            mode: "textareas",
            theme_advanced_resizing: true,
            // plugins : "autolink,lists,pagebreak,style,layer,table,advimage,advlink,emotions,inlinepopups,insertdatetime,preview,media,contextmenu,paste,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
            theme_advanced_buttons1: "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,bullist,numlist,|,formatselect,fontselect,fontsizeselect",
            theme_advanced_buttons2: "",
            theme_advanced_buttons3: "",
            theme_advanced_toolbar_location: "top",
            theme_advanced_toolbar_align: "left",
            theme_advanced_statusbar_location: "bottom"
        });

        var options = {
            success: showResponse,
            dataType: 'json'
            //timeout:   3000
        };

        $('#wizard').smartWizard({onLeaveStep: leaveAStepCallback, onFinish: onFinishCallback, updateHeight: true, onShowStep: mapChecker});

        $('#endDate').datetimepicker();
        $('#startDate').datetimepicker();

        //$('#dealForm').submit(function() {
        //$(this).ajaxSubmit(options);
        //return false;
        //});


        function leaveAStepCallback(obj) {
            var step_num = obj.attr('rel'); // get the current step number
            return validateSteps(step_num); // return false to stay on step and true to continue navigation
        }

        function mapChecker(obj) {
            var step_num = obj.attr('rel'); // get the current step number
            if (step_num == 3 && google) {
                google.maps.event.trigger(window.map, 'resize');
                showCurLoc();
            }
        }

        function onFinishCallback() {
            if (validateAllSteps()) {
                $("#dealForm").ajaxSubmit(options);
                return false;
            }
        }

        // Your Step validation logic
        function validateSteps(stepnumber) {
            var isStepValid = true;
            // validate step 1
            if (stepnumber == 1) {
                // Your step validation logic
                // set isStepValid = false if has errors
                //	alert("validating step one")
            } else if (stepnumber == 2) {
                //alert("validating step two")
            }
            return isStepValid;
        }

        function validateAllSteps() {
            var isStepValid = true;
            // all step validation logic
            //alert("validating all setps")
            tinyMCE.triggerSave();
            return isStepValid;
        }

        function showResponse(responseText, statusText, xhr, $form) {
            //	$("#register").hide();
            if (responseText.errorDescription) {
                showMsg("Registration error",
                        responseText.errorDescription)
            } else {
                $("#dealForm").hide("slow");
                $("#status").fadeTo(500, 100, function () {
                    //$(this).html("You are now registered!");
                })

            }
        }


    });
</script>

</body>
</html>