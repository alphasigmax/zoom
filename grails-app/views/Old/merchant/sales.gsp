﻿<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta name="layout" content="application">
    <title>Zoom Deals</title>

</head>

<body class="metrouicss">

<script
        src="<g:resource dir="js" file="/metroui/dialog.js" absolute="true"/>">

</script>


<script type="text/javascript"
        src="<g:resource dir="js" file="/metroui/buttonset.js" absolute="true"/>"></script>


<div class="page">

    <g:render template="/templates/header"/>


    <div class="page-region">
        <div class="grid">
            <div class="row">
                <div class="span12">
                    <h2>Actions</h2>


                    <button id="deactButton" class="image-button bg-color-orange fg-color-white">
                        Deactivate <i class="icon-history bg-color-red"></i>
                    </button>
                    <button id="delButton" class="image-button bg-color-orange fg-color-white">
                        Delete <i class="icon-cancel bg-color-red"></i>
                    </button>

                    <button id="statsButton" class="image-button bg-color-orange fg-color-white">
                        Statistics <i class="icon-stats-up bg-color-red"></i>
                    </button>

                    <button class="image-button bg-color-orange fg-color-white">
                        Edit <i class="icon-pencil bg-color-red"></i>
                    </button>
                </div>
            </div>
        </div>

        <div class="page-region-content tiles" id="maintiles">

            <div class="tile quadro image" data-template data-adid="{{id}}">

                <div class="tile-content">
                    <img src="images/deal.gif"
                         data-src="<g:createLink controller="adImage" action="index"/>?name={{id}}back460.jpg"/>
                </div>

                <div class="brand bg-color-orange">

                    <p class="text">
                        <a
                                href="<g:createLink controller="dealDetail" action="showDeal"/>?id={{id}}">{{title}}</a>
                    </p>

                    <div class="badge">{{curPrice}}</div>

                </div>

            </div>

        </div>

    </div>

</div>


<script
        src="<g:resource dir="js" file="tempo/tempo.js" absolute="true"/>">

</script>
<script
        src="<g:resource dir="js" file="metroui/jquery/jquery.mousewheel.min.js" absolute="true"/>">

</script>

<script>
    var offset = 0;
    var twitter = Tempo.prepare('maintiles');
    $(document).ready(function () {

        loadMoreData();
        $(window).scroll(
                function () {
                    if ($(window).scrollTop() >= $(document).height() - $(window).height() - 10) {
                        offset = 1;//offset+25
                        loadMoreData();
                    }
                });

        $(document).delegate("div.tile", "click", function () {
            if ($(this).hasClass("selected")) {
                $(this).removeClass("selected")
            } else {
                $(this).addClass("selected")
            }
        });


        $("#delButton").click(function () {

            if (confirm("Are you sure you want to delete these items?")) {

                $('#maintiles').children('.tile').each(function (i) {

                    if ($(this).hasClass("selected")) {
                        console.log($(this).data("adid"));
                        var adid = $(this).data("adid");
                        var divx = $(this)

                        $.ajax("<g:createLink controller="merchant" action="deletead" />?adid=" + adid).done(function (data) {

                            divx.hide('slow', function () {
                                divx.remove();
                            });
                        }).fail(function () {
                                    alert("failed!")
                                });
                    }
                });
            }

        });


        $("#deactButton").click(function () {

            $('#maintiles').children('.tile').each(function (i) {

                if ($(this).hasClass("selected")) {
                    console.log($(this).data("adid"));
                    var adid = $(this).data("adid");
                    var divx = $(this)

                    if (divx.find(".badge").hasClass("paused")) {
                        $.ajax("<g:createLink controller="merchant" action="activatead" />?adid=" + adid).done(function (data) {
                            divx.addClass("quadro").find(".badge").removeClass("paused")
                        }).fail(function () {
                                    alert("failed!")
                                });
                    }
                    else {
                        $.ajax("<g:createLink controller="merchant" action="deactivatead" />?adid=" + adid).done(function (data) {
                            divx.removeClass("quadro").find(".badge").addClass("paused")
                        }).fail(function () {
                                    alert("failed!")
                                });
                    }

                }
            });
        });


    });

    function loadMoreData() {

        $.ajax("<g:createLink controller="merchant" action="showMyItems" />").done(function (data) {
            if (offset > 0) {
                twitter.append(data);
            } else {
                twitter.render(data);
            }

        }).fail(function () {
                    alert("error");
                });

    }
</script>

<script type="text/javascript" src="<g:resource dir="js" file="noty/jquery.noty.js" absolute="true"/>"></script>
<script type="text/javascript" src="<g:resource dir="js" file="noty/layouts/top.js" absolute="true"/>"></script>
<script type="text/javascript" src="<g:resource dir="js" file="noty/layouts/center.js" absolute="true"/>"></script>
<script type="text/javascript" src="<g:resource dir="js" file="noty/layouts/centerLeft.js" absolute="true"/>"></script>
<script type="text/javascript" src="<g:resource dir="js" file="noty/themes/default.js" absolute="true"/>"></script>

</body>
</html>