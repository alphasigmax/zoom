﻿<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>

    <meta name="layout" content="public">

    <link rel="stylesheet" type="text/css"
          href="${resource(dir: 'css', file: 'smartwizard/smart_wizard.css', absolute: true)}"/>
    <title>Zooom Deals</title>
    <script src="https://maps.googleapis.com/maps/api/js?sensor=false&libraries=places"></script>

    <script src="${resource(dir: 'js', file: 'google/gmaps.js', absolute: true)}"></script>

    <script src="${resource(dir: "js", file: "smartwiza rd/jquery.smartWizard-2.0.js", absolute: true)}"></script>
    <script src="${resource(dir: 'js', file: 'metroui/dialog.js', absolute: true)}"></script>
    <script src="${resource(dir: 'js', file: 'jquery/jquery.form.js', absolute: true)}"></script>

</head>

<body>
<!-- TODO get the current location based on HTML5 or IP and center the map to that location
 first use navigator.location and if that is not available use the IP service to get current city and center to that city
 -->


<div class="wrapper white">

    <!-- Inner Page Content // Start -->

    <div class="inner-page">
        <div class="container">
            <div class="page-mainbar login">
                <!-- Login/Register Content -->
                <div class="login-content" style="max-width:990px; padding-top: 30px">

                    <div class="span12">

                        <div id="register">
                            <form action="${createLink(controller: 'merchant', action: 'doregister')}" autocomplete="on"
                                  class="form-horizontal" name="merchantForm" id="merchantForm">
                                <input type="hidden" name="lat" value="0.0"> <input type="hidden" name="lon"
                                                                                    value="0.0">

                                <h1>Register</h1>

                                <div id="wizard" class="swMain tab-content">
                                    <ul>
                                        <li><a href="#step-1"><label class="stepNumber">1</label> <span
                                                class="stepDesc">Details<br/> <small>All your
                                            details</small>
                                        </span>
                                        </a></li>
                                        <li><a href="#step-2"><label class="stepNumber">2</label> <span
                                                class="stepDesc">Contact<br/> <small>Your contact details</small>
                                        </span>
                                        </a></li>

                                        <li><a href="#step-3"><label class="stepNumber">3</label> <span
                                                class="stepDesc">Location<br/> <small>Your Location</small>
                                        </span>
                                        </a></li>

                                    </ul>

                                    <g:render template="step1"/>
                                    <g:render template="step2"/>
                                    <g:render template="step3"/>

                                </div>

                                <p class="change_link">
                                    Already a member ? <a href="#tologin" id="tologin" class="tologin">Go and log in</a>
                                </p>

                                <br> <br>

                                <div class="progress-bar">
                                    <div class="bar bg-color-pink" style="width: 30%"></div>

                                    <div class="bar bg-color-yellow" style="width: 30%"></div>

                                    <div class="bar bg-color-green" style="width: 40%"></div>
                                </div>
                                <br> <br> <br> <br>

                                <div class="span5" style="float: right">
                                    Shoppers: <a
                                        href="${createLink(controller: 'shopper', action: 'signUp')}">Click here to register</a>
                                </div>
                            </form>
                        </div>

                        <div id="login" style="display: none;">
                            <div class="span6">

                                <ul id="mytab" class="nav nav-tabs nav-jsignUpAjaxustify">
                                    <li class="active">
                                        <a href="#login" id="loginDivButton" class="br-red" data-toggle="tab">
                                            <!-- Icon -->
                                            <i class="fa fa-sign-in"></i>
                                            <span>Login</span>
                                        </a>
                                    </li>
                                    <li>
                                </ul>

                                <div class="tab-content">
                                    <g:render template="/templates/login"/>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>

</div>


<div class="notices" id="status" style="display: none">
    <div class="bg-color-green">

        <div class="notice-header fg-color-white">Registered!</div>
        <!-- TODO remove this and login user automatically -->
        <div class="notice-text">
            You are now registered. Please <a href="#tologin" class="tologin">login to continue</a>..
        </div>
    </div>
</div>




<script type="text/javascript">
    //google.load("maps", "3.x", {other_params: "sensor=false", callback:initializeMaps});


    function verifyEmailValidity(formPostUrl) {
        var dataString = $("#username").val();
        $.ajax({
            type: "POST",
            url: formPostUrl,
            data: {email: dataString},
            dataType: "json",
            success: function (data) {
                if (data.resultData == 'true') {
                    $('.showEmailValidationFail').show();
                    $('.buttonNext').addClass('buttonDisabled');
                } else {
                    $('.showEmailValidationFail').hide();
                    $('.buttonNext').removeClass('buttonDisabled');
                }
            }, error: function () {
                $('.buttonNext').addClass('buttonDisabled');
            }
        });
    }

    $(document).ready(
            function () {

                //	$("#merchantForm").validationEngine();

                $('.tologin').click(function () {
                    $("#register").hide();
                    $('#login').show();

                });

                $('#toregister').click(function () {
                    $("#register").show();
                    $('#login').hide();

                });

                var options = {
                    success: showResponse,
                    dataType: 'json'
                    //timeout:   3000
                };

                $('#wizard').smartWizard({
                    onLeaveStep: leaveAStepCallback,
                    onFinish: onFinishCallback,
                    onShowStep: mapChecker
                });


                function mapChecker(obj) {
                    var step_num = obj.attr('rel'); // get the current step number
                    if (step_num == 3 && google) {
                        google.maps.event.trigger(window.map, 'resize');
                        showCurLoc();
                    }
                }


                function onFinishCallback() {


                }

                $('#merchantForm').submit(function () {
                    $(this).ajaxSubmit(options);
                    return false;
                });

                function leaveAStepCallback(obj) {
                    var step_num = obj.attr('rel'); // get the current step number

                    return validateSteps(step_num); // return false to stay on step and true to continue navigation
                }

                function onFinishCallback() {
                    if (validateAllSteps()) {
                        $("#merchantForm").ajaxSubmit(options);
                        return false;
                    }
                }

                // Your Step validation logic
                function validateSteps(stepnumber) {
                    var isStepValid = true;
                    // validate step 1
                    if (stepnumber == 1) {
                        // Your step validation logic
                        // set isStepValid = false if has errors
                        //	alert("validating step one")
                    } else if (stepnumber == 2) {
                        //alert("validating step two")
                    }
                    return isStepValid;
                }

                function validateAllSteps() {
                    var isStepValid = true;
                    // all step validation logic
                    return isStepValid;
                }

                function showResponse(responseText, statusText, xhr, $form) {
                    //	$("#register").hide();
                    if (responseText.errorDescription) {
                        showMsg("Registration error",
                                responseText.errorDescription)
                    } else {
                        $("#merchantForm").hide("slow");
                        $("#status").fadeTo(500, 100, function () {
                            //$(this).html("You are now registered!");
                        })
                    }
                }

                initializeMaps();
            });


</script>

</body>
</html>
