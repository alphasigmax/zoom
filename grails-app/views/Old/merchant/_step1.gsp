<div id="step-1" class="tab-pane fade in active br-brown">

    <div class="form-group">
        <label for="usernamesignup" class="uname" data-icon="m">Merchant Name</label>

        <div class="col-sm-12">
            <input id="usernamesignup" name="name" class="form-control" onblur="vali()" required="required" type="text"
                   placeholder="Your Name"/>
        </div>
    </div>


    <div class="form-group">
        <label for="username" class="youmail" data-icon="m">Email</label>

        <div class="col-sm-12">

            <input id="username" name="username"
                   onblur='verifyEmailValidity("${createLink(controller: 'deal',action: 'checkUniqueEmail')}")'
                   class="form-control" required="required" type="email" placeholder="User name"/>
            <span class="showEmailValidationFail" style="color: red;display: none">Email already exists.</span>

        </div>
    </div>

    <div class="form-group">
        <label for="password" class="youpasswd" data-icon="n">Password</label>

        <div class="col-sm-12">
            <input id="password" name="password" class="form-control" required="required" type="password"
                   placeholder="Choose password"/>
        </div>
    </div>

    <div class="form-group">
        <label for="description" class="youmail" data-icon="m">Describe your business</label>

        <div class="col-sm-12">
            <textarea id="description" name="description" placeholder="Fill discription here"
                      style="height: 55px;width: 663px"></textarea>
            %{--<input id="description" name="description"  class="form-control" required="required" type="text" />--}%
        </div>
    </div>

</div>
