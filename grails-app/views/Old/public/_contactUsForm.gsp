<div class="tab-pane fade br-green" id="contact">
    <!-- Heading -->
    <h2>Contact Us</h2>

    <form role="form">
        <div class="form-group">
            <!-- form input -->
            <input type="text" class="form-control" id="name" placeholder="Name">
        </div>

        <div class="form-group">
            <!-- form input -->
            <input type="email" class="form-control" id="email" placeholder="Email">
        </div>

        <div class="form-group">
            <!-- form textarea -->
            <textarea class="form-control" id="comment" rows="3" placeholder="Comments"></textarea>
        </div>

        <div class="form-group text-center">
            <!-- form Submit and reset button -->
            <button type="submit" class="btn btn-black">Submit</button>&nbsp;
            <button type="reset" class="btn btn-default">Reset</button>
        </div>
    </form>
</div>