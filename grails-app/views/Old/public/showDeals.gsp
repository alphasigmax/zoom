<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="public"/>
    <title>Zoom Deals</title>
    <script type="text/javascript" src="${resource(dir: 'js', file: 'mustache/mustache.js')}"></script>

</head>

<body>

<ul id="showDealsListDiv" style="position: relative; height: 768px;">
    <!-- Add the above used filter names inside div tag. You can add more than one filter names. For image lightbox you need to include "a" tag pointing to image link, along with the class "prettyphoto". -->



    <div id="singleShowDealDiv" class="container-fluid" style="display: none">
        <li class="" style="display: inline-table; margin: 10px;">

            <div class="blog-sidebar" style>
                <div class="side-widget br-yellow" id="deal_{{id}}" style="width: 170px;float: left;">
                    <h4>{{title}}</h4>

                    <div class="widget-content" style="padding: 20px;">
                        <img src="${resource(dir: 'bootstrap-theme', file: 'img/portfolio/2.jpg')}" id={{id}}
                             style="height: 100px;" alt=""/>&nbsp;&nbsp;
                        <p>{{startDate}}</p>
                        %{--<div class="social">--}%
                        <!--<a href="javascript:void(0)" onclick="getDealDetailsPageJSON('%{--${createLink(controller: 'publicApi',action: 'editMerchantDealJSON')}--}%/{{id}}')" class="btn btn-success">View</a>-->
                        %{--&nbsp;&nbsp;--}%
                        %{--<a href="javascript:void(0)" onclick="getEditDealMerchantJSON('${createLink(controller: 'publicApi',action: 'editMerchantDealJSON')}/{{id}}')" class="btn btn-danger">Edit</a>--}%
                        %{--</div>--}%
                    </div>
                </div>
            </div>
        </li>
    </div>
</ul>
%{--
<div id="side-widget br-yellow singleShowDealDiv" class="container-fluid" style="display: none;width: 170px;float: left;">
    <li class="row" style="display:inline-table">
        <div class="col-md-3 widget-content" style="min-height: 150px;min-width: 200px">
        <a href="${resource(dir: 'bootstrap-theme',file: 'img/portfolio/1.jpg')}">
            <img src="${resource(dir: 'bootstrap-theme',file: 'img/portfolio/2.jpg')}" style="height: 100px;" alt="" >
            <!-- Portfolio caption -->
                <h4>{{title}}</h4>
                <p>{{description}}</p>
        </a>
            </div>
    </li>
</div>
--}%



<br><br>

<script type="text/javascript">
    getDealsJSON("${createLink(controller: 'deal',action: 'getDealsJSON')}");

    function getDealsJSON(formPostUrl) {
        var template = $('#singleShowDealDiv').html();
        $.getJSON(formPostUrl, function (data) {
            $.each(data, function (idx, obj) {
                var output = Mustache.render(template, obj);
                $('#showDealsListDiv').append(output);
            });
        });
    }
</script>
%{--<g:render template="/templates/footer"/>--}%
</body>
</html>