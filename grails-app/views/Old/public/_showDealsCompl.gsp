<div class="inner-page">
    <div class="page-mainbar resume" id="buyDealStatus" style="display: none;margin: 10px;">
        <div class="resume-content">
            <div class="row">
                <div class="col-md-10 col-sm-8">
                    <!-- Resume Details -->
                    <div class="resume-details br-orange animated animation fadeInRight">
                        <!-- Labels -->
                        <span class="label">Thanks You Buy a Deal Successfully.</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<ul id="showDealsListDiv" style="position: relative; height: 768px;">
    <!-- Add the above used filter names inside div tag. You can add more than one filter names. For image lightbox you need to include "a" tag pointing to image link, along with the class "prettyphoto". -->



    <div id="singleShowDealDiv" class="container-fluid" style="display: none">
        <li class="" style="display: inline-table; margin: 10px;" id="postList">

            <div class="blog-sidebar" style>
                <div class="side-widget br-yellow" id="deal_{{id}}" style="width: 170px;float: left;">
                    <h4>{{title}}</h4>

                    <div class="widget-content" style="padding: 20px;">
                        %{--<a href="{{imageURL}}" class="group1" >--}%
                        <img src="{{imageURL}}" id="imageView_{{id}}" style="height: 120px;width: 123px" alt=""/> %{--</a>--}%&nbsp;&nbsp;
                        <p>{{description}}</p>
                        <a href="javascript:void(0)" id="buyDealBtn"
                           onclick="buyDeal('${createLink(controller:'deal',action:'buyDeal')}' + '?id={{id}}')"
                           class="btn btn-primary buyDealBtn" style="display: none">Buy this</a>
                    </div>
                </div>
            </div>
        </li>
    </div>
</ul>

<script type="text/javascript">
    $(document).ready(function(){

//        $('.group1').colorbox({rel:'group1'});
        getDealsJSON("${createLink(controller: 'deal',action: 'getDealsJSON')}");
    });




    function getDealsJSON(formPostUrl) {
        var template = $('#singleShowDealDiv').html();
        $.getJSON(formPostUrl, function (data) {
            $.each(data, function (idx, obj) {
                var output = Mustache.render(template, obj);
                //showBuyDealFunction();
                $('#showDealsListDiv').prepend(output);
            });
        });
    }
    function buyDeal(hitURL) {
//      alert('id value is'+$("").val);
        console.log($("#id").serialize());

        $.ajax({
            type: "POST",
            url: hitURL,
            //data:$("#id").serialize(),
            //dataType: "json",
            success: function (data) {

//                alert('call success');
                if (data) {
                    $('#buyDealStatus').show(0).delay(5000).hide(0);
                    //$("#buyDealStatus").show();
                } else {
                    alert('in else');
                }
            }, error: function () {
                alert('in error');
            }
        });
    }
    function showBuyDealFunction() {

        if ($("#loginAs").hasClass("loginSuccess")) {
            $("li").each(function (index, value) {
                        console.log('each..........' + index);
                        $(".buyDealBtn").show();

                    }
            )
        }
    }
</script>