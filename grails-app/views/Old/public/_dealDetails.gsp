<ul id="showDetailsDiv" style="position: relative; height: 768px;">
</ul>
<div id="showDetails" class="container-fluid col-md-12" style="display:none">

    <li style="display: inline-table" class="col-md-10">
        <div class="blog-sidebar col-md-3" style="float: left">

            <div class="side-widget br-yellow" id="deal_{{id}}" style="width: 170px;float: left;">
                <h4>Post Title is {{title}}</h4>

                <div class="widget-content">
                    <img src="${resource(dir: 'bootstrap-theme', file: 'img/portfolio/2.jpg')}" id={{id}}
                         style="height: 100px" alt="">
                </div>
                <a href="javascript:void(0)" onclick="backManageDeals()" %{--onclick="getMerchantDealsJSON('${createLink(controller:'publicApi',action:'getMerchantsDealJSON')}')"--}% class="btn btn-black">Back</a>
            </div>
        </div>


        <div class="col-md-3" style="">
          <p>Title : <span style="color: #c77405">{{title}}</span></p>
          <p>Start Date : <span style="color: #c77405">{{startDate}}</span></p>
          <p>End Date : <span style="color: #c77405">{{endDate}}</span></p>
          <p>Quantity : <span style="color: #c77405">{{quantity}}</span></p>
          <p>Prior Price : <span style="color: #c77405">{{priorPrice}} $</span></p>
          <p>Current Price : <span style="color: #c77405">{{curPrice}} $</span></p>
          <p>Description : <span style="color: #c77405">{{description}}</span></p>
        </div>
        <div class="col-md-3" ></div>
        <div class="col-md-3" ></div>
    </li>
</div>
<script>
 function backManageDeals(){
     $(".landingPageForms").hide();
     $("#manageDealDisplay").show()
 }

</script>



