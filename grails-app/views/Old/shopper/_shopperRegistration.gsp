<div class="tab-pane br-green active" id="registerShopper">
    <!-- Heading -->
    <h2>Shopper SignUp</h2>

    <g:form class="form-horizontal" name="addShopperForm" role="form" action="doregister" controller="shopper">
        <div class="form-group">
            <div class="col-sm-12">
                %{--<input type="text" class="form-control" name="name" placeholder="Your Name"><g:renderErrors bean="${signUpCO}" field="name" />--}%
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-12">
                <input type="email" class="form-control" id="email" name="email" placeholder="Your Mail Address"
                       onblur='verifyShopperEmailValidity("${createLink(controller: 'deal',action: 'checkUniqueEmail')}")'><g:renderErrors
                    bean="${signUpCO}" field="email"/>
                <span class="showEmailValidationFail" style="color: red;display: none">Email already exists.</span>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-12">
                <input type="password" class="form-control" name="password"
                       placeholder="Choose Password"><g:renderErrors bean="${signUpCO}" field="password"/>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-12">
                <div class="checkbox">
                    <label>
                        <input type="checkbox"> Accept All Terms & Conditions
                    </label>
                    <span class="pull-right"><a href="#">View</a></span>

                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

        <div class="form-group text-center">
            <div class="col-sm-12">
                <a href="javascript:void(0)" class="btn btn-default" id="submitRegistrationButton"
                   onclick='createDealFormSubmit("${createLink(controller: "shopper",action: "doregister")}")'>Register</a>
                <input type="reset" class="btn btn-default" value="Reset"/>

            </div>
        </div>
    </g:form>

</div>
<style type="text/css">
.form-group ul li {
    color: red;
    text-align: left;
}
</style>

<script>
  $(document).ready(function(){
      alert('hrrrrrrr');
  });
    function verifyShopperEmailValidity(formPostUrl) {
        var dataString = $("#email").val();
        //alert("email id"+dataString)
        $.ajax({
            type: "POST",
            url: formPostUrl,
            data: {email: dataString},
            dataType: "json",
            success: function (data) {
                if (data.resultData == 'true') {
                    //alert("ok")
                    $('.showEmailValidationFail').show();
                    $('#submitRegistrationButton').addClass('disabled');
                } else {
                    $('.showEmailValidationFail').hide();
                    $('#submitRegistrationButton').removeClass('disabled');
                }
            }, error: function () {
                $('#submitRegistrationButton').addClass('disabled');
            }
        });
    }


    function createDealFormSubmit(formPostUrl) {
        var dataString = $("#addShopperForm").serialize();
        $.ajax({
            type: "POST",
            url: formPostUrl,
            data: dataString,
            dataType: "json",
            success: function (data) {
                console.log(data);
                if (data) {
//                    alert('OK');
                    $("#registerShopper").hide("slow");
//                    $("#statusRegister").fadeTo(500, 100, function() {
                    //$(this).html("You are now registered!");
//                    })
                    //window.location=data.redirectUrl
                    $("#mstatus").show();
                }
                else {
//                  $('#errorMessageDiv').html(resultMap.errorMessages)
                    $('#errorMessageDiv').show();
                }
            },
            error: function () {
                $('#errorMessageDiv').html(data.errorDescription);
            }
        });
    }


</script>

