<!DOCTYPE html>
<html>
<head>
    <!-- Title here -->
    <title>Zoomdeals, Login/Register</title>
    <!-- Description, Keywords and Author -->
    <meta name="layout" content="public"/>
</head>

<body>
<div class="wrapper white">
    <div class="inner-page">
        <div class="container">
            <div class="page-mainbar login">
                <!-- Login/Register Content -->
                <div class="login-content">
                    <!-- Nab Bar Tan Menu list -->
                    %{--<ul id="mytab" class="nav nav-tabs nav-justify">

                        <li>
                            <a href="#register" id="registrationDivButton" class="br-lblue" data-toggle="tab">
                                <!-- Icon -->
                                <i class="fa fa-sign-out"></i>
                                <span>Shopper</span>
                            </a>
                        </li>

                    </ul>--}%
                    <!-- Content for each menu item -->
                    <div class="tab-content">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="notices" id="mstatus" style="display: none">
    <div class="bg-color-green">

        <div class="notice-header fg-color-white">Registered!</div>
        <!-- TODO remove this and login user automatically -->
        <div class="notice-text">
            You are now registered. Please <a
                href=${createLink(controller: 'public', action: 'loginAjax')} class="tologin">login to continue</a>..
        </div>
    </div>
</div>

</body>
</html>