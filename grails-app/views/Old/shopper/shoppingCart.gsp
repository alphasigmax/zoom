<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <title>Elementary</title>
    <meta charset="iso-8859-1">

    <!--[if lt IE 9]><script src="scripts/html5shiv.js"></script><![endif]-->
    <!-- homepage scripts -->
    <r:require module="common"/>
    <r:require module="dealdetail"/>
    <style>
    div.related {
        margin: 20px;
    }

    ul.related {
        list-style-type: none;
    }

    li.related img {
        float: left;
        margin: 10px;
        border: 5px solid #fff;

        -webkit-transition: box-shadow 0.5s ease;
        -moz-transition: box-shadow 0.5s ease;
        -o-transition: box-shadow 0.5s ease;
        -ms-transition: box-shadow 0.5s ease;
        transition: box-shadow 0.5s ease;
    }

    li.related:hover {
        -webkit-box-shadow: 0px 0px 7px rgba(255, 255, 255, 0.9);
        box-shadow: 0px 0px 7px rgba(255, 255, 255, 0.9);
        background: #eee;
        cursor: pointer;
    }
    </style>
    <!-- / homepage scripts -->
</head>

<body class="home blog chrome et_includes_sidebar daily-deal full-width">
<div id="main-wrap" class="clearfix">

    <g:render template="/templates/header"/>

    <div id="main">
        <div id="wrapper">

            <!-- content -->
            <div class="wrapper row2">
                <div id="container" class="clear">
                    <!-- content body -->
                    <section id="slides">
                    </section>
                    <!-- Services area -->

                    <section class="services clear">
                        <div class="btncontainer">
                            <a href="#" class="largebtn">Send Now</a>
                        </div>

                        <div xstyle="float:right">
                            <strong>My Rating</strong>

                            <div class="rateit bigstars" data-rateit-value=0 data-rateit-resetable="false"
                                 data-rateit-max=5 data-rateit-starwidth="32" id="rateit"
                                 data-rateit-starheight="32"></div>
                        </div>
                        <br>



                        <sc:each>
                            ${it} /

                        </sc:each>

                        <g:each in="${com.metasieve.shoppingcart.Shoppable.list()}" var="product">
                            ${product}
                        </g:each>
                    </section>
                    <section class="services clear">
                        <article>
                            <figure><g:img dir="images/dealdetail/demo/" file="32x32.gif" alt="" width="32"
                                           height="32"/></figure>
                            <strong>Related Deals</strong> <br><hr>


                            <div class="related">

                                <ul class="related">

                                    <li class="related">
                                        <img src="<g:createLink controller="adImage"
                                                                action="index"/>?name=1back460.jpg&">

                                        <h3>Headline</h3>

                                        <p>Lorem ipsum dolor sit amet...</p>
                                    </li>

                                    <li class="related">
                                        <img src="<g:createLink controller="adImage"
                                                                action="index"/>?name=2back460.jpg&">

                                        <h3>Headline</h3>

                                        <p>Lorem ipsum dolor sit amet...</p>
                                    </li>

                                    <li class="related">
                                        <img src="<g:createLink controller="adImage"
                                                                action="index"/>?name=3back460.jpg&">

                                        <h3>Headline</h3>

                                        <p>Lorem ipsum dolor sit amet...</p>
                                    </li>

                                    <li class="related">
                                        <img src="<g:createLink controller="adImage"
                                                                action="index"/>?name=4back460.jpg&">

                                        <h3>Headline</h3>

                                        <p>Lorem ipsum dolor sit amet...</p>
                                    </li>
                                </ul>
                            </div>

                        </article>

                        <div class="spacer"></div>

                        <br><br><br>

                    </section>

                    <!-- / content body -->
                </div>
            </div>

        </div>
    </div>
</div>
<!-- footer -->
<g:render template="/templates/footer"/>

</body>
</html>
