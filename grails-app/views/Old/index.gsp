<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>

    <meta name="layout" content="application">


    <title>Zoom Deals</title>

</head>

<body class="metrouicss">

<div class="page">

    <g:render template="/templates/header"/>


    <div class="page-region">
        <div class="page-region-content tiles" id="maintiles">

            <div class="tile double image" data-template>

                <div class="tile-content">
                    <img src="images/deal.gif" data-src="adImage/index?name={{id}}back460.jpg"/>
                </div>

                <div class="brand bg-color-orange">
                    <!--  img class="icon" src="images/Rss128.png"-->
                    <p class="text"><a href="dealDetail?id={{id}}">{{title}}</a></p>

                    <div class="badge">{{curPrice}}</div>
                </div>

            </div>

        </div>

    </div>

</div>



<script src="<g:resource dir="js" file="tempo/tempo.js" absolute="true"/>"></script>
<script src="<g:resource dir="js" file="metroui/jquery/jquery.mousewheel.min.js" absolute="true"/>"></script>

<script>
    var offset = 0;
    var lat = 0;
    var lon = 0;

    var twitter = Tempo.prepare('maintiles');
    $(document).ready(
            function () {

                //	$('.full-width').horizontalNav({});

                loadMoreData();
                $(window).scroll(
                        function () {
                            if ($(window).scrollTop() >= $(document)
                                    .height()
                                    - $(window).height() - 10) {

                                offset = 1;//offset+25

                                loadMoreData();
                            }
                        });


                $(document).delegate("div.tile", "click", function () {
                    window.location = $(this).find("a").attr("href");
                });

            });


    function loadMoreData() {

        $.ajax("deals?lat=" + lat + "&lon=" + lon + "&offset=" + offset).done(function (data) {
            console.log(data);
            if (offset > 0) {
                twitter.append(data);
            } else {
                twitter.render(data);
            }

        }).fail(function () {
                    alert("error");
                });

    }

    navigator.geolocation.getCurrentPosition(
            onSuccess,
            onError, {
                enableHighAccuracy: true,
                timeout: 20000,
                maximumAge: 120000
            });


    function onSuccess(position) {
        //the following are available to use
        //position.coords.latitude
        //position.coords.longitude
        // position.coords.altitude
        // position.coords.accuracy
        // position.coords.altitudeAccuracy
        // position.coords.heading
        // position.coords.speed
        lat = position.coords.latitude;
        lon = position.coords.longitude;
        //(position.coords.latitude,position.coords.longitude)
        loadMoreData()
    }

    function onError(position) {
        //call up google maps. Or call our internal service to get the location
        console.log("Error fetching location..")
    }

</script>
<script src="https://maps.googleapis.com/maps/api/js?sensor=false&libraries=places"></script>
</body>
</html>