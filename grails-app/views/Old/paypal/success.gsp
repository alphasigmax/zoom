<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta name="layout" content="application">


    <title>Transaction Complete</title>

</head>

<body class="metrouicss">
<div class="page">
    <g:render template="/templates/header"/>



    <div class="page-region">

        <div class="page-region-content">

            Your purchase is complete. Information for your reference can be seen below:
            <div id="transactionSummary" class="transactionSummary">
                <div class="transSummaryItem">
                    <span class="transSummaryItemName">Transaction ID:</span>
                    <span class="transSummaryItemValue">SUCCESS</span>
                </div>

            </div>

        </div>

    </div>

</div>
</body>
</html>


	
	