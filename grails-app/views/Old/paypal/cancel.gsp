<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta name="layout" content="application">


    <title>Puchase Cancelled</title>

</head>

<body class="metrouicss">
<div class="page">
    <g:render template="/templates/header"/>



    <div class="page-region">

        <div class="page-region-content">

            Your purchase transaction has been cancelled. Information about the items you planned to purchase can be seen below:
            <div id="transactionSummary" class="transactionSummary">

            </div>

        </div>

    </div>

</div>
</body>
</html>