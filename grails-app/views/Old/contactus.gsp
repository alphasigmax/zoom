﻿<!DOCTYPE html>

<html class=" js rgba backgroundsize opacity cssanimations csstransitions fontface"
      lang="en"><!--<![endif]--><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta name="keywords" content="deals, reverse bid, options">
    <meta name="description" content=" some stuff">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="shortcut icon" type="image/x-icon" href="http://zoomdeals.com.au/favicon.ico">

    <title>Contact  Us</title>

    <meta name="layout" content="clear">
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'contactus/style.css', absolute: true)}">
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'contactus/contact.css', absolute: true)}">

</head>

<body>

<g:render template="/templates/header"/>
<div id="searchDescription">Zoom Deals best deals</div>

<div class="header-bg"></div>

<div class="main shop" role="main">
    <header>
        <div class="social twitter">
            <a href="https://twitter.com/#!/zoomdeals" target="_blank">Follow on Twitter</a>
        </div>

        <div class="social facebook">
            <a href="https://www.facebook.com/zoomdeals" target="_blank">Like on Facebook</a>
        </div>


        <nav class="left">
            <a href="http://zoomdeals.com.au/shop">The Shop <span style="opacity: 0;"></span></a>
            <span>&nbsp;</span>
            <a href="http://zoomdeals.com.au/about">About <span style="opacity: 0;"></span></a>
            <span>&nbsp;</span>
            <a href="http://zoomdeals.com.au/menu">Menu <span style="opacity: 0;"></span></a>
        </nav>

        <nav class="right">
            <a href="http://zoomdeals.com.au/catering" class="catering">Catering <span
                    style="opacity: 0; margin-top: -20px;"></span></a>
            <span>&nbsp;</span>
            <a href="http://blog.zoomdeals.com.au/">Blog <span style="opacity: 0;"></span></a>
            <span>&nbsp;</span>

        </nav>

        <div class="drop">&nbsp;</div>
    </header>

    <div class="content">

        <div class="info">
            <h4>Phone</h4>

            <h5>Melbourne</h5>
            <h6>04 7805 7274</h6>

            <h4>Fax</h4>
            <h6>(405) 285-1525</h6>

            <h4>Email</h4>
            <h5>General Inquiries</h5>
            <h6><a href="mailto:info@zoomdeals.com.au">info@zoomdeals.com.au</a></h6>

            <h5>Business Inquiries</h5>
            <h6><a href="mailto:business@zoomdeals.com.au">Business@zoomdeals.com.au</a></h6>

        </div>

        <form action="<g:createLink controller="contact" action="sendmymail"/>" method="post">

            <h4>Send us some electronic mail</h4>

            <div class="errors"><g:message code="${flash.message}" args="${flash.args}"
                                           default="${flash.default}"/></div>

            <p>
                <label for="name">Your Name:</label>
                <input type="text" id="name" name="name">
            </p>

            <p>
                <label for="company">Company:</label>
                <input type="text" id="company" name="company">
            </p>

            <p>
                <label for="email">Email Address:</label>
                <input type="text" id="email" name="email">
            </p>

            <p>
                <label for="phone">Phone Number:</label>
                <input type="text" id="phone" name="phone">
            </p>

            <p>
                <label for="location">Location (city):</label>
                <input type="text" id="location" name="location">
            </p>

            <p>
                <label for="message">Message:</label>
                <textarea rows="10" name="message"></textarea>
            </p>

            <div class="dots">
                <input class="submit" type="submit" name="submit" value="send">
            </div>
        </form>
    </div>


    <footer>
        <a class="logo" href="http://www.zoomdeals.com.au/">
            <span></span>
            <img src="logo.png" alt="Zoom Deals">
        </a>

        <nav>
            <a href="http://zoomdeals.com.au/shop">The Shop</a>
            <a href="http://zoomdeals.com.au/about">About Us</a>
            <a href="http://zoomdeals.com.au/menu">Menu</a>
            <a href="http://zoomdeals.com.au/retail">Retail</a>
            <a href="http://zoomdeals.com.au/catering">Catering</a>
            <a href="http://zoomdeals.com.au/blog">Blog</a>
        </nav>


        <div class="clear"></div>
    </footer>
</div>

<div class="thanks">Thanks for Stopping By!</div>

<div class="footer-bg"></div>

</body></html>