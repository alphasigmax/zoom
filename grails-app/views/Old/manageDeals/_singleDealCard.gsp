<li class="" style="display: inline-table">
    %{--<div class="col-md-3" style="min-height: 150px;min-width: 150px">--}%
    %{--<a href=""  onclick=getEditDealMerchantJSON('${createLink(controller: 'publicApi',action: 'editMerchantDealJSON')}/7')>--}%
    %{--<img src="${resource(dir: 'bootstrap-theme',file: 'img/portfolio/2.jpg')}" id={{id}}   style="height: 100px" alt="">--}%
    %{--<!-- Portfolio caption -->--}%
    %{--<h4>{{title}}</h4>--}%
    %{--<p style="display: none">{{id}}</p>--}%

    %{--</a>--}%
    %{--</div>--}%

    <div class="blog-sidebar">
        <!-- Sidebar widget -->
        <!-- First -->

        <!-- Second -->
        <div class="side-widget br-yellow" id="deal_{{id}}" style="width: 170px;float: left;">
            <h4>{{title}}</h4>

            <div class="widget-content ">
                <img src="{{imageURL}}" id={{id}} style="height: 100px" alt="">

                <p>{{description}} id:{{id}}</p>

                <div class="social">
                    <a href="javascript:void(0)"
                       onclick="getDealDetailsJSON('${createLink(controller: 'deal',action: 'editMerchantDealJSON')}/{{id}}')"
                       class="btn btn-success">View</a>
                    &nbsp;&nbsp;
                    <a href="javascript:void(0)"
                       onclick="getEditDealMerchantJSON('${createLink(controller: 'deal',action: 'editMerchantDealJSON')}/{{id}}')"
                       class="btn btn-danger">Edit</a>
                    &nbsp;&nbsp;
                    <a href="javascript:void(0)"
                       onclick="getPurchaseHistoryList('${createLink(controller: 'deal',action: 'getBuyersList')}'+'?id={{id}}')"
                       class="btn btn-black">Buyers</a>
                </div>

            </div>
        </div>
    </div>
</li>