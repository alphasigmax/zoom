<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>

    <meta name="layout" content="public">

    <link rel="stylesheet" type="text/css"
          href="${resource(dir: 'css', file: 'smartwizard/smart_wizard.css', absolute: true)}"/>
    <title>Zoom Deals</title>
    <script src="https://maps.googleapis.com/maps/api/js?sensor=false&libraries=places"></script>

    <script src="${resource(dir: 'js', file: 'google/gmaps.js', absolute: true)}"></script>

    <script src="${resource(dir: "js", file: "smartwizard/jquery.smartWizard-2.0.js", absolute: true)}"></script>
    <script src="${resource(dir: 'js', file: 'metroui/dialog.js', absolute: true)}"></script>
    <script src="${resource(dir: 'js', file: 'jquery/jquery.form.js', absolute: true)}"></script>
    <script type="text/javascript" src="${resource(dir: 'js', file: 'mustache/mustache.js')}"></script>
    %{--JQuery Vlidation--}%
    <script type="text/javascript" src="${resource(dir: 'js', file: 'jquery/jquery-validator-1.11.1.js')}"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
    <script>
        $(function () {
            $("#endDate").datepicker();
        });
        $(function () {
            $("#startDate").datepicker();
        })





    </script>

</head>

<body class="metrouicss">

<!-- TODO get the current location based on HTML5 or IP and center the map to that location
 first use navigator.location and if that is not available use the IP service to get current city and center to that city
 -->

<div class="inner-page">
    <div class="page-mainbar resume" id="addDealStatus" style="display: none;margin: 10px;">
        <div class="resume-content">
            <div class="row">
                <div class="col-md-10 col-sm-8">
                    <!-- Resume Details -->
                    <div class="resume-details br-orange animated animation fadeInRight">
                        <!-- Labels -->
                        <span class="label">Deal Created Successfully.</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="wrapper white">

    <!-- Inner Page Content // Start -->

    <div class="inner-page">
        <div class="container">
            <div class="page-mainbar login col-md-12" style="padding-left: 0">
                <!-- Login/Register Content -->
                <div class="col-md-6">
                    <ul id="merchantCreateDealsListDiv" style="position: relative;padding-left: 0;">

                    </ul>
                </div>
                <div class="login-content col-md-6"
                     style="max-width: 700px;padding-top: 0px;padding-right: 0;margin-top: 0px">
                    <g:render template="/manageDeals/createAdPostForm"/>
                </div>
            </div>
        </div>
    </div>

</div>

<div id="singleCreateDealDiv" class="container-fluid" style="display:none">
    <g:render template="/manageDeals/singleDealCard"/>
</div>
<div style="display:none">
    <g:render template="/public/showAllBuyersList"/>

</div>


<script>
    $(document).ready(function () {

        initializeMaps();



//        tinyMCE.init({
//            theme : "advanced",
//            mode : "textareas",
//            theme_advanced_resizing : true,
//            // plugins : "autolink,lists,pagebreak,style,layer,table,advimage,advlink,emotions,inlinepopups,insertdatetime,preview,media,contextmenu,paste,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
//            theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,bullist,numlist,|,formatselect,fontselect,fontsizeselect",
//            theme_advanced_buttons2 : "",
//            theme_advanced_buttons3 : "",
//            theme_advanced_toolbar_location : "top",
//            theme_advanced_toolbar_align : "left",
//            theme_advanced_statusbar_location : "bottom"
//        });


        var options = {
            success: showResponse,
            dataType: 'json'
            //timeout:   3000
        };

        $('#wizard').smartWizard({onLeaveStep: leaveAStepCallback, onFinish: onFinishCallback, updateHeight: true, onShowStep: mapChecker});

//        $('#endDate').datetimepicker();

        //$( "#endDate" ).datepicker();


        //$('#dealForm').submit(function() {
        //$(this).ajaxSubmit(options);
        //return false;
        //});


        function leaveAStepCallback(obj) {
            var step_num = obj.attr('rel'); // get the current step number
            return validateSteps(step_num); // return false to stay on step and true to continue navigation
        }

        function mapChecker(obj) {
            var step_num = obj.attr('rel'); // get the current step number
            if (step_num == 3 && google) {
                google.maps.event.trigger(window.map, 'resize');
                showCurLoc();
            }
        }

        function onFinishCallback() {
            if (validateAllSteps()) {
                $("#dealForm").ajaxSubmit(options);
                return false;
            }
//            $("#register").hide();
////            if (responseText.errorDescription) {
////                showMsg("Registration error",
////                        responseText.errorDescription)
////            } else {
//                $("#dealForm").hide("slow");
//                alert('Deal Saved')
//                $("#addDealStatus").fadeTo(500, 100, function() {
//                    //$(this).html("You are now registered!");
//                })
//
////            }

        }

        // Your Step validation logic
        function validateSteps(stepnumber) {
            var isStepValid = true;
            // validate step 1
            if (stepnumber == 1) {
                validateStep1();
                // Your step validation logic
                // set isStepValid = false if has errors
                //alert("validating step one")
            } else if (stepnumber == 2) {
                //alert("validating step two")
            }
            return isStepValid;
        }

        function validateAllSteps() {
            var isStepValid = true;
            // all step validation logic
//            alert("validating all setps")
            //tinyMCE.triggerSave();
            return isStepValid;
        }

        function showResponse(responseText, statusText, xhr, $form) {
            $("#registerdPost").hide();
            if (responseText.errorDescription) {
                showMsg("Registration error",
                        responseText.errorDescription)
            } else {
                $("#dealForm").hide("slow");
                //alert('Deal Saved')
                $("#addDealStatus").fadeTo(500, 100, function () {
                    //$(this).html("You are now registered!");
                })
                getNewDealJSON("${createLink(controller: 'deal',action: 'getRecentMerchantDealJSON')}");


            }
        }

        $("#step-1").validate({
            rules: {
                title: {
                    required: true

                },
                description: {
                    required: true

                }
            },
            messages: {
                title: {
                    required: ""
                }
            }
        });

    });
    function validateStep1() {
        //alert('llllll');
        $("#dealForm").validate({

            rules: {
                title: {
                    required: true

                },
                description: {
                    required: true

                }
            },
            messages: {
                title: {
                    required: ""
                }
            }
        });
    }

    function getDealsJSON(formPostUrl) {

        var template = $('#singleCreateDealDiv').html();
        $.getJSON(formPostUrl, function (data) {

            $("#merchantCreateDealsListDiv").html('');
            $.each(data, function (idx, obj) {
                var output = Mustache.render(template, obj);
                $('#merchantCreateDealsListDiv').prepend(output);
            });
        });
    }

    function getNewDealJSON(formPostUrl) {
        var template = $('#singleCreateDealDiv').html();
        $.getJSON(formPostUrl, function (data) {
            //$.each(data, function(idx, obj){
            var output = Mustache.render(template, data);
            $('#merchantCreateDealsListDiv').prepend(output);
            //});
        });
    }

    function getEditDealMerchantJSON(formPostUrl) {
        $.getJSON(formPostUrl, function (data) {
            //alert('title is'+data.title);
            //alert("Title field value is "+$("#title").value);
            $("#title").attr('value', data.title);
            $("#quantity").attr('value', data.quantity);
            $("#priorPrice").attr('value', data.priorPrice);
            $("#curPrice").attr('value', data.curPrice);
            $("#startDate").attr('value', data.startDate);
            $("#endDate").attr('value', data.endDate);
            $("#description").text(data.description);
            //$("#lonid").attr('value',data.lonid);


        });
    }

    function displayCreateAdDealForm() {
        alert('in display');
        $("#registerdPost").show();
//          $("#registerdPost").show();
        $("#addDealStatus").hide();

    }


    function getPurchaseHistoryList(formPostUrl) {
        var template = $('#showBuyers').html();
        $.getJSON(formPostUrl, function (data) {
//            console.log(".....");
            $('#merchantCreateDealsListDiv').html('');
//            $('#showBuyers').show();
            $.each(data, function(idx, obj){
                console.log("id is"+obj.id);
                console.log("buyerListURL is"+obj.buyerListURL);
                //alert("whyyy"+obj.buyerId);
            var output = Mustache.render(template,obj);
                $('#merchantCreateDealsListDiv').prepend(output);

            });
        });
    }

    $("#addDealsLink").click(function () {
        alert('in click');
        //$("#registerdPost").css({'display':''});
        //$("#registerdPost").css({'display':''});
        displayCreateAdDealForm();
    });

</script>

</body>
</html>
