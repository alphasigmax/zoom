<html>
<head>
    <meta name='layout' content='public'/>
    <title><g:message code="springSecurity.login.title"/></title>
</head>

<body>
<div class="wrapper white">

    <!-- Inner Page Content // Start -->

    <div class="inner-page">
        <div class="container">
            <div class="page-mainbar login">
                <!-- Login/Register Content -->
                <div class="login-content col-md-3" style="padding-top: 30px">
                    <!-- Nab Bar Tan Menu list -->
                    <ul id="mytab" class="nav nav-tabs nav-justify">
                        <li class="active">
                            <a href="#login" id="loginDivButton" class="br-red" data-toggle="tab">
                                <!-- Icon -->
                                <i class="fa fa-sign-in"></i>
                                <span>Login</span>
                            </a>
                        </li>
                        <li>
                            <a href="#register" id="registrationDivButton" class="br-lblue" data-toggle="tab">
                                <!-- Icon -->
                                <i class="fa fa-sign-out"></i>
                                <span>Sign Up</span>
                            </a>
                        </li>
                        <li>
                            <a href="#contact" class="br-green" data-toggle="tab">
                                <!-- Icon -->
                                <i class="fa fa-user"></i>
                                <span>Contact</span>
                            </a>
                        </li>
                    </ul>
                    <!-- Content for each menu item -->
                    <div class="tab-content">
                        <!-- First Content for Nav bar -->

                        <g:render template="/public/loginForm"
                                  model="[postUrl: postUrl, rememberMeParameter: rememberMeParameter]"/>

                        <!-- Second Content for Nav bar -->
                        <g:render template="/public/signUpForm" model="[signUpCO: signUpCO]"/>
                        <!-- Sign Up Form Start -->
                        <!-- Sign Up form End -->

                        <!-- Third Content for Nav bar -->
                        <g:render template="/public/contactUsForm"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

%{--<div id='login'>--}%
%{--<div class='inner'>--}%
%{--<div class='fheader'><g:message code="springSecurity.login.header"/></div>--}%

%{--<g:if test='${flash.message}'>--}%
%{--<div class='login_message'>${flash.message}</div>--}%
%{--</g:if>--}%

%{--<form action='${postUrl}' method='POST' id='loginForm' class='cssform' autocomplete='off'>--}%
%{--<p>--}%
%{--<label for='username'><g:message code="springSecurity.login.username.label"/>:</label>--}%
%{--<input type='text' class='text_' name='j_username' id='username'/>--}%
%{--</p>--}%

%{--<p>--}%
%{--<label for='password'><g:message code="springSecurity.login.password.label"/>:</label>--}%
%{--<input type='password' class='text_' name='j_password' id='password'/>--}%
%{--</p>--}%

%{--<p id="remember_me_holder">--}%
%{--<input type='checkbox' class='chk' name='${rememberMeParameter}' id='remember_me' <g:if test='${hasCookie}'>checked='checked'</g:if>/>--}%
%{--<label for='remember_me'><g:message code="springSecurity.login.remember.me.label"/></label>--}%
%{--</p>--}%

%{--<p>--}%
%{--<input type='submit' id="submit" value='${message(code: "springSecurity.login.button")}'/>--}%
%{--</p>--}%
%{--</form>--}%
%{--</div>--}%
%{--</div>--}%
<script type='text/javascript'>
    <!--
    (function () {
        document.forms['loginForm'].elements['j_username'].focus();
    })();
    // -->

    jQuery(document).ready(function () {
        <g:if test="${fromSignUpPage}">
        jQuery('#login').removeClass('active');
        jQuery('#register').addClass('active');
        jQuery('#registrationDivButton').click();

        </g:if>
        <g:elseif test="${fromLoginPage}">
        jQuery('#register').removeClass('active');
        jQuery('#login').addClass('active');
        jQuery('#loginDivButton').click();

        </g:elseif>

    })
</script>
</body>
</html>
