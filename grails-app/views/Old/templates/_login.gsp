<div class="tab-pane fade in active br-red">

    <form id="loginForm" class="form-horizontal" autocomplete="on">

        <h1>Login</h1>

        <div class="form-group">
            <div class="col-sm-12">

                <input id="username" name="j_username" class='form-control' required="required" type="email"/>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-12">
                <input id="password" name="j_password" class='form-control' required="required" type="password"/>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-12">

                <input type="checkbox" name="_spring_security_remember_me" id="loginkeeping" value="loginkeeping"/>
                <span class="helper">Keep me logged in</span>
            </div>
        </div>

        <p>
            <input type="submit" class="btn btn-primary" id="login-btn" value="Login"/>
        </p>

        <p class="change_link">
            %{--Not a member yet ? <a href="#toregister" id="toregister" class="toregister">Join us</a>--}%
            %{--Not a member yet ? <a href="${createLink(controller: 'merchant',action: 'signUp')}" id="toregister" class="toregister">Join us</a>--}%
            Not a member yet ? <a href="javascript:void(0)" id="toregister" class="toregister">Join us</a>
        </p>

    </form>
    <div id="loginSuccess" style="display: none">
        <h1>You Login Successfully.......</h1>
    </div>
</div>

<script>
    //$(document).ready(function() {

    $("#loginForm").submit(function () {
        console.log($(this).serialize());
        $.ajax({
            type: "POST",
            data: $(this).serialize(),
            cache: false,
            url: "${request.contextPath}/j_spring_security_check",
            success: function (data) {
                if (data.success) {
                    //alert('hii this one under success'+data.username);
//                        $("#successlogin").show("slow");
                    $("#loginAs").addClass("loginSuccess");
                    $("#loginAs").html("Logged in as " + data.username + " <a href='${createLink(controller: 'logout')}' id='logoutTo'>( Logout )</a>");

                    if (data.loginUserRole.match('ROLE_MERCHANT')) {
                        getMerchantDealsJSON("${createLink(controller: 'deal',action: 'getMerchantsDealJSON')}");

                        $("#shopper").hide();
                        $("#merchant").hide();
                        $("#manageDealIcon").show();
                    }
                    if(data.loginUserRole.match('ROLE_SHOPPER')){
                        getAllPurchasePosts('${createLink(controller:'deal',action:'getPurchaseDealList')}');
                        $("#manageDealIcon").hide();
                        $(".publicMenu").show();
                    }
                    if(data.loginUserRole.match('ROLE_ADMIN')){

                    }
                    if(data.loginUserRole.match('ROLE_USER')){

                    }
                    $("#shopper").hide();
                    $("#merchant").hide();
                    $("#login").hide();
                    $("#logout").show();
//                    alert("User role is " + data.loginUserRole);
                    $("#loginSuccess").show("slow");
                    $("#loginForm").get(0).reset();
                    $("#loginForm").hide("slow");

                    showBuyDealFunction();

                    //$("#loginForm").resetForm;
                    /*setTimeout(function() {1
                     console.log(data.username);
                    %{--window.location.href = "${request.contextPath}";--}%
                     }, 500);*/

                }
                else {
                    $("#loginAs").removeClass("loginSuccess");
                    showMsg("Login error", data.error)
                }
            }
        });
        return false;

    });

    //});

    function showMsg(title, msg) {
        $.Dialog({
            'title': title,
            'content': msg,
            'draggable': false,
            'overlay': true,
            'closeButton': false,
            'buttonsAlign': 'right',
            'position': {
                'zone': 'center'
            },
            'buttons': {
                'OK': {
                    'action': function () {
                    }
                }
            }
        });
    }


</script>