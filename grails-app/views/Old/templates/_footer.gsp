<div class="footer">

    <span class="fl_left">Copyright &copy; 2012 - All Rights
    Reserved - <a href="#">AlphaSigma</a>
    </span> <span class="fl_right"><g:link
        url="${resource(dir: '/', file: 'privacy')}">Privacy Policy</g:link>
</span>
    <span class="fl_right"><g:link
            url="${resource(dir: '/', file: 'terms')}">Terms & Conditions</g:link>
    </span>
</div>