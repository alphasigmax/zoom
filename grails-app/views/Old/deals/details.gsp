﻿<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta name="layout" content="application">


    <title>Zooom Deals</title>
    <style>
    .page-control {
        margin: 20px 0 0 50px;
    }

    .page-control ul {
        list-style: none;
        background-color: transparent;
    }

    .page-control ul li {
        display: inline;
        float: left;
        margin-bottom: 20px;
    }

    /* background color set to RGBA, with opacity on 0.3 and also using text-shadow */
    .page-control ul li a {
        padding: 20px;
        background: rgba(255, 138, 30, 0.3);
        text-decoration: none;
        font: bold 14px Helvetica, Sans-Serif;
        letter-spacing: -1px;
        color: #402e16;
        text-shadow: #eee 0px 0px 2px;
    }

    /* :first-child pseudo selector with rounded top left corner */
    .page-control ul li:first-child a {
        -moz-border-radius-topleft: 12px;
        -webkit-border-top-left-radius: 12px;
    }

    /* :last-child pseudo selector with rounded top right corner */
    .page-control ul li:last-child a {
        -moz-border-radius-topright: 12px;
        -webkit-border-top-right-radius: 12px;
    }

    /* hover state shows a linear gradient and opacity it brought down to 0.9 and also shows a very slight grey shadow on top */
    .page-control ul li a:hover {
        -moz-box-shadow: 0 -5px 10px #777;
        -webkit-box-shadow: 0 -5px 10px #777;
        background: -webkit-gradient(linear, right bottom, left top, color-stop(0, rgb(237, 227, 112)), color-stop(0.72, rgb(255, 173, 10))) !important;
        background: -moz-linear-gradient(right bottom, rgb(237, 227, 112) 0%, rgb(255, 173, 10) 72%) !important;
        background-color: rgb(255, 173, 10) !important;
        -moz-opacity: .90;
        filter: alpha(opacity=90);
        opacity: .90;
    }

    /* another RGBA background, now with an opacity of 0.8 */
    .page-control ul li a.active {
        background: rgba(255, 138, 30, 0.8) !important;
    }

    /* main contents with RGBA background (same colour as active tab) and three rounded corners */
    .frames {
        clear: both;
        background: rgba(255, 138, 30, 0.8);
        width: 90%;
        margin-left: 10px;
        -moz-border-radius-topright: 12px;
        -moz-border-radius-bottomright: 12px;
        -moz-border-radius-bottomleft: 12px;
        -webkit-border-top-right-radius: 12px;
        -webkit-border-bottom-right-radius: 12px;
        -webkit-border-bottom-left-radius: 12px;
    }

    /* header with a text-shadow */
    .frames h3 {
        text-transform: uppercase;
        padding: 20px 0 0 20px;
        color: #eee;
        text-shadow: #000 0px 0px 2px;
    }

    .frames p {
        padding-bottom: 20px;
        color: #ddd;
    }
    </style>

    <script
            src="<g:resource dir="js" file="/metroui/dialog.js" absolute="true"/>"></script>

    <script
            src="<g:resource dir="js" file="/metroui/rating.js" absolute="true"/>"></script>

</head>

<body class="metrouicss">

<script
        src="<g:resource dir="js" file="/paypal/simplecart.js" absolute="true"/>"></script>

<script>
    simpleCart({
        cartColumns: [
            { attr: "name", label: "Name"},
            { attr: "id", label: "ID" },
            { view: "currency", attr: "price", label: "Price"},
            { view: "decrement", label: false},
            { attr: "quantity", label: "Qty"},
            { view: "increment", label: false},
            { view: "currency", attr: "total", label: "SubTotal" },
            { view: "remove", text: "Remove", label: false}
        ],
        // "div" or "table" - builds the cart as a
        // table or collection of divs
        cartStyle: "div",
        // how simpleCart should checkout, see the
        // checkout reference for more info
        checkout: {
            type: "PayPal",
            email: "you@yours.com"
        }

    });
</script>

<div class="page">
<g:render template="/templates/header"/>



<div class="page-region">

    <div class="page-region-content">
        <div class="grid">
            <div class="row">
                <div class="span7">
                    <img
                            src="<g:createLink controller="adImage" action="index"/>?name=${deal.id}back460.jpg&"
                            alt="${deal.title}" class="home-tiles">
                </div>

                <div class="axnArea" style="height: 100px"></div>

                <div class="axnArea">

                    <!--  <script src="<g:resource dir="js" file="/paypal/paypal-button.js" absolute="true"/>?merchant=SMV78D8QFSEE8"
   data-env="sandbox"
   data-button="cart"
    data-name="${deal.title}"
    data-amount="1.00" 
     data-callback="http://www.merkwelt.com/people/stan/paypal/callback" 
    data-return="http://www.merkwelt.com/people/stan/paypal/success.html"
    
    > </script>
							
		
		
<form target="paypal" action="https://sandbox.paypal.com/cgi-bin/webscr" method="post" >
<input type="hidden" name="cmd" value="_cart">
<input type="hidden" name="business" value="SMV78D8QFSEE8">
<input type="hidden" name="lc" value="AU">
<input type="hidden" name="item_name" value="${deal.title}">
<input type="hidden" name="amount" value="1.00">
<input type="hidden" name="currency_code" value="AUD">
<input type="hidden" name="button_subtype" value="products">
<input type="hidden" name="no_note" value="0">
<input type="hidden" name="cn" value="Add special instructions to the seller:">
<input type="hidden" name="no_shipping" value="2">
<input type="hidden" name="add" value="1">
<input type="hidden" name="bn" value="PP-ShopCartBF:btn_cart_LG.gif:NonHosted">
<input type="image" src="https://www.paypalobjects.com/en_AU/i/btn/btn_cart_LG.gif" border="0" name="submit" alt="PayPal — The safer, easier way to pay online.">
<img alt="" border="0" src="https://www.paypalobjects.com/en_AU/i/scr/pixel.gif" width="1" height="1">
</form>
	-->
                    <br>

                    <div class="simpleCart_shelfItem">
                        <h2 class="item_name">
                            ${deal.title}
                        </h2>
                        <input type="text" value="1" class="item_Quantity"> <input
                            type="hidden" value="${deal.id}" class="item_id"> <span
                            class="item_price">
                        ${deal.curPrice}
                    </span> <a class="item_add" href="javascript:;">Add to Cart</a>
                    </div>

                </div>
            </div>
        </div>


        <div class="page-control" data-role="page-control">
            <!-- Responsive controls -->

            <span class="menu-pull"></span>

            <div class="menu-pull-bar"></div>
            <!-- Tabs -->
            <ul>
                <li class="active"><a href="#desc">Description</a></li>

                <li><a href="#loc">Location</a></li>
                <li><a href="#terms">Fine Print</a></li>
            </ul>

            <!-- Tabs content -->
            <div class="frames">
                <div class="frame active" id="desc">
                    <h3>
                        ${deal.title}
                    </h3>

                    <div id="postrating" class="rating" data-role="rating"></div>
                    <br> Detail:
                ${deal.description}
                </div>

                <div class="frame" id="loc">
                    <div id="map_canvas"
                         style="height: 350px; width: 100%; display: block"></div>
                </div>

                <div class="frame" id="terms">T&C</div>
            </div>
        </div>




        <script
                src="<g:resource dir="js" file="metroui/pagecontrol.js" absolute="true"/>"></script>

        <div class="social">
            <!-- AddThis Button BEGIN -->
            <div class="addthis_toolbox addthis_default_style ">
                <a class="addthis_button_facebook_like"
                   fb:like:layout="button_count"></a> <a
                    class="addthis_button_tweet"></a> <a
                    class="addthis_button_pinterest_pinit"></a> <a
                    class="addthis_counter addthis_pill_style"></a>
            </div>
            <script type="text/javascript">var addthis_config = {"data_track_addressbar": true};</script>
            <script type="text/javascript"
                    src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5185d9df24e54ad0"></script>
            <!-- AddThis Button END -->
        </div>


        <div class="comments">
            <div id="disqus_thread"></div>
            <script type="text/javascript">
                /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
                var disqus_shortname = 'zoomdeals';

                /* * * DON'T EDIT BELOW THIS LINE * * */
                (function () {
                    var dsq = document.createElement('script');
                    dsq.type = 'text/javascript';
                    dsq.async = true;
                    dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
                    (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
                })();
            </script>

            <a href="http://disqus.com" class="dsq-brlink">comments powered
            by <span class="logo-disqus">Disqus</span>
            </a>
        </div>

    </div>

</div>


<script type="text/javascript">
    function showMsg(title, msg) {
        $.Dialog({
            'title': title,
            'content': " Buy ${deal.title} for ${deal.id}",
            'draggable': false,
            'overlay': true,
            'closeButton': false,
            'buttonsAlign': 'right',
            'position': {
                'zone': 'center'},
            'buttons': {
                'OK': {
                    'action': function () {
                    }
                }
            }
        });
    }

    var map;
    var lat =${deal.location[0]};
    var lon =${deal.location[1]};

    function initialize() {
        google.maps.event.addDomListener(window, 'load', initializeMaps);
    }

    function initializeMaps() {
        var latlng = new google.maps.LatLng(lat, lon);
        var mapOptions = {
            center: latlng,
            zoom: 17,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);

        marker = new google.maps.Marker({
            map: map,
            title: "Deal Location",
            draggable: false
        });

        marker.setPosition(latlng);
        map.setCenter(latlng);
        //map.panTo(evt.latLng);
    }

    $(document).ready(
            function () {
                initializeMaps();
                google.maps.event.addDomListener(window.map, 'tilesloaded', resizeMaps);
            });

    function resizeMaps() {
        google.maps.event.trigger(window.map, 'resize');
    }


    $(function () {
        $("#postrating").on("click", function (e) {

            $.ajax({
                url: "<g:createLink controller="adPost" action="rateDeal" />",
                data: {
                    adid:${deal.id},
                    value: $("#postrating").RatingValue()
                },
                success: function (data) {
                    // alert(data)
                }
            });
        })


        $.ajax({
            url: "<g:createLink controller="adPost" action="showPersonalRating" />",
            data: {
                adid:${deal.id}
            },
            success: function (data) {

                $("#postrating").RatingValue(data.res.rating)
            }
        })


    })

</script>
</div>

<script
        src="<g:resource dir="js" file="/paypal/paypal-button-minicart.js" absolute="true"/>">

</script>

<!-- script>
    PAYPAL.apps.MiniCart.render({
        displayEdge: "right",
        edgeDistance: "50px",
        events: {
            afterAddToCart: function () {
                var msg = "There's now " + this.products.length + " unique product(s).";
                msg += " That's a total of " + this.calculateSubtotal() + "!";
console.log (msg)
              //  alert(msg);
            }
        }
    });
</script-->



<script
        src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>

</body>
</html>