<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <title>Elementary</title>
    <meta charset="iso-8859-1">

    <!--[if lt IE 9]><script src="scripts/html5shiv.js"></script><![endif]-->
    <!-- homepage scripts -->
    <r:require module="common"/>

    <style>
    #feedback {
        font-size: 1.4em;
    }

    #selectable .ui-selecting {
        background: #FECA40;
    }

    #selectable .ui-selected {
        background: #F39814;
        color: white;
    }

    #selectable {
        list-style-type: none;
        margin: 0;
        padding: 0;
        width: 450px;
    }

    #selectable li {
        margin: 3px;
        padding: 1px;
        float: left;
        width: 100px;
        height: 80px;
        font-size: 4em;
        text-align: center;
    }

    #Oceania {
        border: 0px
    }
    </style>
    <!-- / homepage scripts -->
</head>

<body class="home blog chrome et_includes_sidebar daily-deal full-width">
<div id="main-wrap" class="clearfix">

    <g:render template="/templates/header"/>

    <div id="main">
        <div id="wrapper">

            <!-- content -->
            <div class="wrapper row2">
                <div id="container" class="clear">
                    <!-- content body -->

                    <!-- Services area -->


                    Terms & Conditions


                    <!-- / content body -->
                </div>
            </div>

        </div>
    </div>
</div>
<!-- footer -->
<g:render template="/templates/footer"/>

</body>
</html>
