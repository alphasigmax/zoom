<!DOCTYPE html>
<html lang="en">
<head>
	<meta name="layout" content="loggedin" />
	<title>My Profile</title>

	<r:require module="timeline" />
</head>
<body>
	
      <!-- Page content -->
      
      <div class="account-content">
         <div class="container">
            <div class="row">
               <div class="col-sm-6 col-md-6">
                  <div class="profile-box">
                     <img src="<g:createLinkTo dir="/"  />images/flatize/profile-img.png" class="profile-pic-sty" alt=""/>
					 <h1 class="name-txt">Eddie Loben</h1>
					 <a href="#" class="btn btn-primary btn-xs edit-profile-btn" id="editprofile">Edit Profile</a>
					 
					 <div class="edit-form">
					 <div class="register-login" style="display:none;" id="editbox">
					   <form class="form-horizontal" role="form">
                    <div class="form-group">
					<label for="inputAddress" class="col-md-4 control-label label-txt">Your Name</label>
                      <div class="col-md-8">
                        <input type="text" class="form-control" id="inputName" placeholder="Name">
                      </div>
                    </div>            
                    <div class="form-group">
					<label for="inputAddress" class="col-md-4 control-label label-txt">Your Email</label>
                      <div class="col-md-8">
                        <input type="email" class="form-control" id="inputEmail1" placeholder="Email">
                      </div>
                    </div>
                    <div class="form-group">
					<label for="inputAddress" class="col-md-4 control-label label-txt">Phone Number</label>
                     <div class="col-md-8">
                        <input type="text" class="form-control" id="inputPhone" placeholder="Phone">
                      </div>
                    </div>
                    <div class="form-group">
					<label for="inputAddress" class="col-md-4 control-label label-txt">Your Country</label>
                      <div class="col-md-8">
                        <select class="form-control">
                          <option>Select Country</option>
                          <option>USA</option>
                          <option>India</option>
                          <option>Canada</option>
                          <option>UK</option>
                        </select>
                      </div>
                    </div>              
                    <div class="form-group">
					<label for="inputAddress" class="col-md-4 control-label label-txt">Your Address</label>
                        <div class="col-md-8">
                           <textarea class="form-control" rows="3" placeholder="Address"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
					<label for="inputAddress" class="col-md-4 control-label label-txt">Zip Code</label>
                      <div class="col-md-8">
                        <input type="text" class="form-control" id="inputZip" placeholder="Zip Code">
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <div class="col-md-offset-4 col-md-8">
                        <button type="submit" class="btn btn-danger">Save</button>
                       </div>
                    </div>
                  </form> 
                  </div>
				  </div>
				</div>
				<div class="clearfix"></div>
				  <hr>
				  <span class="wish-txt">Wishlist</span>
				  <button class="btn btn-primary btn-xs see-all-btn" type="button">See More</button>
				  <div class="clearfix"></div>
				  <div class="col-sm-6 col-sm-6">
				  <div class="row">
				   <div class="first-box">
				    <img src="<g:createLinkTo dir="/"  />images/flatize/wish-list2.jpg" class="wishlist-sty" alt=""/>
					<h2><strong>HTC</strong></h2>
					<p>Price:<strong>&nbsp;$530</strong></p>
				   </div> 
				   </div>
				  </div>
				  <div class="col-sm-6 col-sm-6">
				  <div class="first-box">
				    <img src="<g:createLinkTo dir="/"  />images/flatize/wish-list3.jpg" class="wishlist-sty" alt=""/>
					<h2><strong>Sony Xperia</strong></h2>
					<p>Price:<strong>&nbsp;$530</strong></p>
				   </div> 
				  </div>
				  
				  <div class="col-sm-6 col-sm-6">
				  <div class="row">
				  <div class="first-box">
				    <img src="<g:createLinkTo dir="/"  />images/flatize/wish-list4.jpg" class="wishlist-sty" alt=""/>
					<h2><strong>Nokia Asha</strong></h2>
					<p>Price:<strong>&nbsp;$530</strong></p>
				   </div> 
				   </div>
				   </div>
				   
				  <div class="col-sm-6 col-sm-6">
				  <div class="first-box">
				    <img src="<g:createLinkTo dir="/"  />images/flatize/wish-list5.jpg" class="wishlist-sty" alt=""/>
					<h2><strong>Black Barry</strong></h2>
					<p>Price:<strong>&nbsp;$530</strong></p>
				   </div> 
				  </div>
				  <div class="clearfix"></div>
				   <hr>
				   <span class="wish-txt">Edit Profile</span>
				  <button class="btn btn-primary btn-xs see-all-btn" type="button">See More</button>
				  <div class="clearfix"></div>
				  <div class="col-sm-6 col-sm-6">
				  <div class="row">
				   <div class="first-box">
				    <img src="<g:createLinkTo dir="/"  />images/flatize/wish-list2.jpg" class="wishlist-sty" alt=""/>
					<h2><strong>HTC</strong></h2>
					<p>Price:<strong>&nbsp;$530</strong></p>
				   </div> 
				   </div>
				  </div>
				  <div class="col-sm-6 col-sm-6">
				  <div class="first-box">
				    <img src="<g:createLinkTo dir="/"  />images/flatize/wish-list3.jpg" class="wishlist-sty" alt=""/>
					<h2><strong>Sony Xperia</strong></h2>
					<p>Price:<strong>&nbsp;$530</strong></p>
				   </div> 
				  </div>
				  
				  <div class="col-sm-6 col-sm-6">
				  <div class="row">
				  <div class="first-box">
				    <img src="<g:createLinkTo dir="/"  />images/flatize/wish-list4.jpg" class="wishlist-sty" alt=""/>
					<h2><strong>Nokia Asha</strong></h2>
					<p>Price:<strong>&nbsp;$530</strong></p>
				   </div> 
				   </div>
				   </div>
				   
				  <div class="col-sm-6 col-sm-6">
				  <div class="first-box">
				    <img src="<g:createLinkTo dir="/"  />images/flatize/wish-list5.jpg" class="wishlist-sty" alt=""/>
					<h2><strong>Black Barry</strong></h2>
					<p>Price:<strong>&nbsp;$530</strong></p>
				   </div> 
				  </div>
				  
               </div>
               <div class="col-sm-6 col-md-6">
				<section id="cd-timeline" class="cd-container">
  <div class="cd-timeline-block">
    <div class="cd-timeline-img cd-picture"> <i class="fa fa-shopping-cart"></i> </div>
    <!-- cd-timeline-img -->
    
    <div class="cd-timeline-content">
      <img class="img-responsive" src="<g:createLinkTo dir="/"  />images/flatize/wish-list2.jpg"> 
	  <span class="cd-date"><strong class="pro-txt">HTC</strong><br> $530<br>Feb 24</span></div>
	  
    <!-- cd-timeline-content --> 
  </div>
  <!-- cd-timeline-block -->
  
  <div class="cd-timeline-block">
    <div class="cd-timeline-img cd-movie"><i class="fa fa-shopping-cart"></i> </div>
    <!-- cd-timeline-img -->
    
    <div class="cd-timeline-content">
      <img class="img-responsive" src="<g:createLinkTo dir="/"  />images/flatize/wish-list2.jpg"> <span class="cd-date"><strong class="pro-txt">Sony Xperia</strong><br> $530<br>Feb 18</span> </div>
    <!-- cd-timeline-content --> 
  </div>
  <!-- cd-timeline-block -->
  
  <div class="cd-timeline-block">
    <div class="cd-timeline-img cd-picture"> <i class="fa fa-shopping-cart"></i> </div>
    <!-- cd-timeline-img -->
    
    <div class="cd-timeline-content">
      <img class="img-responsive" src="<g:createLinkTo dir="/"  />images/flatize/wish-list6.jpg"><span class="cd-date"><strong class="pro-txt">Nokia Asha</strong><br> $530<br>Jan 24</span> </div>
    <!-- cd-timeline-content --> 
  </div>
  <!-- cd-timeline-block -->
  
  <div class="cd-timeline-block">
    <div class="cd-timeline-img cd-location"><i class="fa fa-shopping-cart"></i> </div>
    <!-- cd-timeline-img -->
    
    <div class="cd-timeline-content">
      <img class="img-responsive" src="<g:createLinkTo dir="/"  />images/flatize/wish-list7.jpg"> <span class="cd-date"><strong class="pro-txt">HTC</strong><br> $530<br>Jan 14</span> </div>
    <!-- cd-timeline-content --> 
  </div>
  <!-- cd-timeline-block -->
  
  <div class="cd-timeline-block">
    <div class="cd-timeline-img cd-location"> <i class="fa fa-shopping-cart"></i></div>
    <!-- cd-timeline-img -->
    
    <div class="cd-timeline-content">
      <img class="img-responsive" src="<g:createLinkTo dir="/"  />images/flatize/wish-list2.jpg"> <span class="cd-date"><strong class="pro-txt">Sony Xperia</strong><br> $530<br>Jan 8</span> </div>
    <!-- cd-timeline-content --> 
  </div>
  <!-- cd-timeline-block -->
  
  <div class="cd-timeline-block">
    <div class="cd-timeline-img cd-movie"> <i class="fa fa-shopping-cart"></i> </div>
    <!-- cd-timeline-img -->
    
    <div class="cd-timeline-content">
      <img class="img-responsive" src="<g:createLinkTo dir="/"  />images/flatize/wish-list6.jpg">
      <span class="cd-date"><strong class="pro-txt">Nokia Asha</strong><br> $530<br>Jan 6</span> </div>
    <!-- cd-timeline-content --> 
  </div>
  <!-- cd-timeline-block --> 
</section>
<!-- cd-timeline --> 
				
				</div>
            </div>
            
           
         </div>
      </div>
     
	
	<r:require module="timeline" />

	
</body>
</html>