<html>
<head>

<title><g:layoutTitle default="Haggell" /></title>
<link rel="shortcut icon" href="#">

<meta charset="utf-8">
<meta name="description" content="Your description">
<meta name="keywords" content="Your,Keywords">
<meta name="author" content="alphasigma for haggell">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="layout" content="application">
<!-- Google Fonts -->
<link href='http://fonts.googleapis.com/css?family=Raleway:400,700'
	rel='stylesheet' type='text/css'>
<link
	href='http://fonts.googleapis.com/css?family=Roboto+Slab:400,700,300'
	rel='stylesheet' type='text/css'>
<g:layoutHead />

<!--  STYLE SHEETS -->
<r:require module="stylesheets" />
<r:layoutResources />



</head>
<body>
	<div id="page">


		<header>

			<g:render template="/templates/flatize/top" />
			<g:render template="/templates/flatize/nav" />
		</header>
		<!-- Begin Login -->
		<g:render template="/templates/flatize/login" />
		<!-- End Login -->
		<g:layoutBody />




		<r:require module="anonymous" />
		<r:layoutResources />

		<g:render template="/templates/flatize/footer" />

		<g:render template="/templates/flatize/styleswitch" />

		<g:render template="/templates/flatize/search" />

	</div>
</body>
</html>