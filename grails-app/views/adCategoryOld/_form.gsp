<%@ page import="com.luckydog.dealpost.AdCategory" %>

<div class="fieldcontain ${hasErrors(bean: adCategoryInstance, field: 'parentId', 'error')} ">
	<label for="parentId">
		<g:message code="adCategory.parentId.label" default="Parent Id" />
		
	</label>
	<g:textField name="parentId" value="${adCategoryInstance?.parentId}" />

</div>


<div class="fieldcontain ${hasErrors(bean: adCategoryInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="adCategory.name.label" default="Name" />
		
	</label>
	<g:textField name="name" value="${adCategoryInstance?.name}" />

</div>

<div class="fieldcontain ${hasErrors(bean: adCategoryInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="adCategory.description.label" default="Description" />
		
	</label>
	<g:textField name="description" value="${adCategoryInstance?.description}" />

</div>

<div class="fieldcontain ${hasErrors(bean: adCategoryInstance, field: 'imagePath', 'error')} ">
	<label for="imagePath">
		<g:message code="adCategory.imagePath.label" default="Image Path" />
		
	</label>
	<g:textField name="imagePath" value="${adCategoryInstance?.imagePath}" />

</div>

<div class="fieldcontain ${hasErrors(bean: adCategoryInstance, field: 'isPopular', 'error')} ">
	<label for="isPopular">
		<g:message code="adCategory.isPopular.label" default="Is Popular" />
		
	</label>
	<g:checkBox name="isPopular" value="${adCategoryInstance?.isPopular}" />

</div>

<div class="fieldcontain ${hasErrors(bean: adCategoryInstance, field: 'left', 'error')} ">
	<label for="left">
		<g:message code="adCategory.left.label" default="Left" />
		
	</label>
	<g:field type="number" name="left" value="${adCategoryInstance.left}" />

</div>





<div class="fieldcontain ${hasErrors(bean: adCategoryInstance, field: 'right', 'error')} ">
	<label for="right">
		<g:message code="adCategory.right.label" default="Right" />
		
	</label>
	<g:field type="number" name="right" value="${adCategoryInstance.right}" />

</div>

