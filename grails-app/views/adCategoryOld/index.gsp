
<%@ page import="com.luckydog.dealpost.AdCategory" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="homepage" />
	</head>
	<body>
		
		<div class="container">
		<div class="nav" role="navigation">
			<ul>
				<li><g:link class="create" action="create"> Create new Category</g:link></li>
			</ul>
		</div>
		
		<div id="list-adCategory" class="content scaffold-list" role="main">
			<h1>Category</h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table width="100%">
			<thead>
					<tr>
					
					<g:sortableColumn property="id" title="${message(code: 'adCategory.id.label', default: 'ID')}" />
					
						<g:sortableColumn property="description" title="${message(code: 'adCategory.description.label', default: 'Description')}" />
					
						<g:sortableColumn property="imagePath" title="${message(code: 'adCategory.imagePath.label', default: 'Image Path')}" />
					
						<g:sortableColumn property="isPopular" title="${message(code: 'adCategory.isPopular.label', default: 'Is Popular')}" />
					
						<g:sortableColumn property="left" title="${message(code: 'adCategory.left.label', default: 'Left')}" />
					
						<g:sortableColumn property="name" title="${message(code: 'adCategory.name.label', default: 'Name')}" />
					
						<g:sortableColumn property="parentId" title="${message(code: 'adCategory.parentId.label', default: 'Parent Id')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${adCategoryInstanceList}" status="i" var="adCategoryInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${adCategoryInstance.id}">${fieldValue(bean: adCategoryInstance, field: "id")}</g:link></td>
					
						<td><g:link action="show" id="${adCategoryInstance.id}">${fieldValue(bean: adCategoryInstance, field: "description")}</g:link></td>
					
						<td>${fieldValue(bean: adCategoryInstance, field: "imagePath")}</td>
					
						<td><g:formatBoolean boolean="${adCategoryInstance.isPopular}" /></td>
					
						<td>${fieldValue(bean: adCategoryInstance, field: "left")}</td>
					
						<td>${fieldValue(bean: adCategoryInstance, field: "name")}</td>
					
						<td>${fieldValue(bean: adCategoryInstance, field: "parentId")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${adCategoryInstanceCount ?: 0}" />
			</div>
		</div>
		
		</div>
	</body>
</html>
