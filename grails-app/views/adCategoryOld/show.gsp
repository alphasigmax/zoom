
<%@ page import="com.luckydog.dealpost.AdCategory" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="homepage" />
		<g:set var="entityName" value="${message(code: 'adCategory.label', default: 'AdCategory')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
	<div class="container">
		
		<div class="nav" role="navigation">
			<ul>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-adCategory" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol xclass="property-list adCategory">
			<blockquote>
				<li xclass="fieldcontain">
					<span id="description-label" class="property-label alert alert-success"><g:message code="adCategory.id.label" default="ID" /></span>
					
						<span class="property-value" aria-labelledby="description-label"><g:fieldValue bean="${adCategoryInstance}" field="id"/></span>
					
				</li>
				
				
				</blockquote>
				<blockquote>
				<g:if test="${adCategoryInstance?.description}">
				<li xclass="fieldcontain">
					<span id="description-label" class="property-label alert alert-success"><g:message code="adCategory.description.label" default="Description" /></span>
					
						<span class="property-value" aria-labelledby="description-label"><g:fieldValue bean="${adCategoryInstance}" field="description"/></span>
					
				</li>
				</g:if>
				</blockquote>
				<blockquote>
				<g:if test="${adCategoryInstance?.imagePath}">
				<li class="fieldcontain">
					<span id="imagePath-label" class="property-label alert alert-success"><g:message code="adCategory.imagePath.label" default="Image Path" /></span>
					
						<span class="property-value" aria-labelledby="imagePath-label"><g:fieldValue bean="${adCategoryInstance}" field="imagePath"/></span>
					
				</li>
				</g:if>
				</blockquote>
				<blockquote>
				<g:if test="${adCategoryInstance?.isPopular}">
				<li class="fieldcontain">
					<span id="isPopular-label" class="property-label alert alert-success"><g:message code="adCategory.isPopular.label" default="Is Popular" /></span>
					
						<span class="property-value" aria-labelledby="isPopular-label"><g:formatBoolean boolean="${adCategoryInstance?.isPopular}" /></span>
					
				</li>
				</g:if>
				</blockquote>
				<blockquote>
				<g:if test="${adCategoryInstance?.left}">
				<li class="fieldcontain">
					<span id="left-label" class="property-label alert alert-success"><g:message code="adCategory.left.label" default="Left" /></span>
					
						<span class="property-value" aria-labelledby="left-label"><g:fieldValue bean="${adCategoryInstance}" field="left"/></span>
					
				</li>
				</g:if>
				</blockquote>
				<blockquote>
				<g:if test="${adCategoryInstance?.name}">
				<li class="fieldcontain">
					<span id="name-label" class="property-label alert alert-success"><g:message code="adCategory.name.label" default="Name" /></span>
					
						<span class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${adCategoryInstance}" field="name"/></span>
					
				</li>
				</g:if>
				</blockquote>
				<blockquote>
				<g:if test="${adCategoryInstance?.parentId}">
				<li class="fieldcontain">
					<span id="parentId-label" class="property-label alert alert-success"><g:message code="adCategory.parentId.label" default="Parent Id" /></span>
					
						<span class="property-value" aria-labelledby="parentId-label"><g:fieldValue bean="${adCategoryInstance}" field="parentId"/></span>
					
				</li>
				</g:if>
				</blockquote>
				<blockquote>
				<g:if test="${adCategoryInstance?.right}">
				<li class="fieldcontain">
					<span id="right-label" class="property-label alert alert-success"><g:message code="adCategory.right.label" default="Right" /></span>
					
						<span class="property-value" aria-labelledby="right-label"><g:fieldValue bean="${adCategoryInstance}" field="right"/></span>
					
				</li>
				</g:if>
				</blockquote>
				
			</ol>
			<g:form url="[resource:adCategoryInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${adCategoryInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
		</div>
	</body>
</html>
