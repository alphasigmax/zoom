package com.luckydog.dealpost

import grails.rest.Resource

//@Resource(uri='/categories')
class AdCategory {

	String name
	String description
	//AdCategory parentCategory;
	//List<AdCategory> children = new ArrayList()
	// List<String> categoryPath = new ArrayList()
	static hasMany = [children: AdCategory]
	static belongsTo = [parentCategory: AdCategory]




	static constraints = {
		description blank: true
		name blank: true

	}



	public String toString() {
		"[$id]-$name"
	}


}
