package com.luckydog.dealpost

import com.luckydog.common.Address
import com.luckydog.merchant.Merchant
import com.luckydog.utils.Discounts

class AdPost {

    String title
    String description
    String tac
    List location

    Float curPrice = 0

	Date startDate = new Date()
    Date endDate
    
	Date dateCreated
    Date lastUpdated
    Long karma = 0
    Float avgRating = 0.0
    Boolean isActive = true
//    List<String> categorypath
    List<AdCategory> adCategories
    List<Address> address
	List<Discounts> discounts
    static hasMany = [adCategories: AdCategory,discounts: Discounts,address: Address]
	 
    static belongsTo = [merchant: Merchant]

    static embedded = ['adCategories']

    
    static constraints = {
        adCategories nullable: true
        tac nullable: true
    }


    static mapping = {
        location geoIndex: true
        //title index:true
        compoundIndex location: "2d", categories: 1
        description type: "text"
        tac type: "text"
    }


    public String toString() {
        "[$title]"
    }

   

}
