package com.luckydog.common

import com.luckydog.location.City
import com.luckydog.location.CityLocation

class Address {

	boolean isPrimary = false
	String email
	String fax
	String website
	String addressOne
	String addressTwo
	String postCode
	List phone;
	CityLocation city;
	String description
	static hasMany = [phone: Phone]
	static hasOne = [city: City]

	List location


	static constraints = {
		email nullable: true
		phone nullable:true
		fax nullable: true
		website nullable: true
		addressOne nullable: true
		addressTwo nullable: true
		postCode nullable: true
		description nullable: true
	}

	static mapping = {
		location geoIndex: true
		city lazy: false
		phone lazy: false
	}
}
