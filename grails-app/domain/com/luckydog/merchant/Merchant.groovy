package com.luckydog.merchant

import com.luckydog.common.Address
import com.luckydog.dealpost.AdPost
import com.luckydog.security.User

class Merchant extends User {

	String description
	List adposts
	List address
	static hasMany = [adposts: AdPost, address: Address]

	static constraints = {
		//  adposts nullable: true
		description nullable:true
		//	address nullable:true
	}

	static mapping = {
		description type: "text"
		//title index:true
		//compoundIndex location:"2d", categories:1
		
			address lazy: false
		
	}

	public String toString() {
		"[$id / $username]"
	}
}
