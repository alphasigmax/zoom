package com.luckydog.location

class CityLocation {

	
long	geonameId
String continentCode,continentName,countryIsoCode,countryName,subdivisionIsoCode,subdivisionName,cityName,metroCode,timeZone


static constraints = {
	continentCode  nullable: true
	continentName  nullable: true
	countryIsoCode  nullable: true
	countryName  nullable: true
	subdivisionIsoCode  nullable: true
	subdivisionName  nullable: true
	cityName  nullable: true
	metroCode  nullable: true
	timeZone  nullable: true
	
}

String toString()
{
	"$cityName / $countryName"
}

}
