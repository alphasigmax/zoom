package com.luckydog.location

class CityBlocks {

	/*network_start_ip,network_prefix_length,geoname_id,
	 * registered_country_geoname_id,represented_country_geoname_id,postal_code,latitude,longitude,
	 * is_anonymous_proxy,is_satellite_provider
	 */


	long geonameId
	String 	networkStartIp,postalCode
	int networkPrefixLength=0
	long registeredCountryGeonameId,representedCountryGeonameId

	double latitude,longitude

	static constraints = {

		networkStartIp  nullable: true
		networkPrefixLength  nullable: true
		registeredCountryGeonameId  nullable: true
		representedCountryGeonameId  nullable: true
		postalCode  nullable: true
	}
}
