package com.luckydog.location

class CountryLocation {
/*
 * geoname_id / 
continent_code / 
continent_name / 
country_iso_code / 
country_name / 
subdivision_iso_code / 
subdivision_name / 
city_name / 
metro_code / 
time_zone / 
 */

	long geonameId
	String 	continentCode,continentName,countryIsoCode,countryName,subdivisionIsoCode,subdivisionName,cityName,metroCode,timeZone
	
	
	static constraints = {
		continentCode  nullable: true
		continentName  nullable: true
		countryIsoCode  nullable: true
		countryName  nullable: true
		subdivisionIsoCode  nullable: true
		subdivisionName  nullable: true
		cityName  nullable: true
		metroCode  nullable: true
		timeZone  nullable: true
		
	}
	
}
