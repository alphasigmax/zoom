package com.luckydog.shopper

class ShopItem {

    private static transient final UNKNOWN = -1
    private static transient final INCART = 1
    private static transient final CHECKEDOUT = 2
    private static transient final PAID = 4
    private static transient final PROCESSED = 8
    private static transient final SHIPPED = 12
    private static transient final EXPIRED = 18


    Long adPostId
    Long userName
    Float price
    Float taxes = 0
    Float totalPrice
    String title
    Integer currentStatus = -1
}
