package com.luckydog


import com.luckydog.security.User


class PublicController {

    def springSecurityService

    def landing = {
        render(view: '/layouts/applicationLanding')
    }

    def showDeals = {
        render(view: '/public/showDeals')
    }

//    def signUp = {
//        forward(controller: 'login', action: 'auth', params: [fromSignupPage: true])
//    }

//    def createUser = { SignUpCO signUpCO ->
//        println("-----------Name" + signUpCO.name)
//        if (signUpCO.validate()) {
//            User user = new User(signUpCO)
//            user.password = springSecurityService.encodePassword(signUpCO.password)
//            user.save(flush: true);
//            println "********* User Saved *********"
//            UserRole.create(user, Role.findByAuthority('ROLE_USER'), true)
//            springSecurityService.reauthenticate(user.username)
//            redirect(action: 'dashBord', controller: 'public')
//        } else {
//            signUpCO.errors.allErrors.each {
//                println("---------------" + it)
//            }
//            def config = SpringSecurityUtils.securityConfig
//
//            if (springSecurityService.isLoggedIn()) {
//                redirect uri: config.successHandler.defaultTargetUrl
//                return
//            }
//            String postUrl = "${request.contextPath}${config.apf.filterProcessesUrl}"
//            render(view: '/login/auth', model: [signUpCO: signUpCO, postUrl: postUrl,
//                    rememberMeParameter: config.rememberMe.parameter, fromSignUpPage: true])
//        }
//    }



    def dashBord = {

        User user = springSecurityService.currentUser as User
        render(view: '/public/dashBord', model: [user: user])
    }

    def loginAjax = {
        render(view: '/public/signUpAjax')
    }
}
