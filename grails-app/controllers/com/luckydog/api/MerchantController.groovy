package com.luckydog.api

import grails.converters.JSON

import org.springframework.security.access.annotation.Secured

import com.luckydog.common.Address
import com.luckydog.common.Phone
import com.luckydog.dealpost.AdPost
import com.luckydog.location.CityLocation
import com.luckydog.merchant.Merchant
import com.luckydog.security.Role
import com.luckydog.security.User
import com.luckydog.security.UserRole
import com.luckydog.utils.ErrorObject
import com.luckydog.utils.ImageTool
import com.luckydog.utils.SuccessObject

class MerchantController {
	def gridfsService
	def springSecurityService
	static jsonify = "*"


	def doregister( ) {
		
		Merchant merchantInstance= new Merchant()
		merchantInstance.properties=request
			/*
		Address address = new Address()
		address.website="http://www.json.com"
		address.fax="1234556"
		Phone phone = new Phone()
		phone.phoneNumber ="1234"
		phone.phoneType ="Mobile"
		address.phone= new ArrayList()
		address.phone << phone
		CityLocation city = CityLocation.get(1)
		address.city=city
		merchantInstance.address= new ArrayList()
		merchantInstance.address << address
			String res = merchantInstance as JSON
		println " JSON is \n\n $res "
	
	
		
		City city = new City()
		city.name="Melbourne"
		city.state="Victoria"
		city.location = new ArrayList()
		city.location << 134
		city.location << 145
		city.IpAddr="15.2.2.2"
		
		
		Country country = new Country()
		country.name ="Australia"
		country.countryCode="AU"
		country.ipAddr="15.2.2.2"
		country.location = new ArrayList()
		country.location << 134
		country.location << 145
		country.cities =  new ArrayList()
		country.cities <<city
		city.country=country
		country.save(flush:true)
		
		address.city =city
		
		
		*/
	
			if ( !merchantInstance.validate()) {
				log.error "Invalid merchant data $merchantInstance"
				ErrorObject err = new ErrorObject()
				err.errorNumber = 301
				err.errorDescription = "Invalid data. "
				int i=0;
				merchantInstance.errors.allErrors.each
				{
					err.errors [i++]= it
				}
				
				//FIXME. Show what errors are invalid 
				return [error:err];
			}
			
			if(User.findByUsername(merchantInstance.username))
			{
				ErrorObject err = new ErrorObject()
				err.errorNumber = 301
				err.errorDescription = "Email is already registered. <br> Please enter a different email address or login to continue"
				return [error:err];
			}

		if (!merchantInstance.save(flush: true, failOnError: true)) {
			log.error "Could not save merchant $merchantInstance"
			return  [merchant:merchantInstance]
		}

		Role userRole = Role.findByAuthority('ROLE_MERCHANT')
		UserRole ur = UserRole.create(merchantInstance, userRole, true)
		ur.save(flush: true)


		if(params.logo)
		{
			def file = request.getFile('logo')
			if (file) {
				ImageTool imageTool = new ImageTool()
				imageTool.load(file.getBytes())
				imageTool.thumbnail(120)
				byte[] arr = imageTool.getBytes("JPEG")
				gridfsService.saveFile(arr, file.contentType, merchantInstance.id.toString(), "merchant.jpg", 120, 120)
			}

		}

		return [merchant:merchantInstance] 
	}


	@Secured(['ROLE_ADMIN'])
	def deleteProfile() {
		User user = springSecurityService.currentUser as User
		Merchant merchant = Merchant.findByUsername(user.username)
		if (merchant) {
			merchant.delete(flush: true)
			[model: new SuccessObject(code: 200, message: "Success Delete")]
		} else {
			ErrorObject err = new ErrorObject()
			err.errorNumber = 302
			err.errorDescription = "No such AdPost"
			return [model: err]
		}
	}

	@Secured(['ROLE_MERCHANT'])
	def deactivateProfile() {
		User user = springSecurityService.currentUser as User
		Merchant merchant = Merchant.findByUsername(user.username)
		if (merchant) {
			merchant.enabled = false
			merchant.save(flush: true)
			[model: new SuccessObject(code: 200, message: "Success Deactivate")]
		} else {
			ErrorObject err = new ErrorObject()
			err.errorNumber = 302
			err.errorDescription = "No such AdPost"
			return [model: err]
		}
	}

	def showProfile() {
		User user = springSecurityService.currentUser as User
		Merchant merchant = Merchant.findByUsername(user.username)
		if (merchant) {
			return [model: merchant]
		} else {
			ErrorObject err = new ErrorObject()
			err.errorNumber = 302
			err.errorDescription = "No such Merchant"
			return [model: err]
		}
	}

	@Secured(['ROLE_MERCHANT'])
	def updateProfile() {
		User user = springSecurityService.currentUser as User
		Merchant merchant = Merchant.findByUsername(user.username)
		//BUGFIX put in excludes
		merchant.properties = params
		if (!merchant.save(flush: true)) {
			ErrorObject err = new ErrorObject();
			err.errorDescription = "Could not Save Merchant"
			err.errorNumber = 501
			return [model: err]
		}
		return [model: merchant]
	}


	@Secured(['ROLE_MERCHANT'])
	def showMyItems() {
		User user = springSecurityService.currentUser as User
		Merchant merchant = Merchant.findByUsername(user.username)
		if (!merchant) {
			ErrorObject err = new ErrorObject()
			err.errorNumber = 302
			err.errorDescription = "No such merchant"
			return [model: err]
		}
		println "Found merchant ${merchant.adposts}"
		return [model: merchant.adposts]
	}


	@Secured(['ROLE_MERCHANT'])
	def deletead() {
		long adid = params.adid as long
		log.info "Deleting AdPost .. $adid"
		User user = springSecurityService.currentUser as User
		Merchant merchant = Merchant.findByUsername(user.username)

		if (!merchant) {
			ErrorObject err = new ErrorObject()
			err.errorNumber = 302
			err.errorDescription = "No such merchant"
			return [model: err]
		}

		AdPost post = merchant.adposts.find { it.id == adid }
		if (post) {
			post.delete(flush: true)
			return [model: ["OK"]]
		} else {
			ErrorObject err = new ErrorObject()
			err.errorNumber = 302
			err.errorDescription = "No such AdPost"
			return [model: err]
		}
	}

	@Secured(['ROLE_MERCHANT'])
	def deactivatead() {
		Long adid = params.long('adid')
		log.info ".... Deactivating AdPost .... $adid"
		User user = springSecurityService.currentUser as User
		Merchant merchant = Merchant.findByUsername(user.username)

		if (!merchant) {
			ErrorObject err = new ErrorObject()
			err.errorNumber = 302
			err.errorDescription = "No such merchant"
			return [model: err]
		}
		AdPost post = merchant.adposts.find { it.id == adid }
		if (post) {
			post.isActive = false
			post.save(flush: true)
			return [model: ["OK"]]
		} else {
			ErrorObject err = new ErrorObject()
			err.errorNumber = 302
			err.errorDescription = "No such AdPost"
			return [model: err]
		}
	}

	@Secured(['ROLE_MERCHANT'])
	def activatead() {
		println("Under activated.............")
		long adid = params.adid as long
		log.info "Activating AdPost .. $adid"
		User user = springSecurityService.currentUser as User
		Merchant merchant = Merchant.findByUsername(user.username)

		if (!merchant) {
			ErrorObject err = new ErrorObject()
			err.errorNumber = 302
			err.errorDescription = "No such merchant"
			render err as JSON
			//return [model: err]
		}

		AdPost post = merchant.adposts.find { it.id == adid }
		if (post) {
			post.isActive = true
			post.save(flush: true)
			//render "OK+" as JSON;
			return [model: ["OK"]]
		} else {
			ErrorObject err = new ErrorObject()
			err.errorNumber = 302
			err.errorDescription = "No such AdPost"
			render err as JSON
			//return [model: err]
		}
	}
}
