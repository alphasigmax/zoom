package com.luckydog.api

import com.luckydog.dealpost.AdCategory
import grails.converters.JSON;

class CategoryController {

    //not functioning!
    /*
     * This function must return the current category and iterate through all the subcategories and return them as well.
     * We have to iterate through all the children
     */

    def index() {

        def adCategoryInstance
        List categoryList = []

        if (params.id) {
            getChildCategory();
            /*adCategoryInstance = AdCategory.get(params.id)
            List childCatList=adCategoryInstance.childCategory;
            childCatList.each {
                categoryList.add(AdCategory.findById((it as Long)));
            }*/
        } else {
            adCategoryInstance = AdCategory.getAll()
            adCategoryInstance.each {
                categoryList.add(it)
            }
        }
        //List catChildren = adCategoryInstance.childCategory

        //iterate through all the children
        render(categoryList as JSON)
        //return the result as tree
        //return [model: [res: adCategoryInstance]]
    }

    def createCategory() {
        println("++++++++++++++++++++Under create Category method+++++++++++params" + params)
        Long id = (params.parentid) as Long;
        println("+++++++++++++++parentId+++++++++++" + params.parentid);
        AdCategory parentCategory = AdCategory.findById(id);
        if (parentCategory) {
            AdCategory newChildCategory = new AdCategory();

            newChildCategory.name = params.name;
            newChildCategory.imagePath = params.image;
            newChildCategory.description = params.description;

            newChildCategory.save(flush: true)
            //List<String> listA=[]
            List<String> allChildOfCat = parentCategory.childCategory;
            //listA.add((newChildCategory.id.toString()));
            //listA.each {
            allChildOfCat.add((newChildCategory.id.toString()))
            //}
            parentCategory.childCategory = allChildOfCat;


            parentCategory.save()
            println("parent category save")
            render newChildCategory as JSON
        } else {
            AdCategory defaultParentCategory = new AdCategory();

            defaultParentCategory.name = params.name;
            defaultParentCategory.parentId = defaultParentCategory.getDefaultParentCategory();
            defaultParentCategory.imagePath = params.image;
            defaultParentCategory.description = params.description;

            defaultParentCategory.save();
            render defaultParentCategory as JSON

        }
    }
    def deleteCategory = {
        AdCategory adCategory = AdCategory.findById(params.id);

        if (adCategory) {
            adCategory.delete();
            render(adCategory as JSON)
        } else {
            return [model: ["category not found with this id"]]
        }

    }
    def getChildCategory = {
        AdCategory adCategory = AdCategory.findById(params.id);
        if (adCategory) {
            List<String> childCategoryIdList = adCategory.getChildCategory();
            List<AdCategory> childCategoryList = []
            childCategoryIdList.each {
                Long id = it as Long
                childCategoryList.add(AdCategory.findById(id))
            }
            render childCategoryList as JSON;
        } else {
            return [model: ["category not found with this id"]]
            println("not a category with this id")
        }
    }

}
