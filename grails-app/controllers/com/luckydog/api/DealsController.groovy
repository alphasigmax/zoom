package com.luckydog.api

import grails.converters.JSON

import org.springframework.security.access.annotation.Secured

import com.luckydog.dealpost.AdPost
import com.luckydog.utils.ErrorObject
import com.luckydog.utils.Sorter
import com.mongodb.BasicDBObject
import com.mongodb.DBCursor
import com.mongodb.DBObject


class DealsController {

    def mongo
    def springSecurityService

    def signUp = {

        render(view: "/public/adDeals")
    }
	
	def showDeal()
	{
		render(view: "/detail")
	}

    def index() {

        //TODO check the max of both
        int max = params.int('max') ?: 25
        int offsetCount = params.int('offset') ?: 0


        if (!params.containsKey('lat') || !params.containsKey('lon')) {
			log.warn "No latitude or longitude enabled"
            ErrorObject err = new ErrorObject();
            err.errorDescription = "Missing the parameter, latitude or longitude"
            err.errorNumber = 201

            render  err as JSON
            //For the second phase we get the UUID and retrieve the last know good location
        }


        def adPosts = AdPost.withCriteria {


            if (params.lat && params.lon) {
                double lat = params.lat as double
                double lon = params.lon as double
                int radius = 10
                def locs = [lat, lon];
                'withinCircle'('location', [locs, radius])
            }


            eq('isActive', true)

            if (params.containsKey('merchantId')) {
                Long merchantID = params.merchantId as long
                eq('merchant', merchantID)
                //				query.put("merchant.\$ref", "merchant")

            }
            String searchTerm = params.searchTerm
            if (params.containsKey('searchTerm')) {

                def pattern = ~/${searchTerm}/
                'like'("description", pattern)

            }
            //sorting by lastUpdated date
            if (params.sortby) {
                def sorter = params.sortby as int
                switch (sorter) {
                    case Sorter.WHATS_HOT:
                        order("karma", "asc")
                        break
                    case Sorter.RATING:
                        order("avgRating", "asc")
                        break
                    case Sorter.LAST_UPDATED:
                        order("lastUpdated", "asc")
                        break
                    case Sorter.DEAL_PERCENT:
                        order("percent", "asc")
                        break
                    default:
                        order("karma", "asc")

                }

            } else {
                order("karma", "asc")
            }

            if (params.categoryIds) {
                'in'('categoryPath', [params.categoryIds])
            }

            maxResults(max)
            offset(offsetCount)
        }
        DBCursor cursor = adPosts.cursor
      
        log.debug("+++++++++++++++++++++++++" + (getAllPosts(cursor) as JSON));

        render adPosts as JSON;
        // return [model: getAllPosts(cursor)]
    }


    def findByMerchant() {
        if (!params.merchantId) {
            ErrorObject err = new ErrorObject();
            err.errorDescription = "Missing MerchantID"
            err.errorNumber = 201
            return [model: err]

        }
        int max = params.int('max') ?: 20
        int offset = params.int('offset') ?: 0

        def db = mongo.getDB(grailsApplication.config.mongodb.databaseName)
        def coll = db.getCollection("adPost")

        long merchantID = params.merchantId as long

        BasicDBObject query = new BasicDBObject("merchant.\$ref", "merchant");

        query.put("merchant.\$id", merchantID)
        query.sort({ lastUpdated: 1 });                                                     //        }


        DBCursor cursor = coll.find(query).limit(max)


        return [model: getAllPosts(cursor)]

    }


    def findNearby() {
		if (!params.adid) {
			ErrorObject err = new ErrorObject();
			err.errorDescription = "Missing AdID"
			err.errorNumber = 201
			return [model: err]

		}
		
        def adid = params.adid as long
        def post = AdPost.get(adid)
        def locs = post.location
        def db = mongo.getDB(grailsApplication.config.mongodb.databaseName)
        def coll = db.getCollection("adPost")
        //TODO set the max distance
        int max = params.int('max') ?: 20
        int offset = params.int('offset') ?: 0
        BasicDBObject filter = new BasicDBObject("\$near", locs as double[]);
        BasicDBObject query = new BasicDBObject("location", filter);
        query.sort({ lastUpdated: 1 });
        DBCursor cursor = coll.find(query).limit(max)
        return [model: getAllPosts(cursor)]
    }


    def findRelated() {
        def id = params.adid as long
        AdPost post = AdPost.get(id)
        def locs = post.location
        def db = mongo.getDB(grailsApplication.config.mongodb.databaseName)
        def coll = db.getCollection("adPost")
        //TODO set the max distance
        int max = params.int('max') ?: 20
        int offset = params.int('offset') ?: 0
        BasicDBObject filter = new BasicDBObject("\$near", locs as double[]);
        BasicDBObject query = new BasicDBObject("location", filter);
        query.sort({ karma: 1 });
        String searchTerm = post.title
        def pattern = ~/${searchTerm}/
        //query.put("description",  pattern)
        DBCursor cursor = coll.find(query).limit(max)
        println("-----------------------------" + (getAllPosts(cursor) as JSON));
        render(getAllPosts(cursor) as JSON);
        //return [model: getAllPosts(cursor)]
    }


    private def getAllPosts(DBCursor cursor) {
        def allPosts = []

        if (cursor != null) {
            //			cursor.each {
            //				def merchant=[id:it.merchant]
            //				def newPost=[id:it._id,curPrice:it.curPrice,description:it.description,percent:it.percent,title:it.title, merchant:merchant,location:it.location, priorPrice: it.priorPrice, email:it.email,dateCreated:it.dateCreated]
            //				allPosts<<newPost;qqqqqqqqqqq
            //			}
            while (cursor.hasNext()) {

                DBObject post = cursor.next();
                def merchant = [id: post.merchant]
                def newPost = [id: post._id, curPrice: post.curPrice, description: post.description, percent: post.percent, title: post.title, merchant: merchant, location: post.location, priorPrice: post.priorPrice, email: post.email, dateCreated: post.dateCreated]
                allPosts << newPost;
            }
        }
        return allPosts
    }

    //this is for future versions!
    def findByBox() {
        //form a bounding box
        if (!params.containsKey('TRlat') || !params.containsKey('TRlon') || !params.containsKey('BLlat') || !params.containsKey('BLlon')) {
            ErrorObject err = new ErrorObject();
            err.errorDescription = "Missing the parameter, lattitude or longitude"
            err.errorNumber = 201
            return [model: err]

        }
        float TRlat = params.TRlat as float
        float TRlon = params.TRlon as float
        float BLlat = params.BLlat as float
        float BLlon = params.BLlon as float


        LinkedList<double[]> box = new LinkedList<double[]>();

        // Set the lower left point
        box.addLast([BLlat, BLlon] as float[])
        // Set the upper right point
        box.addLast([TRlat, TRlat] as float[]);
        final BasicDBObject query = new BasicDBObject("location", new BasicDBObject("\$within", new BasicDBObject("\$box", box)));

        int max = params.int('max') ?: 20
        def db = mongo.getDB(grailsApplication.config.mongodb.databaseName)
        def coll = db.getCollection("adPost")
        DBCursor cursor = coll.find(query).limit(max)
        return [model: getAllPosts(cursor)]
    }

}
