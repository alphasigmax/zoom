package com.luckydog.api

import com.luckydog.location.CityBlocks
import com.luckydog.location.CityLocation
import com.luckydog.location.CountryBlocks
import com.luckydog.location.CountryLocation



class PublicApiController {

	def gridfsService
	def springSecurityService
	def mongo

	def getCities() {
		//def db = mongo.getDB(grailsApplication.config.mongodb.databaseName)
		//def coll = db.getCollection("city_location")
		def countryfile=getFile("GeoLite2-Country-Locations.csv")

		countryfile.splitEachLine(",") {fields ->



			println "#" *25
		}

	}


	def loadCityBlocks()
	{
		def cityFile=getFile("GeoLite2-City-Blocks.csv")
		int i =0;

		cityFile.splitEachLine(",") {fields ->
			i++

			String msg="$i parsing ${fields[2]} ...</br>"
			render msg
			println msg

			if(fields[2].isLong()) {
				CityBlocks cityBlock =null
				cityBlock= new CityBlocks()
				cityBlock.setGeonameId(Long.parseLong(fields[2]))
				cityBlock.setNetworkStartIp(fields[0])
				cityBlock.setNetworkPrefixLength(Integer.parseInt(fields[1]?:"0"))
				cityBlock.setRegisteredCountryGeonameId(Long.parseLong(fields[3]?:"0"))
				cityBlock.setRepresentedCountryGeonameId(Long.parseLong(fields[4]?:"0"))
				cityBlock.setPostalCode(fields[5])
				cityBlock.setLatitude(Double.parseDouble(fields[6]?:"0"))
				cityBlock.setLongitude(Double.parseDouble(fields[7]?:"0"))
				cityBlock.save(flush:true)
				render "Saved </br>"
				print "."
				render "<hr/>"
			}
			else
			{
				render "skipping ${fields[2]} </br>"
			}
		}
	}




	def loadCountryBlocks()
	{
		/*network_start_ip,network_prefix_length,geoname_id,
		 * registered_country_geoname_id,represented_country_geoname_id,postal_code,latitude,longitude,
		 * is_anonymous_proxy,is_satellite_provider
		 */

		def countryfile=getFile("GeoLite2-Country-Blocks.csv")
		int i =0;

		countryfile.splitEachLine(",") {fields ->
			i++

			String msg="$i parsing ${fields[2]} ...</br>"
			render msg
			println msg


			if(fields[2].isLong()) {

				CountryBlocks countryBlock =null//=CountryBlocks.findByGeonameId(Long.parseLong(fields[2]))
				if(null==countryBlock)
				{
					countryBlock= new CountryBlocks()
					countryBlock.setGeonameId(Long.parseLong(fields[2]))
					countryBlock.setNetworkStartIp(fields[0])
					countryBlock.setNetworkPrefixLength(Integer.parseInt(fields[1]?:"0"))
					countryBlock.setRegisteredCountryGeonameId(Long.parseLong(fields[3]?:"0"))
					countryBlock.setRepresentedCountryGeonameId(Long.parseLong(fields[4]?:"0"))
					countryBlock.setPostalCode(fields[5])
					countryBlock.setLatitude(Double.parseDouble(fields[6]?:"0"))
					countryBlock.setLongitude(Double.parseDouble(fields[7]?:"0"))
					countryBlock.save(flush:true)
					render "Saved </br>"
					print "."

				}else
				{
					render "Already exists. Skipping</br>"
				}



				render "<hr/>"
			}
			else
			{
				print "skipping ${fields[2]}"
			}
		}
	}
	/*
	 * geoname_id,continent_code,continent_name,country_iso_code,country_name,subdivision_iso_code,
	 * subdivision_name,city_name,metro_code,time_zone
	 */
	def loadCountryLocation()
	{
		def countryfile=getFile("GeoLite2-Country-Locations.csv")

		countryfile.splitEachLine(",") {fields ->

			render "parsing ${fields[0]} ...</br>"
			print "."
			if(fields[0].isLong()) {

				CountryLocation countryLoc =CountryLocation.findByCountryIsoCode(fields[3])
				if(null==countryLoc)
				{
					countryLoc= new CountryLocation()
					countryLoc.setGeonameId(Long.parseLong(fields[0]))
					countryLoc.setContinentCode (fields[1])
					countryLoc.setContinentName(cleanString(fields[2]))
					countryLoc.setCountryIsoCode(fields[3])
					countryLoc.setCountryName(cleanString(fields[4]))
					countryLoc.setSubdivisionIsoCode(fields[5])
					countryLoc.setSubdivisionName(fields[6])
					countryLoc.setCityName(cleanString(fields[7]))
					countryLoc.setMetroCode(fields[8])
					countryLoc.setTimeZone(fields[9])

					countryLoc.save(flush:true)
					render "Saved </br>"

				}else
				{
					render "Already exists. Skipping</br>"
				}



				render "<hr/>"
			}
		}
	}

	def deleteCityLocation()
	{
		def db = mongo.getDB(grailsApplication.config.mongodb.databaseName)
		def coll = db.getCollection("cityLocation")


	}

	def loadCityLocation()
	{
		def cityFile=getFile("GeoLite2-City-Locations.csv")
		int i =0;
		cityFile.splitEachLine(",") {fields ->
			i++
			render "$i parsing ${fields[0]} ...</br>"
			if(fields[0].isLong()) {

				CityLocation cityLocation =CityLocation.findByGeonameId(Long.parseLong(fields[0]))
				if(null==cityLocation)
				{
					cityLocation= new CityLocation()
					cityLocation.setGeonameId(Long.parseLong(fields[0]))
					cityLocation.setContinentCode (fields[1])
					cityLocation.setContinentName(cleanString(fields[2]))
					cityLocation.setCountryIsoCode(fields[3])
					cityLocation.setCountryName(cleanString(fields[4]))
					cityLocation.setSubdivisionIsoCode(fields[5])
					cityLocation.setSubdivisionName(cleanString(fields[6]))
					cityLocation.setCityName(cleanString((fields[7])))
					cityLocation.setMetroCode(fields[8])
					cityLocation.setTimeZone(fields[9])
					cityLocation.save(flush:true)
					render "Saved </br>"

				}else
				{
					render "Already exists. Skipping</br>"
				}



				render "<hr/>"
			}
		}
	}

	private String cleanString(String string)
	{
		return string?.replace("\"", "")?.replace("'", "")
	}

	private def getFile(String fname)
	{
		return grailsApplication.mainContext.getResource('/WEB-INF/resources/'+fname).file
	}
	//static def listInitSize;

	/*def checkUniqueEmail = {
	 println '-------checkUniqueEmail-----------------------' + params
	 Map resultMap = ['resultData': 'false']
	 if (User.countByUsername(params.email)) {
	 resultMap['resultData'] = 'true'
	 }
	 println "============resultMap=============" + resultMap
	 render resultMap as JSON
	 }
	 def getDealsJSON = {
	 println '---------getDealsJSON------------Params----------------------' + params
	 List<AdPost> adPostList = AdPost.listOrderById();
	 List<AdPostVO> adPostVOList = new ArrayList<AdPostVO>();
	 adPostList.each {
	 adPostVOList.add(new AdPostVO(it));
	 }
	 render adPostVOList as JSON
	 }
	 def getMerchantsDealJSON = {
	 Merchant merchant = springSecurityService.currentUser as Merchant;
	 List<AdPost> adPostList = AdPost.findAllByMerchant(merchant);
	 List<AdPostVO> adPostVOList = new ArrayList<AdPostVO>();
	 adPostList.each {
	 adPostVOList.add(new AdPostVO(it));
	 }
	 println('+++++++++++++++++++total size of current user posts.........'+adPostVOList.size());
	 render adPostVOList as JSON;
	 }
	 def getRecentMerchantDealJSON = {
	 Merchant merchant = springSecurityService.currentUser as Merchant;
	 AdPost adPost = AdPost.findByMerchant(merchant, [order: 'desc', sort: 'dateCreated']);
	 AdPostVO adPostVO = new AdPostVO(adPost);
	 println '--------getRecentMerchantDealJSON adPost-------------' + (adPost as JSON)
	 render adPostVO as JSON;
	 }
	 //    *********************************************** API Actions ****************************************
	 def editMerchantDealJSON() {
	 println("Under------------------editMerchantDealJSON-------------------" + params.id);
	 AdPost adPostEdit = AdPost.get(params.id);
	 AdPostVO adPostVO = new AdPostVO(adPostEdit);
	 def adPostVOJSON = adPostVO as JSON;
	 println("this is json of edit deal=====================" + adPostVOJSON)
	 render adPostVOJSON;
	 }
	 def buyDeal() {
	 println('+++++++++++++buyDeal+++++params+++++++++' + params)
	 Long id = (params.id) as Long
	 PurchaseHistory purchaseHistory = new PurchaseHistory();
	 println("current user id=" + springSecurityService.currentUser.id)
	 purchaseHistory.buyerId = springSecurityService.currentUser.id
	 purchaseHistory.itemId = id;
	 purchaseHistory.dateCreated = new Date()
	 purchaseHistory.dateUpdated = new Date()
	 println('purchase history object created......')
	 purchaseHistory.status = 1;
	 purchaseHistory.save(flush: true);
	 println("purchase history object save successfully");
	 render purchaseHistory as JSON
	 }
	 def getBuyersList(Long id) {
	 println('++++++++Under Show Buyers++++++++++' + params)
	 List<PurchaseHistory> purchaseHistoryList = PurchaseHistory.findAllByItemId(id)
	 List<PurchaseHistoryVO> historyVOList = []
	 purchaseHistoryList.each {
	 historyVOList.add(new PurchaseHistoryVO(it));
	 }
	 println("++++++++++++purchaseHistoryVOList++++++as+++JSON " + (historyVOList as JSON))
	 render(historyVOList as JSON)
	 }
	 def getPurchaseDealList() {
	 User currentUser = springSecurityService.currentUser as User;
	 List<PurchaseHistory> purchaseHistoryList = PurchaseHistory.findAllByBuyerId(currentUser.id,[order:'desc' , sort:'id']);
	 List<AdPostVO> adPostVOList = []
	 purchaseHistoryList.each {
	 adPostVOList.add(new AdPostVO(it.adPost));
	 }
	 println("AdPostVolist JSON is " + (adPostVOList as JSON))
	 render(adPostVOList as JSON);
	 }
	 def getCurrentPurchasePosts(){
	 User currentUser = springSecurityService.currentUser as User;
	 PurchaseHistory purchaseHistoryList = PurchaseHistory.findByBuyerId(currentUser.id,[order:'desc' , sort:'id']);
	 AdPostVO postVO=new AdPostVO(purchaseHistoryList.adPost);
	 println("---------AdPostVo-----------"+postVO);
	 render(postVO as JSON);
	 }*/
}
