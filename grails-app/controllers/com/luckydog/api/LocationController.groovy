package com.luckydog.api

import com.luckydog.location.CityLocation
import com.luckydog.location.CountryLocation

class LocationController {

	public static def jsonify = "*"
	
	def findCity() {
		String cityName=params.cityName
		String countryName=params.countryName
		// firstName ==~ ~/B.+/ pattern match
		def results
		if(countryName)
		{
			results= CityLocation.findAll(sort:"cityName", max:10) {
				cityName =~ cityName+"%" && countryName =~countryName + "%"

			}
		}
		else
		{
			results= CityLocation.findAll(sort:"cityName", max:10) { cityName =~ cityName+"%"  }
		}

		return [location:results]
	}

	def findCountry()
	{
		String countryName=params.countryName
		// firstName ==~ ~/B.+/ pattern match
		def results = CountryLocation.findAll(sort:"countryName", max:10) { countryName =~ countryName+"%" }

		return [location:results]

	}

	def guessCity()
	{

	}


	def guessCountry()
	{

	}

}
