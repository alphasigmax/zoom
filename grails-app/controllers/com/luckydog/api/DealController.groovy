package com.luckydog.api

class DealController {

    def index() { }
	
	def show()
	{
		render (view:"/detail")
	}
	
	def create()
	{
		/*
		 * Try to populate with same address as merchant.
		 * Get the logged in merchant and pass his address in here
		 */
		render (view:"/deal/create")
	}
}
