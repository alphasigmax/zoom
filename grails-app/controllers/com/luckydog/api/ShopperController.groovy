package com.luckydog.api

import grails.converters.JSON

import org.springframework.security.access.annotation.Secured

import com.luckydog.security.Role
import com.luckydog.security.User
import com.luckydog.security.UserRole
import com.luckydog.shopper.PurchaseHistory
import com.luckydog.shopper.Shopper
import com.luckydog.utils.ErrorObject
import com.luckydog.utils.social.user.ShopperCO

class ShopperController {
	def springSecurityService;


	def register = { ShopperCO shopperCO ->
		Shopper shopperInstance
		log.debug("+++++++++++++++++Under shopper params+++++" + params)
		log.info "Registering Shopper .. "

		if(!shopperCO.validate()) {
			ErrorObject err = new ErrorObject()
			err.errorNumber = 1301
			err.errorDescription = "Please enter a valid email address and password for registering the merchant"
			log.error("-----------------------invalid params")

			return [error:err];
		}


		//check if user already exists
		if (User.findByUsername(shopperCO.email)) {
			ErrorObject err = new ErrorObject()
			err.errorNumber = 1302
			err.errorDescription = "Email is already registered. <br> Please enter a different email address or login to continue"
			log.error("------------------------email id exist")
			return [error:err];

		}

		/*bindData(shopperInstance, params, [include: ['name','password']])
		 shopperInstance.username=params.email;*/

		try{
			shopperInstance = new Shopper()
			shopperInstance.username = shopperCO.email;
			shopperInstance.enabled = true
			shopperInstance.name = shopperCO.name;
			shopperInstance.password = shopperCO.password
			shopperInstance.save(flush: true)
			def shopperRole = Role.findByAuthority('ROLE_SHOPPER')

			UserRole ur = UserRole.create(shopperInstance, shopperRole, true)
			ur.save(flush: true)
			log.debug("Shopper saved successfully.......................")
			return [shopper:shopperInstance];
		}
		catch(Exception ex)
		{
			ErrorObject err = new ErrorObject()
			err.errorNumber = 1301
			err.errorDescription = "Unknown error while registering"
			log.error(ex)

			return [error:err];
		}



	}

	@Secured(['ROLE_SHOPPER'])
	def purchaseHistory () {

		User user = springSecurityService.currentUser as User
		println(user.username + "----------" + user.id)
		List<PurchaseHistory> purchasePost = PurchaseHistory.findAllByBuyerId(user.id)
		//List pList=user.boughtItems;
		//println("+++++++++++++++++++++++++++"+pList)
		render purchasePost as JSON
		//return [model: user.boughtItems]

	}
	
	def profile()
	{
		
	}


}
