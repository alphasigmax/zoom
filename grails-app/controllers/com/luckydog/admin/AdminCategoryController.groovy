package com.luckydog.admin


import grails.converters.JSON

import com.luckydog.dealpost.AdCategory


class AdminCategoryController {

    /**
     * @param name name of the new category
     * @param description of the category
     * @param parent. The parent id to set for the category being created.
     * @return JSON of category
     */
    def createCategory() {
        def adCategoryInstance = new AdCategory()
        adCategoryInstance.name = params.name
        adCategoryInstance.description = params.description
        adCategoryInstance.save(flush: true)
        adCategoryInstance.parentId = params.parent as long
        AdCategory parent = AdCategory.get(params.parent)
        parent.getCatChildren().add(adCategoryInstance);
        parent.save(flush: true)

        render adCategoryInstance as JSON
    }

    /**
     * @param id the id of the category being modified
     * @param name name of the new category
     * @param description of the category
     * @param parent. The parent id to set for the category being created.
     * @return updated category
     */
    def updateCategory() {
        def adCategoryInstance = AdCategory.get(params.id)
        def prevParent = AdCategory.get(adCategoryInstance.parentId)
        prevParent.getCatChildren().remove(adCategoryInstance)

        if (params.name) adCategoryInstance.name = params.name
        if (params.description)
            adCategoryInstance.description = params.description
        adCategoryInstance.save(flush: true)

        if (params.parent) {
            AdCategory parent = AdCategory.get(params.parent)
            parent.getCatChildren().add(adCategoryInstance);
            parent.save(flush: true)
        }


        render adCategoryInstance as JSON
    }

    /**
     * @param catid the id of the category being delete
     * @return true false
     */
    def deleteCategory() {
        AdCategory adCategoryInstance = AdCategory.get(params.id)

        deleteCat(adCategoryInstance)
        render["true"] as JSON
    }

    private void deleteCat(AdCategory adCategoryInstance) {
        for (child in adCategoryInstance.catChildren) {
            deleteCat(child)
        }
        adCategoryInstance.delete(flush: true)
    }
}
