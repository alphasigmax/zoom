package com.luckydog.core



public class Runner {

	public static void main(String[] args) {
		//runCountryBlock()
		println "Main Running..."
		
	}
	
	private static void testDB()
	{
//		DBAddress address = new DBAddress("localhost", "mydb")
//		Mongo db = new Mongo(address)
//		db.authenticate('testUser', 'testPassword')
		
	}


	private static void runCountryLocation() {
		/*
		 * geoname_id,continent_code,continent_name,country_iso_code,country_name,subdivision_iso_code,
		 * subdivision_name,city_name,metro_code,time_zone
		 */
		new File("./db/GeoLite2-Country-Locations.csv").splitEachLine(",") {fields ->

			for(i in 0..9) {
				print fields[i] + " / "
			}
			println "\n"
			println "#" *25
		}
		
		/*
		 * TODO strip the double quotes
		 */
	}

	private static void runCountryBlock() {
		/*network_start_ip,network_prefix_length,geoname_id,
		 * registered_country_geoname_id,represented_country_geoname_id,postal_code,latitude,longitude,
		 * is_anonymous_proxy,is_satellite_provider
		 */


		new File("./db/GeoLite2-Country-Blocks.csv").splitEachLine(",") {
			fields ->
			
			for(i in 0..9) {
				print fields[i] + " / "
			}
			println "\n"
			println "#" *25
		}
	}


	private static void runCityLocation() {
		//geoname_id,continent_code,continent_name,country_iso_code,country_name,
		//subdivision_iso_code,subdivision_name,city_name,metro_code,time_zone

		new File("./db/GeoLite2-City-Locations.csv").splitEachLine(",") {fields ->
			println fields[0] + " // " + fields[1]+ " // " + fields[2]+ " // " + fields[3]+ " // " + fields[4]+ " // " + fields[5]
		}
	}

	private static void runCityBlock()
	{
		/*
		 * network_start_ip,network_prefix_length,geoname_id,registered_country_geoname_id,
		 * represented_country_geoname_id,postal_code,latitude,longitude,is_anonymous_proxy,is_satellite_provider
		 */

		new File("./db/GeoLite2-City-Blocks.csv").splitEachLine(",") {fields ->
			println fields[0] + " // " + fields[1]+ " // " + fields[2]+ " // " + fields[3]+ " // " + fields[4]+ " // " + fields[5]
		}
	}
}
