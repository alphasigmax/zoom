package com.luckydog.core

import org.springframework.web.servlet.ModelAndView

import com.luckydog.dealpost.AdPost
import com.luckydog.shopper.ShopItem;

class ShoppingController {
    def shoppingCartService

    static defaultAction = "buyDeal"

    def buyDeal() {
        //String username = getPrincipal().username
        def adid = params.adid
        if (adid) {
            def post = AdPost.get(adid)
            ShopItem item = new ShopItem()
            item.adPostId = adid
            item.title = post.title
            item.price = post.curPrice
            shoppingCartService.addToShoppingCart(item)
        }

        def items = shoppingCartService.getItems()
//		for(ShoppingItem item in items)
//		{
//			println item
//		}
        return new ModelAndView("/shopper/shoppingCart", [items: shoppingCartService.getItems()])
    }


}
