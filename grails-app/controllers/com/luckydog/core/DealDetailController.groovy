package com.luckydog.core

import org.springframework.web.servlet.ModelAndView

import com.luckydog.dealpost.AdPost

class DealDetailController {
    static defaultAction = "showDeal"

    def showDeal() {
        def adid = params.id
        def post = AdPost.get(adid)

        return new ModelAndView("/deals/details", [deal: post])
    }


}
