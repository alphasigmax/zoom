package com.luckydog.core

import org.springframework.web.servlet.ModelAndView

import com.luckydog.dealpost.AdPost
import com.luckydog.shopper.ShopItem;

class ContactController {

    def mailService
    static defaultAction = "sendmail"

    def sendmymail() {
       
        String name = params.name
        String company = params.company
        String email = params.email
        String phone = params.phone
        String location = params.location
        String message = params.message

        String emessage = " $name / $company / $email / $phone / $location :: MESSAGE: $message"

        mailService.sendMail {
            to "info@alphasigma.com.au"
            subject "CONTACT US FORM"
            body emessage
        }


        flash.message = "Email Sent!"
        flash.default = "Your email was sent"
        return new ModelAndView("/contactus", [:])
    }


}
